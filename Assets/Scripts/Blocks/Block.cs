﻿using UnityEngine;
using System;       //Add this line
using System.Collections.Generic;

[Serializable]
public class Block
{
    public byte id;
    public byte dmg;
    public byte form;
    public byte tint;
    public byte suspension;
    public GameObject model;
    public CCLComponent cclComponent;

    public const float TILE_SIZE = 0.125f;
    public const float TILE_DMG = 0.25f;

    bool[] solid = { true, true, true, true, true, true };


    public Block(byte id, byte dmg = 0, byte form = 0, byte tint = 0, byte suspension = 0)
    {
        this.id = id;
        this.dmg = dmg;
        this.form = form;
        this.tint = tint;
        this.suspension = suspension;
    }


    public CCLComponent GetCCLComponent()
    {
        if (cclComponent == null) return new CCLComponent(1);
        return cclComponent.GetMasterComponent();
    }

    public Vector2[] TriUVs(Vector2 tilePos, float tile, int c1, int c2, int c3)
    {
        Vector2[] UVs = new Vector2[8];
        UVs[0] = new Vector2(tile * tilePos.x, tile * tilePos.y + tile);
        UVs[1] = new Vector2(tile * tilePos.x + tile / 2, tile * tilePos.y + tile);
        UVs[2] = new Vector2(tile * tilePos.x + tile, tile * tilePos.y + tile);

        UVs[3] = new Vector2(tile * tilePos.x, tile * tilePos.y + tile / 2);
        UVs[4] = new Vector2(tile * tilePos.x + tile, tile * tilePos.y + tile / 2);

        UVs[5] = new Vector2(tile * tilePos.x, tile * tilePos.y);
        UVs[6] = new Vector2(tile * tilePos.x + tile / 2, tile * tilePos.y);
        UVs[7] = new Vector2(tile * tilePos.x + tile, tile * tilePos.y);
        Vector2[] UV = new Vector2[] {UVs[c1], UVs[c2] , UVs[c3]};
        return UV;
    }
    public Vector2[] FaceUVs(Vector2 tilePos ,float tile)
    {
        Vector2[] UVs = new Vector2[4];
        UVs[0] = new Vector2(tile * tilePos.x + tile, tile * tilePos.y);
        UVs[1] = new Vector2(tile * tilePos.x + tile, tile * tilePos.y + tile);
        UVs[2] = new Vector2(tile * tilePos.x, tile * tilePos.y + tile);
        UVs[3] = new Vector2(tile * tilePos.x, tile * tilePos.y);

        return UVs;
    }

    public MeshData Blockdata(Chunk chunk, int x, int y, int z, MeshData meshData)
    {             
        Block_Records.BlockRecord thisBlock = Block_Records.GetRecord(id);
        bool isTrans = thisBlock != null ? thisBlock.transparent > 1 : false;
        Vector2[][] UV1 = new Vector2[6][] { FaceUVs(TexturePosition(Direction.down), TILE_SIZE), FaceUVs(TexturePosition(Direction.up), TILE_SIZE), FaceUVs(TexturePosition(Direction.north), TILE_SIZE), FaceUVs(TexturePosition(Direction.south), TILE_SIZE), FaceUVs(TexturePosition(Direction.east), TILE_SIZE), FaceUVs(TexturePosition(Direction.west), TILE_SIZE) };

        Block blk = chunk.world.GetBlock(chunk.pos.x + x, chunk.pos.y + y, chunk.pos.z + z);
        if (blk != null && blk.model == null) 
        {
            bool[] rf = new bool[6];
            Block block = null;
            block = chunk.world.GetBlock(chunk.pos.x + x, chunk.pos.y + y - 1, chunk.pos.z + z);
            if (block == null || !block.IsSolid(Direction.up, blk) || (block.IsTransparent(Direction.up) && !isTrans && id != 21))
                rf[0] = true;
            block = chunk.world.GetBlock(chunk.pos.x + x, chunk.pos.y + y + 1, chunk.pos.z + z);
            if (block == null || !block.IsSolid(Direction.down, blk) || (block.IsTransparent(Direction.down) && !isTrans && id != 21))
                rf[1] = true;
            block = chunk.world.GetBlock(chunk.pos.x + x, chunk.pos.y + y, chunk.pos.z + z + 1);
            if (block == null || !block.IsSolid(Direction.south, blk) || (block.IsTransparent(Direction.south) && !isTrans && id != 21))
                rf[2] = true;
            block = chunk.world.GetBlock(chunk.pos.x + x, chunk.pos.y + y, chunk.pos.z + z - 1);
            if (block == null || !block.IsSolid(Direction.north, blk) || (block.IsTransparent(Direction.north) && !isTrans && id != 21))
                rf[3] = true;
            block = chunk.world.GetBlock(chunk.pos.x + x + 1, chunk.pos.y + y, chunk.pos.z + z);
            if (block == null || !block.IsSolid(Direction.west, blk) || (block.IsTransparent(Direction.west) && !isTrans && id != 21))
                rf[4] = true;
            block = chunk.world.GetBlock(chunk.pos.x + x - 1, chunk.pos.y + y, chunk.pos.z + z);
            if (block == null || !block.IsSolid(Direction.east, blk) || (block.IsTransparent(Direction.east) && !isTrans && id != 21))
                rf[5] = true;


            int data = World.GetDmgData(blk.dmg, false);
            if (data == 1)
            {
                int by = form;
                bool[] crn = new bool[8];
                int p = 0;
                for (int i = 128; i >= 1; i = i / 2)
                {
                    if (by >= i)
                    {
                        crn[p] = true;
                        by = by - i;
                    }
                    else
                        crn[p] = false;
                    p++;
                }
                //render face - will render and extra faces slopes might need
                rf[0] = (rf[0] && crn[4] && crn[5] && crn[6] && crn[7]);
                rf[1] = (rf[1] && crn[0] && crn[1] && crn[2] && crn[3]);
                rf[2] = (rf[2] && crn[0] && crn[1] && crn[4] && crn[5]);
                rf[3] = (rf[3] && crn[2] && crn[3] && crn[6] && crn[7]);
                rf[4] = (rf[4] && crn[1] && crn[3] && crn[5] && crn[7]);
                rf[5] = (rf[5] && crn[0] && crn[2] && crn[4] && crn[6]);
                Slope.blockData(chunk, x, y, z, meshData, isTrans, UV1, FaceUVs(DmgValuePosition(), TILE_DMG), crn);
            }
            CubePole.blockData(chunk, x, y, z, meshData, isTrans, UV1, FaceUVs(DmgValuePosition(), TILE_DMG), rf);
            if (id == Block_Records.ID_CONSTUCTS && !burried(chunk, x, y, z))
                meshData = Frame.FaceDataExtra(x, y, z, meshData, isTrans, UV1);
        }
        return meshData;
    }

    public bool IsTransparent(Direction direction)
    {
        Block_Records.BlockRecord br = Block_Records.GetRecord(id);

        if (br == null)
            return false;
        else if (br.transparent == 0)
            return false;
        return true;
    }

    public static bool burried(Chunk chunk, int x, int y, int z)
    {
        Block block = null;

        block = chunk.world.GetBlock(chunk.pos.x + x, chunk.pos.y + y + 1, chunk.pos.z + z);
        bool above = block != null && block.id != 21 ? block.IsSolid(Direction.down) : false;

        block = chunk.world.GetBlock(chunk.pos.x + x, chunk.pos.y + y - 1, chunk.pos.z + z);
        bool below = block != null && block.id != 21 ? block.IsSolid(Direction.up) : false;

        block = chunk.world.GetBlock(chunk.pos.x + x, chunk.pos.y + y, chunk.pos.z + z + 1);
        bool north = block != null && block.id != 21 ? block.IsSolid(Direction.south) : false;

        block = chunk.world.GetBlock(chunk.pos.x + x, chunk.pos.y + y, chunk.pos.z + z - 1);
        bool south = block != null && block.id != 21 ? block.IsSolid(Direction.north) : false;

        block = chunk.world.GetBlock(chunk.pos.x + x - 1, chunk.pos.y + y, chunk.pos.z + z);
        bool east = block != null && block.id != 21 ? block.IsSolid(Direction.east) : false;

        block = chunk.world.GetBlock(chunk.pos.x + x + 1, chunk.pos.y + y, chunk.pos.z + z);
        bool west = block != null && block.id != 21 ? block.IsSolid(Direction.west) : false;

        if (above && below && north && south && east && west)
            return true;
        return false;
    }


    public bool IsSolid(Direction direction, Block self = null)
    {
        byte data = Methods.boolToByte(Methods.byteToBool(dmg), 1, 5);
        if (data == 1)
        {
            bool[] crn = Methods.byteToBool(form);
            solid[0] = (crn[4] && crn[5] && crn[6] && crn[7]);
            solid[1] = (crn[0] && crn[1] && crn[2] && crn[3]);
            solid[2] = (crn[0] && crn[1] && crn[4] && crn[5]);
            solid[3] = (crn[2] && crn[3] && crn[6] && crn[7]);
            solid[4] = (crn[1] && crn[3] && crn[5] && crn[7]);
            solid[5] = (crn[0] && crn[2] && crn[4] && crn[6]);
        }
        else if (data == 2)
        {
            if (self != null)
            {
                byte data2 = Methods.boolToByte(Methods.byteToBool(self.dmg), 1, 5);
                byte case1 = Methods.boolToByte(Methods.byteToBool(form), 1, 5);
                //byte case2 = Methods.boolToByte(Methods.byteToBool(self.form), 1, 5); case2 = 0;
                float size1 = ((float)Methods.boolToByte(Methods.byteToBool(form), 4, 0) + 1) / 32;
                float size2 = ((float)Methods.boolToByte(Methods.byteToBool(self.form), 4, 0) + 1) / 32;
                if (data2 != 2) { size2 = 1; }
                if(size1 >= size2) {
                    if (case1 == 0 || case1 == 4)
                    {
                        return true;
                    }
                }
                else solid = new bool[6] { false, false, false, false, false, false };
            }
        }
        if (id >= 40 && id < 45)
            return false;

        switch (direction)
        {
            case Direction.down:
                return solid[0];
            case Direction.up:
                return solid[1];
            case Direction.north:
                return solid[2];
            case Direction.south:
                return solid[3];
            case Direction.east:
                return solid[4];
            case Direction.west:
                return solid[5];
        }
        return false;
    }

    public Vector2 TexturePosition(Direction direction = Direction.south)
    {
        Vector2 tile = new Vector2(2, 2);

        Block_Records.BlockRecord br = Block_Records.GetRecord(id);

        if (br == null)
            return tile;
        
        switch (direction)
        {
            case Direction.east:
                goto case Direction.south;
            case Direction.west:
                goto case Direction.south;
            case Direction.north:
                goto case Direction.south;
            case Direction.south:
                tile.x = br.sideTexU;
                tile.y = br.sideTexV;
                break;
            case Direction.up:
                tile.x = br.topTexU;
                tile.y = br.topTexV;
                break;
            case Direction.down:
                tile.x = br.botTexU;
                tile.y = br.botTexV;
                break;
        }
        
        return tile;
    }
    public Vector2 DmgValuePosition()
    {
        int Dmg = World.GetDmgData(dmg, true);
        switch (Dmg)
        {
            case 1:
                return new Vector2(1, 0);
            case 2:
                return new Vector2(2, 0);
            case 3:
                return new Vector2(3, 0);
            case 4:
                return new Vector2(0, 1);
            case 5:
                return new Vector2(1, 1);
            case 6:
                return new Vector2(2, 1);
            case 7:
                return new Vector2(2, 1);
            case 8:
                return new Vector2(3, 1);
            case 9:
                return new Vector2(3, 1);
            case 10:
                return new Vector2(0, 2);
            case 11:
                return new Vector2(0, 2);
            case 12:
                return new Vector2(1, 2);
            case 13:
                return new Vector2(1, 2);
            case 14:
                return new Vector2(2, 2);
            case 15:
                return new Vector2(2, 2);
            case 16:
                return new Vector2(3, 2);
            case 17:
                return new Vector2(3, 2);
            case 18:
                return new Vector2(0, 3);
            case 19:
                return new Vector2(0, 3);
            case 20:
                return new Vector2(1, 3);
            case 21:
                return new Vector2(1, 3);
            case 22:
                return new Vector2(2, 3);
            case 23:
                return new Vector2(2, 3);
            case 24:
                return new Vector2(3, 3);
            case 25:
                return new Vector2(3, 3);
            default:
                return new Vector2(0, 0);
        }
    }
    public Vector2 TintValuePosition()
    {
        return new Vector2(0, 0);
    }

    public override string ToString()
    {
        return string.Format("(Block id:{0}, dmg:{1}, form:{2}, tint:{3}, suspension:{4}, model: {5})", id, dmg, form, tint, suspension, model);
    }
}
