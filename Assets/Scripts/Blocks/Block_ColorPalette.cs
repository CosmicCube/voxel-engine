﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block_ColorPalette : MonoBehaviour {
    public static Color[] colPal = basic();

    static Color[] basic()
    {
        Color[] col = new Color[256];
        col[0] = new Color(0.5f, 0.5f, 0.5f);
        col[1] = new Color(0, 0.24f, 0.64f);
        col[2] = new Color(0.18f, 0.41f, 0.79f);
        col[3] = new Color(0.59f, 0.02f, 0.02f);
        col[4] = new Color(0.78f, 0.20f, 0.20f);
        col[5] = new Color(0.02f, 0.45f, 0.21f);
        col[6] = new Color(0.07f, 0.55f, 0.28f);
        col[7] = new Color(0.58f, 0.07f, 0.61f);
        col[8] = new Color(0.80f, 0.20f, 0.84f);
        col[9] = new Color(1, 1, 1);
        col[10] = new Color(0, 0, 0);
        return col;
    }
}
