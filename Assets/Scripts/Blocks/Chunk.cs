﻿using UnityEngine;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(MeshCollider))]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class Chunk : MonoBehaviour
{
    public static int CHUNK_SIZE = 16;
    public Block[,,] blocks = new Block[CHUNK_SIZE, CHUNK_SIZE, CHUNK_SIZE];

    private MeshCollider coll;
    public MeshCollider hull;
    private MeshFilter filter;
    public World world;

    public V3I pos;

    public bool dirty = false;
    
    public void InitChunk(int x, int y, int z, GameObject hull)
    {
        coll = GetComponent<MeshCollider>();
        filter = GetComponent<MeshFilter>();
        dirty = true;
        name = "Chunk_" + x / CHUNK_SIZE + "-" + y / CHUNK_SIZE + "-" + z / CHUNK_SIZE;       
        pos = new V3I(x, y, z);
        if (hull != null) this.hull = hull.GetComponent<MeshCollider>();
    }


    public Block GetBlock(int x, int y, int z)
    {
        if (x < 0 || x >= CHUNK_SIZE)
            return null;

        if (y < 0 || y >= CHUNK_SIZE)
            return null;

        if (z < 0 || z >= CHUNK_SIZE)
            return null;

        return blocks[x, y, z];
    }

    public void Rebuild()
    {
        MeshData meshData = new MeshData();

        for (int x = 0; x < CHUNK_SIZE; x++)
            for (int y = 0; y < CHUNK_SIZE; y++)
                for (int z = 0; z < CHUNK_SIZE; z++)
                    if (blocks[x, y, z] != null)
                        meshData = blocks[x, y, z].Blockdata(this, x, y, z, meshData);

        RenderMesh(meshData);
    }

    public void SetBlock(int x, int y, int z, Block block)
    {
        if (x < 0 || y < 0 || z < 0 || x >= CHUNK_SIZE || y >= CHUNK_SIZE || z >= CHUNK_SIZE)
        {
            //Debug.Log("block is out of bounds: " + x + " " + y + " " + z);
            return;
        }
        if (block != null && (Block_Records.GetCost(block.id) != null && Block_Records.GetCost(block.id).prefabName != null))
            Device_Block.InitDevice(this, block, pos.x + x, pos.y + y, pos.z + z);
        blocks[x, y, z] = block;
    }
    
    void RenderMesh(MeshData meshData)
    {
        filter = GetComponent<MeshFilter>();
        coll = GetComponent<MeshCollider>();

        filter.mesh.Clear();
        filter.mesh.subMeshCount = 2;
        filter.mesh.SetVertices(meshData.vertices);
        filter.mesh.SetColors(meshData.vertColours);
        filter.sharedMesh.SetIndices(meshData.triangles.ToArray(), MeshTopology.Triangles, 0);
        filter.sharedMesh.SetIndices(meshData.trianglesTrans.ToArray(), MeshTopology.Triangles, 1);
        filter.sharedMesh.SetTriangles(meshData.triangles, 0);
        filter.sharedMesh.SetTriangles(meshData.trianglesTrans, 1);

        filter.mesh.uv = meshData.uv.ToArray();
        filter.mesh.uv2 = meshData.uv2.ToArray();
        filter.mesh.RecalculateNormals();
        filter.mesh.UploadMeshData(false);


        Mesh mesh = new Mesh();
        mesh.name = "runtimemesh";
        mesh.vertices = meshData.colisionVerts.ToArray();
        mesh.triangles = meshData.colTriangles.ToArray();
        mesh.RecalculateNormals();

        coll.sharedMesh = null;
        coll.sharedMesh = mesh;

        if (hull != null)
        {
            hull.sharedMesh = null;
            hull.sharedMesh = mesh;
        }
    }

    public int maxIndex(List<int> arr)
    {
        int max = 0;
        foreach (int i in arr)
        {
            if (i >= max)
                max = i;
        }
        return max;
    }
}