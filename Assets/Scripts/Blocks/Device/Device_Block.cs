﻿using System.Collections.Generic;
using UnityEngine;

public class Device_Block : MonoBehaviour {
    public World world;
    public Block block; 

    public static void InitDevice(Chunk chunk, Block block, int x, int y, int z)
    {
        if (block.model == null)
        {
            Block_Records.BlockCost record = Block_Records.GetCost(block.id);
            if (record == null) return;
            
            Quaternion rotation = Quaternion.identity;
            switch (block.form)
            {
                case 1: rotation = Quaternion.Euler(0, 90, -90); break;     //north
                case 2: rotation = Quaternion.Euler(180, 90, 90); break;    //south
                case 3: rotation = Quaternion.Euler(0, -180, -90); break;   //east
                case 4: rotation = Quaternion.Euler(0, 0, -90); break;      //weast

                case 5: rotation = Quaternion.Euler(180, 180, 0); break;    //up

                case 6: rotation = Quaternion.Euler(180, 90, -90); break;   //up north
                case 7: rotation = Quaternion.Euler(-180, -90, -90); break; //up south
                case 8: rotation = Quaternion.Euler(0, 0, 90); break;       //up east
                case 9: rotation = Quaternion.Euler(-180, 0, -90); break;   //up weast
            }
            block.model = (GameObject)Instantiate(Resources.Load("Prefabs/Devices/" + record.prefabName), Vector3.zero, Quaternion.identity);
            block.model.transform.parent = chunk.transform;
            block.model.transform.localPosition = -chunk.transform.localPosition + new V3I(x, y, z);
            block.model.transform.localEulerAngles = rotation.eulerAngles;
            block.model.layer = chunk.gameObject.layer;

            if (record.dimensions != new V3I(0, 0, 0))
            {
                for (int x1 = -(record.dimensions.x / 2); x1 < record.dimensions.x - (record.dimensions.x / 2); x1++)
                    for (int y1 = -(record.dimensions.y / 2); y1 < record.dimensions.y - (record.dimensions.y / 2); y1++)
                        for (int z1 = -(record.dimensions.z / 2); z1 < record.dimensions.z - (record.dimensions.z / 2); z1++)
                        {
                            if (chunk.world.GetBlock(x + x1, y + y1, z + z1) == null)
                                chunk.world.SetBlock(x + x1, y + y1, z + z1, block);
                        }
                chunk.world.RebuildWorld();
            }
        }
        Device_Block db = block.model.GetComponent<Device_Block>();
        if (db != null)
        {
            db.block = block;
            db.world = chunk.world;
        }

    }
}
