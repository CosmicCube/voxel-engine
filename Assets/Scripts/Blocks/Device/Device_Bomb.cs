﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Device_Bomb : Device_Block {
    public int TimeRemaining = 0;
    public float BlastRadius = 0;
    public float BlastPower = 0;
    public float BlastKnockback = 0;
    Text text;

    void Start () {
        Device_Interactable inter = gameObject.GetComponent<Device_Interactable>();
        if (inter == null) inter = gameObject.AddComponent<Device_Interactable>();
        block.model = gameObject;
        inter.DisplayInfo();
        text = transform.GetChild(0).GetComponentInChildren<Text>();
        UpdateDisplay();
    }
	void UpdateDisplay()
    {
        TimeRemaining--;
        text.color = Color.Lerp(Color.red, Color.yellow, (float)TimeRemaining / 16);
        text.text = TimeRemaining.ToString();

        if (TimeRemaining == 0) Explode();
        else StartCoroutine(Wait1Sec());
    }
    IEnumerator Wait1Sec()
    {
        yield return new WaitForSeconds(1);
        UpdateDisplay();
    }
    void Explode()
    {
        Destroy(gameObject.GetComponent<Device_Interactable>());
        Destroy(gameObject.GetComponent<MeshRenderer>());
        GameObject g = Instantiate(Resources.Load("Prefabs/Test/Explosion"), transform.position, Quaternion.identity) as GameObject;
        World_Events.Explosion(transform.position, BlastRadius, BlastPower, BlastKnockback);
        g.transform.SetParent(transform);
        StartCoroutine(WaitForExplode(g));
    }
    IEnumerator WaitForExplode(GameObject g)
    {
        yield return new WaitForSeconds(20);
        Destroy(g);
        Destroy(gameObject);
    }
}
