﻿using UnityEngine;
using System.Collections;

public class Device_Bouncer : Device_Block {
    private Vector3 Final;
    void OnTriggerEnter(Collider other)
    {
        Rigidbody rb = other.GetComponent<Rigidbody>();
        if (rb != null)
        {
            rb.velocity = new Vector3(Mathf.Clamp(rb.velocity.x + (transform.up.x * 15), -17, 17), 
                                      Mathf.Clamp(rb.velocity.y + (transform.up.y * 15), -17, 17),
                                      Mathf.Clamp(rb.velocity.z + (transform.up.z * 15), -17, 17)); 
        }
        Character_Controller cc = other.GetComponent<Character_Controller>();
        if(cc != null)
        {
            cc.Applied = transform.up * 15;
        }
    }
}
