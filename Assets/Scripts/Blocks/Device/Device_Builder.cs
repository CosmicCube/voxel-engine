﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class Device_Builder : Device_Block{
    public bool holoDisplay = false;
    public Device_Builder master;
    public List<Device_Builder> nodes = new List<Device_Builder>();
    public float done = 1;

    byte[] data = null;
    World mainWorld;
    World buildingWorld;
    V3I Size = new V3I(0, 0, 0);


    //ENTRY POINT
    void Start()
    {
        if (transform.parent != null && transform.parent.gameObject.GetComponent<Chunk>() != null) mainWorld = transform.parent.gameObject.GetComponent<Chunk>().world;
        else return;
        Block mas = NearbyCheck((mainWorld.transform.InverseTransformPoint(transform.position)), Block_Records.ID_BUILDER);
        if (mas != null)
        {
            master = mas.model.GetComponent<Device_Builder>();
            if (master.nodes.Count == 0 && master.master != null)
                master = master.master;
            master.nodes.Add(this);
        }
        else nodes.Add(this);
    }
    void OnDestroy()
    {
        if(master != null)
            master.nodes.Remove(this);
        //if(display != null)
        //    World_Load.RemoveWorld(display);
    }
    
    //BEGIN BUILDING
    public void startBuilding(Device_Interactable inter, string name, bool holo)
    {
        //if the file does not exist or the builder is already at work return
        string path = Directory.GetCurrentDirectory() + @"\Assets\Resources\Blueprints\" + name  + ".bytes";
        if (!File.Exists(path) || data != null) return;

        //close the GUI and open the file  
        holoDisplay = holo;
        inter.LifeTime = 0;
        State_Manager.settings = false;
        UnityEngine.Cursor.visible = false;
        Stream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None);

        //create an emtpy to nest the file under
        GameObject holder = new GameObject(name);
        holder.transform.SetParent(World_Load.self.transform);
        buildingWorld = World_Load.AddWorld("world", holder);
        buildingWorld.gameObject.layer = 9;

        //create the convex coliders hull
        GameObject hull = new GameObject("Hull", typeof(Rigidbody), typeof(World_Joint));
        hull.transform.SetParent(holder.transform);
        hull.layer = 10;
        hull.GetComponent<World_Joint>().world = buildingWorld;

        //set up the ridgidbody on the main world
        buildingWorld.rb = hull.GetComponent<Rigidbody>();
        buildingWorld.rb.mass = 100;
        buildingWorld.rb.useGravity = false;
        buildingWorld.rb.drag = 1;
        buildingWorld.rb.angularDrag = 3;
        buildingWorld.rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;

        //load the chunks and pass them to the world
        Chunk[,,] chunks = LoadFromFile(fs, hull);
        buildingWorld.WorldInit(chunks, Size);
        buildingWorld.RebuildWorld(true);

        //center the sub-world over the builder block
        hull.transform.position = new Vector3(transform.position.x - Size.x / 2, transform.position.y, transform.position.z - Size.z / 2);
        buildingWorld.transform.position = new Vector3(transform.position.x - Size.x / 2, transform.position.y, transform.position.z - Size.z / 2);
    }

    //LOAD A SET OF CHUNKS FROM A FILE
    public Chunk[,,] LoadFromFile(Stream stream, GameObject hull)
    {
        using (BinaryReader br = new BinaryReader(stream))
        {
            string name = br.ReadString();
            float killPlane = br.ReadSingle();
            float waterPlane = br.ReadSingle();
            short length = br.ReadInt16();
            short height = br.ReadInt16();
            short width = br.ReadInt16();

            Size = new V3I(width, height, length);
            int dataSize = length * width * height * 4;

            data = br.ReadBytes(dataSize);
            Chunk[,,] chunks = new Chunk[width / Chunk.CHUNK_SIZE + 1, height / Chunk.CHUNK_SIZE + 1, length / Chunk.CHUNK_SIZE + 1];
            for (int x = 0; x <= width / Chunk.CHUNK_SIZE; x++)
                for (int y = 0; y <= height / Chunk.CHUNK_SIZE; y++)
                    for (int z = 0; z <= length / Chunk.CHUNK_SIZE; z++)
                        chunks[x, y, z] = World_Load.CreateChunk(x * Chunk.CHUNK_SIZE, y * Chunk.CHUNK_SIZE, z * Chunk.CHUNK_SIZE, buildingWorld, hull);
            if (holoDisplay)
            {
                for (int z = 0; z < length; z++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        for (int x = 0; x < width; x++)
                        {
                            int idx = 4 * (x + (y * width) + (z * height * width));
                            if (data[idx] == 0)
                                continue;

                            Block b = new Block(Block_Records.ID_HOLOGRAM);
                            chunks[x / Chunk.CHUNK_SIZE, y / Chunk.CHUNK_SIZE, z / Chunk.CHUNK_SIZE].SetBlock(World_Load.ChunkCoordinate(x), World_Load.ChunkCoordinate(y), World_Load.ChunkCoordinate(z), b);
                        }
                    }
                }
            }
            return chunks;
        }
    }

    //DO THE ACTUAL BUILDING
    int delay = 0;
    void FixedUpdate () {

        if (data != null && delay == 0) {
            int idx = 0;
            foreach (Device_Builder node in nodes)
            {
                while (true)
                {
                    if (data == null) return;
                    idx = 4 * (iteration.x + (iteration.y * Size.z) + (iteration.z * Size.y * Size.z));
                    if (data[idx + 0] == 0) { Iterate(); continue; }
                    Block blk = buildingWorld.GetBlock(iteration.x, iteration.y, iteration.z);
                    if (blk == null || blk.id == Block_Records.ID_HOLOGRAM) break;
                    Iterate();
                }
                Block b = new Block(data[idx + 0], data[idx + 1], data[idx + 2], data[idx + 3]);
                buildingWorld.SetBlock(iteration.x, iteration.y, iteration.z, b);
                Iterate();
            }
            buildingWorld.RebuildWorld();
            buildingWorld.rb.velocity = Vector3.zero;

            done = (float)idx / (4 * Size.x * Size.y * Size.z);
            delay = 10;
        }
        if (delay > 0) delay--;
    }

    //ITERATE THRU ALL THE POSSIBLE BLOCKS LAYER BY LAYER BOTTOM TO TOP
    V3I iteration = new V3I(0, 0, 0);
    void Iterate()
    {
        iteration.x++;
        if (iteration.x >= Size.x) { iteration.x = 0; iteration.z++; }
        if (iteration.z >= Size.z) { iteration.z = 0; iteration.y++; }
        if (iteration.y >= Size.y)
        {
            //display.rb.constraints = 0;
            buildingWorld.RebuildWorld(true);
            data = null;
            iteration = new V3I(0, 0, 0);
        }
    }

    //CHECK THE NEARBY BLOCKS TO SEE IF ANY ARE BUILDERS
    static readonly int[][] pos = new int[][] { new int[] { 0, 1, 0 }, new int[] { 0, -1, 0 }, new int[] { 1, 0, 0 }, new int[] { -1, 0, 0 }, new int[] { 0, 0, 1 }, new int[] { 0, 0, -1 } };
    Block NearbyCheck(Vector3 l, byte id)
    {
        mainWorld = transform.parent.GetComponent<Chunk>().world;
        for (int i = 0; i < 6; i++)
        {
            Block blk = mainWorld.GetBlock(pos[i][0] + (int)l.x, pos[i][1] + (int)l.y, pos[i][2] + (int)l.z);
            if (blk != null && blk.id == id)
                return blk;
        }
        return null;
    }
}
