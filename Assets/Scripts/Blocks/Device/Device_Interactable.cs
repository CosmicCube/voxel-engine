﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Device_Interactable : MonoBehaviour
{
    public int LifeTime;
    Block block;
    GameObject canvas;
    GameObject healthPos;
    GameObject healthNeg;
    GameObject panel;

    void Awake()
    {
        block = GetComponent<Device_Block>().block;
    }

    public void DisplayHealth()
    {
        LifeTime = 800;
        if (canvas == null) Canvas(block.id);
        if (healthNeg == null)
        {
            healthNeg = Image("Health Negative");
            healthNeg.GetComponent<Image>().color = Color.red;
            healthNeg.transform.localScale = new Vector3(1, 0.1f, 1);
        }
        if (healthPos == null)
        {
            healthPos = Image("Health Positive");
            healthPos.GetComponent<Image>().color = Color.green;
            healthPos.transform.localScale = new Vector3(1, 0.1f, 1);
        }
        float hp = 1 - (((float)block.dmg + 1) / 256);
        healthNeg.transform.localPosition = Vector3.zero;
        healthPos.transform.localPosition = new Vector3((hp * 50) - 50, 0, 0);
        healthPos.transform.localScale = new Vector3(hp, 0.1f, 1);
    }

    public void DisplayGUI()
    {
        Device_Builder db = block.model.GetComponent<Device_Builder>();
        if (db.master != null) db = db.master;

        LifeTime = -1;
        if (canvas == null) Canvas(block.id);
        if (healthNeg == null)
        {
            healthNeg = Image("Health Negative");
            healthNeg.GetComponent<Image>().color = Color.red;
            healthNeg.transform.localScale = new Vector3(1, 0.1f, 1);
        }
        if (healthPos == null)
        {
            healthPos = Image("Health Positive");
            healthPos.GetComponent<Image>().color = Color.green;
            healthPos.transform.localScale = new Vector3(1, 0.1f, 1);
        }
        float hp = 1 - (((float)block.dmg + 1) / 256);
        healthNeg.transform.localPosition = new Vector3(0, 45, 0);
        healthPos.transform.localPosition = new Vector3((hp * 50) - 50, 45, 0);
        healthPos.transform.localScale = new Vector3(hp, 0.1f, 1);

        //now display the menue stuff
        State_Manager.settings = true;
        UnityEngine.Cursor.visible = true;
        if (canvas.GetComponent<GraphicRaycaster>() == null) canvas.AddComponent<GraphicRaycaster>();

        if (panel == null)
        {
            panel = (GameObject)Instantiate(Resources.Load("Prefabs/Device_GUI/Panel - " + Block_Records.GetCost(block.id).prefabName), Vector3.zero, Quaternion.identity);
            panel.transform.SetParent(canvas.transform);
            panel.transform.localPosition = new Vector3(0, 10, 0);
            panel.transform.localScale = new Vector3(1, 1, 1);
            Button button = panel.transform.GetChild(panel.transform.childCount - 1).GetComponent<Button>();
            switch (block.id)
            {
                case Block_Records.ID_BUILDER:
                    InitBuilder(db, button);
                    break;

            }
        }
    }

    public void DisplayInfo()
    {
        LifeTime = -1;
        if (canvas == null) Canvas(block.id);
        if (panel == null) panel = Text("Panel - Countdown");
    }

    private void InitBuilder(Device_Builder db, Button button)
    {
        InputField input = panel.transform.GetChild(0).GetComponent<InputField>();
        Toggle toggle = panel.transform.GetChild(1).GetComponent<Toggle>();
        toggle.isOn = db.holoDisplay;
        if (db.done == 1)
            button.onClick.AddListener(delegate { db.startBuilding(this, input.text, toggle.isOn); });
        else
        {
            button.transform.GetChild(0).GetComponent<Text>().text = db.done + "%";
            button.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 120);
            button.onClick.AddListener(delegate { CloseGUI(); });
        }
    }
    void CloseGUI()
    {
        LifeTime = 0;
        State_Manager.settings = false;
        UnityEngine.Cursor.visible = false;
    }


    //CREATE A CANVAS
    void Canvas(byte id)
    {
        float Yoffset = 0.7f;
        if (id == Block_Records.ID_BOUNCER) Yoffset = 0.2f;
        if (id == Block_Records.ID_BOMB) Yoffset = 0.4f;
        if (id == Block_Records.ID_TURBINE) Yoffset = 1.6f;
        canvas = new GameObject("GUI_Canvas", typeof(RectTransform), typeof(Canvas), typeof(CanvasScaler));
        canvas.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
        canvas.GetComponent<Canvas>().worldCamera = Character_Modify_Terrain.self.gameObject.GetComponent<Camera>();
        RectTransform rt = canvas.GetComponent<RectTransform>();
        rt.SetParent(transform);
        rt.localScale = new Vector3(0.01f, 0.01f, 0.01f);
        rt.localPosition = new Vector3(0, Yoffset, 0);
    }
    //CREATE A IMAGE
    GameObject Image(string name)
    {
        GameObject g = new GameObject(name, typeof(RectTransform), typeof(CanvasRenderer), typeof(Image));
        g.transform.SetParent(canvas.transform);
        g.transform.localPosition = Vector3.zero;
        g.transform.localScale = new Vector3(1, 1, 1);
        return g;
    }
    //CREATE A TEXT FEILD
    GameObject Text(string name)
    {
        GameObject g = new GameObject(name, typeof(RectTransform), typeof(CanvasRenderer), typeof(Text));
        g.transform.SetParent(canvas.transform);
        g.transform.localPosition = Vector3.zero;
        g.transform.localScale = new Vector3(1, 1, 1);
        Text txt = g.GetComponent<Text>();
        txt.font = Font.CreateDynamicFontFromOSFont("Arial", 120);
        txt.alignment = TextAnchor.MiddleCenter;
        txt.fontSize = 30;
        return g;
    }
    //FACE TOWARDS THE PLAYER
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape) && Block_Records.GetCost(block.id).advancedGUI) LifeTime = 0;
        if (Input.GetKey(KeyCode.Return) && panel != null) panel.transform.GetChild(panel.transform.childCount - 1).GetComponent<Button>().onClick.Invoke();

        if (canvas != null) canvas.transform.rotation = Character_Modify_Terrain.self.transform.rotation;
        if (LifeTime == 0)
        {
            Destroy(canvas);
            Destroy(this);
        }
        if (block.id == Block_Records.ID_BUILDER && panel != null)
        {
            InputField txt = panel.transform.GetChild(0).GetComponent<InputField>();
            string path = Directory.GetCurrentDirectory() + @"\Assets\Resources\Blueprints\" + txt.text + ".bytes";
            if (File.Exists(path)) txt.textComponent.color = Color.green;
            else txt.textComponent.color = Color.red;
        }
        LifeTime--;
    }
    void OnDestroy()
    {
        Destroy(canvas);
    }
}
