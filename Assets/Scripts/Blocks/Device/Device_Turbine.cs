﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Device_Turbine : Device_Block {
	// Use this for initialization
	void Start () {
        Transform current = transform;
        while (true)
        {
            if (current.parent == null) { Debug.Log("failed to get the world"); break; }
            current = current.parent;
            if (current.GetComponent<World>() == null) continue;
            world = current.GetComponent<World>(); break;
        }
        if(world.rb != null) Debug.Log("center: " + world.rb.centerOfMass);
	}
	
	// Update is called once per frame
	void Update () {
        if (world.rb == null) return;
                
        if (Input.GetKey(KeyCode.T))
        {
            world.rb.AddForceAtPosition(world.transform.forward, transform.position, ForceMode.Impulse);
        }
	}
    
    void OnDestroy()
    {
        V3I pos = new V3I(transform.localPosition + transform.parent.localPosition);
        Block block = world.GetBlock(pos.x, pos.y, pos.z);
        //Debug.Log(block);
        for (int x = -1; x < 2; x++)
            for (int y = -1; y < 2; y++)
                for (int z = -1; z < 2; z++)
                    if (world.GetBlock(pos.x + x, pos.y + y, pos.z + z) != null && world.GetBlock(pos.x + x, pos.y + y, pos.z + z).model == block.model)
                        world.SetBlock(pos.x + x, pos.y + y, pos.z + z, null);
        world.RebuildWorld();
    }


    void DrawT(Vector3 loc) {
        Debug.DrawLine(loc + new Vector3(0, 0.25f, 0), loc + new Vector3(0, -0.25f, 0), Color.yellow);
        Debug.DrawLine(loc + new Vector3(0.25f, 0, 0), loc + new Vector3(-0.25f, 0, 0), Color.yellow);
        Debug.DrawLine(loc + new Vector3(0, 0, 0.25f), loc + new Vector3(0, 0, -0.25f), Color.yellow);
    }
}
