﻿using UnityEngine;

public class CubePole
{
    private static int Data;
    private static int Dmg;
    private static bool[] RF;
    private static Vector3 mod1;
    private static Vector3 mod2;
    static Color colour;


    public static MeshData blockData(Chunk chunk, int x, int y, int z, MeshData meshData, bool isTrans, Vector2[][] UV1, Vector2[] UV2, bool[] rf)
    {
        RF = rf;
        mod1 = Mod(chunk, x, y, z, rf, false);
        mod2 = Mod(chunk, x, y, z, rf, true);        
        if (RF[0])
            meshData = FaceDataDown(chunk, x, y, z, meshData, UV1[0], UV2, isTrans);
        if (RF[1])
            meshData = FaceDataUp(chunk, x, y, z, meshData, UV1[1], UV2, isTrans);
        if (RF[2])
            meshData = FaceDataNorth(chunk, x, y, z, meshData, UV1[2], UV2, isTrans);
        if (RF[3])
            meshData = FaceDataSouth(chunk, x, y, z, meshData, UV1[3], UV2, isTrans);
        if (RF[4])
            meshData = FaceDataEast(chunk, x, y, z, meshData, UV1[4], UV2, isTrans);
        if (RF[5])
            meshData = FaceDataWest(chunk, x, y, z, meshData, UV1[5], UV2, isTrans);
        return meshData;
    }
    //FENCES/WALLS
    static Vector3 Mod(Chunk chunk, int x, int y, int z, bool[] rf, bool route = false)
    {
        float Xmod = 1, Ymod = 1, Zmod = 1;
        bool Dynamic = false, Wall = false;
        Block block = chunk.world.GetBlock(chunk.pos.x + x, chunk.pos.y + y, chunk.pos.z + z);
        colour = Block_ColorPalette.colPal[block.tint];
        if (colour == Color.black) colour = Block_ColorPalette.colPal[0];
        if (block != null)
        {
            Data = World.GetDmgData(block.dmg, false);
            if (Data == 2)
            {
                float size = ((float) Methods.boolToByte(Methods.byteToBool(block.form), 4, 0) + 1) / 32;
                RF = new bool[] { true, true, true, true, true, true };
                switch (Methods.boolToByte(Methods.byteToBool(block.form), 1, 5))
                {
                    case 1: Ymod = size; Zmod = size; RF[4] = rf[4]; RF[5] = rf[5]; break;
                    case 2: Xmod = size; Zmod = size; RF[0] = rf[0]; RF[1] = rf[1]; break;
                    case 3: Xmod = size; Ymod = size; RF[2] = rf[2]; RF[3] = rf[3]; break;
                    case 4: Dynamic = true; Wall = true; break;
                    case 5: Xmod = size; Wall = true; RF[2] = rf[2]; RF[3] = rf[3]; RF[0] = rf[0]; RF[1] = rf[1]; break;
                    case 6: Ymod = size; Wall = true; RF[2] = rf[2]; RF[3] = rf[3]; RF[4] = rf[4]; RF[5] = rf[5]; break;
                    case 7: Zmod = size; Wall = true; RF[0] = rf[0]; RF[1] = rf[1]; RF[4] = rf[4]; RF[5] = rf[5]; break;
                    default: Dynamic = true; break;
                }
                if (Dynamic)
                {
                    Xmod = size; Ymod = size; Zmod = size;
                    if (Wall)
                    {
                        if ((!rf[1] || !rf[2] || !rf[4]) && !route)
                        {
                            if (!rf[4]) { Xmod = 1; }
                            if (!rf[1]) { Ymod = 1; }
                            if (!rf[2]) { Zmod = 1; }
                        }
                        else if ((!rf[0] || !rf[3] || !rf[5]) && route)
                        {
                            if (!rf[5]) { Xmod = 1; }
                            if (!rf[0]) { Ymod = 1; }
                            if (!rf[3]) { Zmod = 1; }
                        }
                    }
                    else if (!Wall)
                    {
                        if (!rf[4] || !rf[5]) { Xmod = 1; }
                        if (!rf[1] || !rf[0]) { Ymod = 1; }
                        if (!rf[2] || !rf[3]) { Zmod = 1; }
                    }
                }
            }
        }
        return new Vector3(Xmod, Ymod, Zmod);
    }
    //UV MAPPING
    public static Vector2[] FaceUVs(Vector2 tilePos, float modX = 1, float modY = 1)
    {
        float hts = Block.TILE_SIZE / 2;
        Vector2 pos = new Vector2(Block.TILE_SIZE * tilePos.x + hts, Block.TILE_SIZE * tilePos.y + hts);
        Vector2[] UVs = new Vector2[4];
        UVs[0] = new Vector2(pos.x + hts * modX, pos.y - hts * modY);
        UVs[1] = new Vector2(pos.x + hts * modX, pos.y + hts * modY);
        UVs[2] = new Vector2(pos.x - hts * modX, pos.y + hts * modY);
        UVs[3] = new Vector2(pos.x - hts * modX, pos.y - hts * modY);
        return UVs;
    }

    //VERTEXS OF FACES
    public static MeshData FaceDataUp(Chunk chunk, int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans)
    {
        
        Block block = chunk.world.GetBlock(chunk.pos.x + x, chunk.pos.y + y, chunk.pos.z + z);
        if (Data == 2 && block != null)
            uv = FaceUVs(block.TexturePosition(Direction.up), (mod1.z + mod2.z) / 2 , (mod1.x + mod2.x) / 2 );

        bool solid = (block == null || block.id != Block_Records.ID_HOLOGRAM);
        meshData.AddVertex(new Vector3(x - 0.5f * mod2.x, y + 0.5f * mod1.y, z + 0.5f * mod1.z), colour, solid);
        meshData.AddVertex(new Vector3(x + 0.5f * mod1.x, y + 0.5f * mod1.y, z + 0.5f * mod1.z), colour, solid);
        meshData.AddVertex(new Vector3(x + 0.5f * mod1.x, y + 0.5f * mod1.y, z - 0.5f * mod2.z), colour, solid);
        meshData.AddVertex(new Vector3(x - 0.5f * mod2.x, y + 0.5f * mod1.y, z - 0.5f * mod2.z), colour, solid);
        meshData.AddQuadTriangles(trans, 4, 2, solid);
        meshData.uv.AddRange(uv);
        meshData.uv2.AddRange(uv2);
        return meshData;
    }
    public static MeshData FaceDataDown(Chunk chunk, int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans)
    {

        Block block = chunk.world.GetBlock(chunk.pos.x + x, chunk.pos.y + y, chunk.pos.z + z);
        if (Data == 2 && block != null)
            uv = FaceUVs(block.TexturePosition(Direction.down), (mod1.z + mod2.z) / 2 , (mod1.x + mod2.x) / 2 );

        bool solid = (block == null || block.id != Block_Records.ID_HOLOGRAM);
        meshData.AddVertex(new Vector3(x - 0.5f * mod2.x, y - 0.5f * mod2.y, z - 0.5f * mod2.z), colour, solid);
        meshData.AddVertex(new Vector3(x + 0.5f * mod1.x, y - 0.5f * mod2.y, z - 0.5f * mod2.z), colour, solid);
        meshData.AddVertex(new Vector3(x + 0.5f * mod1.x, y - 0.5f * mod2.y, z + 0.5f * mod1.z), colour, solid);
        meshData.AddVertex(new Vector3(x - 0.5f * mod2.x, y - 0.5f * mod2.y, z + 0.5f * mod1.z), colour, solid);
        meshData.AddQuadTriangles(trans, 4, 2, solid);
        meshData.uv.AddRange(uv);
        meshData.uv2.AddRange(uv2);
        return meshData;
    }
    public static MeshData FaceDataNorth(Chunk chunk, int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans)
    {

        Block block = chunk.world.GetBlock(chunk.pos.x + x, chunk.pos.y + y, chunk.pos.z + z);
        if (Data == 2 && block != null)
            uv = FaceUVs(block.TexturePosition(Direction.north), (mod1.x + mod2.x) / 2 , (mod1.y + mod2.y) / 2 );

        bool solid = (block == null || block.id != Block_Records.ID_HOLOGRAM);
        meshData.AddVertex(new Vector3(x + 0.5f * mod1.x, y - 0.5f * mod2.y, z + 0.5f * mod1.z), colour, solid);
        meshData.AddVertex(new Vector3(x + 0.5f * mod1.x, y + 0.5f * mod1.y, z + 0.5f * mod1.z), colour, solid);
        meshData.AddVertex(new Vector3(x - 0.5f * mod2.x, y + 0.5f * mod1.y, z + 0.5f * mod1.z), colour, solid);
        meshData.AddVertex(new Vector3(x - 0.5f * mod2.x, y - 0.5f * mod2.y, z + 0.5f * mod1.z), colour, solid);
        meshData.AddQuadTriangles(trans, 4, 2, solid);
        meshData.uv.AddRange(uv);
        meshData.uv2.AddRange(uv2);
        return meshData;
    }
    public static MeshData FaceDataSouth(Chunk chunk, int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans)
    {

        Block block = chunk.world.GetBlock(chunk.pos.x + x, chunk.pos.y + y, chunk.pos.z + z);
        if (Data == 2 && block != null)
            uv = FaceUVs(block.TexturePosition(Direction.south), (mod1.x + mod2.x) / 2, (mod1.y + mod2.y) / 2);

        bool solid = (block == null || block.id != Block_Records.ID_HOLOGRAM);
        meshData.AddVertex(new Vector3(x - 0.5f * mod2.x, y - 0.5f * mod2.y, z - 0.5f * mod2.z), colour, solid);
        meshData.AddVertex(new Vector3(x - 0.5f * mod2.x, y + 0.5f * mod1.y, z - 0.5f * mod2.z), colour, solid);
        meshData.AddVertex(new Vector3(x + 0.5f * mod1.x, y + 0.5f * mod1.y, z - 0.5f * mod2.z), colour, solid);
        meshData.AddVertex(new Vector3(x + 0.5f * mod1.x, y - 0.5f * mod2.y, z - 0.5f * mod2.z), colour, solid);
        meshData.AddQuadTriangles(trans, 4, 2, solid);
        meshData.uv.AddRange(uv);
        meshData.uv2.AddRange(uv2);
        return meshData;
    }
    public static MeshData FaceDataEast(Chunk chunk, int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans)
    {

        Block block = chunk.world.GetBlock(chunk.pos.x + x, chunk.pos.y + y, chunk.pos.z + z);
        if (Data == 2 && block != null)
            uv = FaceUVs(block.TexturePosition(Direction.east), (mod1.z + mod2.z) / 2, (mod1.y + mod2.y) / 2);

        bool solid = (block == null || block.id != Block_Records.ID_HOLOGRAM);
        meshData.AddVertex(new Vector3(x + 0.5f * mod1.x, y - 0.5f * mod2.y, z - 0.5f * mod2.z), colour, solid);
        meshData.AddVertex(new Vector3(x + 0.5f * mod1.x, y + 0.5f * mod1.y, z - 0.5f * mod2.z), colour, solid);
        meshData.AddVertex(new Vector3(x + 0.5f * mod1.x, y + 0.5f * mod1.y, z + 0.5f * mod1.z), colour, solid);
        meshData.AddVertex(new Vector3(x + 0.5f * mod1.x, y - 0.5f * mod2.y, z + 0.5f * mod1.z), colour, solid);
        meshData.AddQuadTriangles(trans, 4, 2, solid);
        meshData.uv.AddRange(uv);
        meshData.uv2.AddRange(uv2);
        return meshData;
    }
    public static MeshData FaceDataWest(Chunk chunk, int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans)
    {

        Block block = chunk.world.GetBlock(chunk.pos.x + x, chunk.pos.y + y, chunk.pos.z + z);
        if (Data == 2 && block != null)
            uv = FaceUVs(block.TexturePosition(Direction.west), (mod1.z + mod2.z) / 2, (mod1.y + mod2.y) / 2);

        bool solid = (block == null || block.id != Block_Records.ID_HOLOGRAM);
        meshData.AddVertex(new Vector3(x - 0.5f * mod2.x, y - 0.5f * mod2.y, z + 0.5f * mod1.z), colour, solid);
        meshData.AddVertex(new Vector3(x - 0.5f * mod2.x, y + 0.5f * mod1.y, z + 0.5f * mod1.z), colour, solid);
        meshData.AddVertex(new Vector3(x - 0.5f * mod2.x, y + 0.5f * mod1.y, z - 0.5f * mod2.z), colour, solid);
        meshData.AddVertex(new Vector3(x - 0.5f * mod2.x, y - 0.5f * mod2.y, z - 0.5f * mod2.z), colour, solid);
        meshData.AddQuadTriangles(trans, 4, 2, solid);
        meshData.uv.AddRange(uv);
        meshData.uv2.AddRange(uv2);
        return meshData;
    }
}