﻿using UnityEngine;

public class Frame
{
    public static MeshData FaceDataExtra(int x, int y, int z, MeshData meshData, bool trans, Vector2[][] UVS)
    {
        Vector2[] noDmg = new Vector2[] { new Vector2(0.25f,0), new Vector2(0.25f, 0.25f), new Vector2(0, 0.25f), new Vector2(0, 0) };

        Color colour = Block_ColorPalette.colPal[0];

        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.25f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.25f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.25f, z - 0.5f), colour);
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.25f, z - 0.5f), colour);
        meshData.AddQuadTriangles(trans); meshData.uv.AddRange(UVS[0]); meshData.uv2.AddRange(noDmg);

        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.25f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.25f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.25f, z - 0.5f), colour);
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.25f, z - 0.5f), colour);
        meshData.AddQuadTriangles(trans); meshData.uv.AddRange(UVS[1]); meshData.uv2.AddRange(noDmg);

        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.25f), colour);
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.25f), colour);
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.25f), colour);
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.25f), colour);
        meshData.AddQuadTriangles(trans); meshData.uv.AddRange(UVS[2]); meshData.uv2.AddRange(noDmg);

        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.25f), colour);
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.25f), colour);
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.25f), colour);
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.25f), colour);
        meshData.AddQuadTriangles(trans); meshData.uv.AddRange(UVS[3]); meshData.uv2.AddRange(noDmg);

        meshData.AddVertex(new Vector3(x - 0.25f, y - 0.5f, z - 0.5f), colour);
        meshData.AddVertex(new Vector3(x - 0.25f, y + 0.5f, z - 0.5f), colour);
        meshData.AddVertex(new Vector3(x - 0.25f, y + 0.5f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x - 0.25f, y - 0.5f, z + 0.5f), colour);
        meshData.AddQuadTriangles(trans); meshData.uv.AddRange(UVS[4]); meshData.uv2.AddRange(noDmg);

        meshData.AddVertex(new Vector3(x + 0.25f, y - 0.5f, z - 0.5f), colour);
        meshData.AddVertex(new Vector3(x + 0.25f, y + 0.5f, z - 0.5f), colour);
        meshData.AddVertex(new Vector3(x + 0.25f, y + 0.5f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x + 0.25f, y - 0.5f, z + 0.5f), colour);
        meshData.AddQuadTriangles(trans); meshData.uv.AddRange(UVS[5]); meshData.uv2.AddRange(noDmg);

        return meshData;
    }
}
