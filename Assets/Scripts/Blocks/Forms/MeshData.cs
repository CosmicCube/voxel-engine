﻿using UnityEngine;
using System.Collections.Generic;

public class MeshData
{
    //vertices
    public List<Vector3> vertices = new List<Vector3>();
    public List<Vector3> colisionVerts = new List<Vector3>();
    public List<Color> vertColours = new List<Color>();
    //triangles
    public List<int> triangles = new List<int>();
    public List<int> trianglesTrans = new List<int>();
    public List<int> colTriangles = new List<int>();
    //UV maps
    public List<Vector2> uv = new List<Vector2>();
    public List<Vector2> uv2 = new List<Vector2>();
    
    public void AddSlopeTriangles(bool trans, int i1 = 1, int i2 = 2, bool collidable = true)
    {
        if (!trans)
        {
            triangles.Add(vertices.Count - 3);
            triangles.Add(vertices.Count - i1);
            triangles.Add(vertices.Count - i2);
        }
        else
        {
            trianglesTrans.Add(vertices.Count - 3);
            trianglesTrans.Add(vertices.Count - i1);
            trianglesTrans.Add(vertices.Count - i2);
        }
        if (collidable)
        {
            colTriangles.Add(colisionVerts.Count - 3);
            colTriangles.Add(colisionVerts.Count - i1);
            colTriangles.Add(colisionVerts.Count - i2);
        }
    }
    public void AddQuadTriangles(bool trans, int i1 = 4, int i2 = 2, bool collidable = true)
    {
        //trans = false;
        if (!trans)
        {
            triangles.Add(vertices.Count - i1);
            triangles.Add(vertices.Count - 3);
            triangles.Add(vertices.Count - i2);
            triangles.Add(vertices.Count - i1);
            triangles.Add(vertices.Count - i2);
            triangles.Add(vertices.Count - 1);
        }
        else
        {
            trianglesTrans.Add(vertices.Count - i1);
            trianglesTrans.Add(vertices.Count - 3);
            trianglesTrans.Add(vertices.Count - i2);
            trianglesTrans.Add(vertices.Count - i1);
            trianglesTrans.Add(vertices.Count - i2);
            trianglesTrans.Add(vertices.Count - 1);
        }

        if (collidable)
        {
            colTriangles.Add(colisionVerts.Count - i1);
            colTriangles.Add(colisionVerts.Count - 3);
            colTriangles.Add(colisionVerts.Count - i2);
            colTriangles.Add(colisionVerts.Count - i1);
            colTriangles.Add(colisionVerts.Count - i2);
            colTriangles.Add(colisionVerts.Count - 1);
        }
    }
    public void AddVertex(Vector3 vertex, Color color, bool collidable = true)
    {
        vertices.Add(vertex);
        vertColours.Add(color);
        if (collidable)
        {
            colisionVerts.Add(vertex);
        }
    }
}
