﻿using UnityEngine;
using System.Collections.Generic;

public class Slope
{
    static Color colour;
    public static MeshData blockData(Chunk chunk, int x, int y, int z, MeshData meshData, bool isTrans, Vector2[][] UV1, Vector2[] UV2, bool[] crn)
    {
        Block block = chunk.GetBlock(x, y, z);
        if (block != null) colour = Block_ColorPalette.colPal[block.tint];
        if (colour == Color.black) colour = Block_ColorPalette.colPal[0];

        if (!Block.burried(chunk, x, y, z) && Methods.boolToInt(crn, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 }) >= 4)
        {
            //create triangle faces
            if (Methods.boolToInt(crn, new int[] { 0, 1, 2, 3 }) == 3)
                meshData = FaceDataUp(x, y, z, meshData, UV1[1], UV2, isTrans, crn, 2, 1);
            if (Methods.boolToInt(crn, new int[] { 4, 5, 6, 7 }) == 3)
                meshData = FaceDataDown(x, y, z, meshData, UV1[0], UV2, isTrans, crn);
            if (Methods.boolToInt(crn, new int[] { 0, 1, 4, 5 }) == 3)
                meshData = FaceDataNorth(x, y, z, meshData, UV1[2], UV2, isTrans, crn);
            if (Methods.boolToInt(crn, new int[] { 2, 3, 6, 7 }) == 3)
                meshData = FaceDataSouth(x, y, z, meshData, UV1[3], UV2, isTrans, crn);
            if (Methods.boolToInt(crn, new int[] { 1, 3, 5, 7 }) == 3)
                meshData = FaceDataEast(x, y, z, meshData, UV1[2], UV2, isTrans, crn, 2, 1);
            if (Methods.boolToInt(crn, new int[] { 0, 2, 4, 6 }) == 3)
                meshData = FaceDataWest(x, y, z, meshData, UV1[3], UV2, isTrans, crn);

            //create sloped faces / tris
            if (Methods.boolToInt(crn, new int[] { 1, 2, 5, 6 }) >= 3 && ((!crn[0] && !crn[4]) || (!crn[3] && !crn[7])))
                meshData = FaceData1256(x, y, z, meshData, UV1[3], UV2, isTrans, crn);
            if (Methods.boolToInt(crn, new int[] { 0, 3, 4, 7 }) >= 3 && ((!crn[1] && !crn[5]) || (!crn[2] && !crn[6])))
                meshData = FaceData0347(x, y, z, meshData, UV1[3], UV2, isTrans, crn);
            if (Methods.boolToInt(crn, new int[] { 0, 1, 6, 7 }) >= 3 && ((!crn[2] && !crn[3]) || (!crn[4] && !crn[5])))
                if (!crn[2] && !crn[3]) meshData = FaceData0167(x, y, z, meshData, UV1[1], UV2, isTrans, crn);
                else meshData = FaceData0167(x, y, z, meshData, UV1[0], UV2, isTrans, crn);
            if (Methods.boolToInt(crn, new int[] { 2, 3, 4, 5 }) >= 3 && ((!crn[0] && !crn[1]) || (!crn[6] && !crn[7])))
                if (!crn[0] && !crn[1]) meshData = FaceData2345(x, y, z, meshData, UV1[1], UV2, isTrans, crn);
                else meshData = FaceData2345(x, y, z, meshData, UV1[0], UV2, isTrans, crn);
            if (Methods.boolToInt(crn, new int[] { 0, 2, 5, 7 }) >= 3 && ((!crn[1] && !crn[3]) || (!crn[4] && !crn[6])))
                if (!crn[1] && !crn[3]) meshData = FaceData0257(x, y, z, meshData, UV1[1], UV2, isTrans, crn);
                else meshData = FaceData0257(x, y, z, meshData, UV1[0], UV2, isTrans, crn);
            if (Methods.boolToInt(crn, new int[] { 1, 3, 4, 6 }) >= 3 && ((!crn[0] && !crn[2]) || (!crn[5] && !crn[7])))
                if (!crn[0] && !crn[2]) meshData = FaceData1346(x, y, z, meshData, UV1[1], UV2, isTrans, crn);
                else meshData = FaceData1346(x, y, z, meshData, UV1[0], UV2, isTrans, crn);

            //create filler triangles
            //Block block = chunk.world.GetBlock(chunk.pos.x + x, chunk.pos.y + y, chunk.pos.z + z);
            if (crn[1] && crn[4] && crn[7] && (!crn[5] || (!crn[2] && !crn[0] && !crn[3] && !crn[6])))
                FaceData147(x, y, z, meshData, block.TriUVs(block.TexturePosition(), Block.TILE_SIZE, 1, 5, 7), block.TriUVs(block.DmgValuePosition(), Block.TILE_DMG, 1, 5, 7), isTrans, crn);
            if (crn[2] && crn[4] && crn[7] && (!crn[6] || (!crn[1] && !crn[0] && !crn[3] && !crn[5])))
                FaceData247(x, y, z, meshData, block.TriUVs(block.TexturePosition(), Block.TILE_SIZE, 1, 5, 7), block.TriUVs(block.DmgValuePosition(), Block.TILE_DMG, 1, 5, 7), isTrans, crn);
            if (crn[0] && crn[5] && crn[6] && (!crn[4] || (!crn[3] && !crn[1] && !crn[2] && !crn[7])))
                FaceData056(x, y, z, meshData, block.TriUVs(block.TexturePosition(), Block.TILE_SIZE, 1, 5, 7), block.TriUVs(block.DmgValuePosition(), Block.TILE_DMG, 1, 5, 7), isTrans, crn);
            if (crn[3] && crn[5] && crn[6] && (!crn[7] || (!crn[0] && !crn[1] && !crn[2] && !crn[4])))
                FaceData356(x, y, z, meshData, block.TriUVs(block.TexturePosition(), Block.TILE_SIZE, 1, 5, 7), block.TriUVs(block.DmgValuePosition(), Block.TILE_DMG, 1, 5, 7), isTrans, crn);

            if (crn[5] && crn[0] && crn[3] && (!crn[1] || (!crn[6] && !crn[4] && !crn[7] && !crn[2])))
                FaceData503(x, y, z, meshData, block.TriUVs(block.TexturePosition(), Block.TILE_SIZE, 6, 0, 2), block.TriUVs(block.DmgValuePosition(), Block.TILE_DMG, 6, 0, 2), isTrans, crn);
            if (crn[6] && crn[0] && crn[3] && (!crn[2] || (!crn[5] && !crn[4] && !crn[7] && !crn[1])))
                FaceData603(x, y, z, meshData, block.TriUVs(block.TexturePosition(), Block.TILE_SIZE, 6, 0, 2), block.TriUVs(block.DmgValuePosition(), Block.TILE_DMG, 6, 0, 2), isTrans, crn);
            if (crn[4] && crn[1] && crn[2] && (!crn[0] || (!crn[7] && !crn[5] && !crn[6] && !crn[3])))
                FaceData412(x, y, z, meshData, block.TriUVs(block.TexturePosition(), Block.TILE_SIZE, 6, 0, 2), block.TriUVs(block.DmgValuePosition(), Block.TILE_DMG, 6, 0, 2), isTrans, crn);
            if (crn[7] && crn[1] && crn[2] && (!crn[3] || (!crn[4] && !crn[5] && !crn[6] && !crn[0])))
                FaceData712(x, y, z, meshData, block.TriUVs(block.TexturePosition(), Block.TILE_SIZE, 6, 0, 2), block.TriUVs(block.DmgValuePosition(), Block.TILE_DMG, 6, 0, 2), isTrans, crn);
        }
        return meshData;
    }

    //tri-faces
    public static MeshData FaceDataUp(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn, int i1 = 1, int i2 = 2)
    { 
        if (crn[0]){
            meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f), colour);
            meshData.uv.Add(uv[0]); meshData.uv2.Add(uv2[0]); 
        }
        if (crn[1]){
            meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f), colour);
            meshData.uv.Add(uv[1]); meshData.uv2.Add(uv2[1]);
        }
        if (crn[3]){
            meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f), colour);
            meshData.uv.Add(uv[2]); meshData.uv2.Add(uv2[2]);
        }
        if (crn[2]){
            meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f), colour);
            meshData.uv.Add(uv[3]); meshData.uv2.Add(uv2[3]);
        }
        meshData.AddSlopeTriangles(trans, i1, i2); 
        return meshData;
    }
    public static MeshData FaceDataDown(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn, int i1 = 1, int i2 = 2)
    {
        if (crn[4])
        {
            meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f), colour);
            meshData.uv.Add(uv[0]); meshData.uv2.Add(uv2[0]);
        }
        if (crn[5])
        {
            meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f), colour);
            meshData.uv.Add(uv[1]); meshData.uv2.Add(uv2[1]);
        }
        if (crn[7])
        {
            meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f), colour);
            meshData.uv.Add(uv[2]); meshData.uv2.Add(uv2[2]);
        }
        if (crn[6])
        {
            meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f), colour);
            meshData.uv.Add(uv[3]); meshData.uv2.Add(uv2[3]);
        }
        meshData.AddSlopeTriangles(trans, i1, i2); 
        return meshData;
    }
    public static MeshData FaceDataNorth(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn, int i1 = 1, int i2 = 2)
    {
 

        if (crn[0])
        {
            meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f), colour);
            meshData.uv.Add(uv[1]); meshData.uv2.Add(uv2[1]);
        }
        if (crn[1])
        {
            meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f), colour);
            meshData.uv.Add(uv[2]); meshData.uv2.Add(uv2[2]);
        }
        if (crn[5])
        {
            meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f), colour);
            meshData.uv.Add(uv[3]); meshData.uv2.Add(uv2[3]);
        }
        if (crn[4])
        {
            meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f), colour);
            meshData.uv.Add(uv[0]); meshData.uv2.Add(uv2[0]);
        }
        if (crn[4] && crn[5]) meshData.AddSlopeTriangles(trans, i1, i2);
        else meshData.AddSlopeTriangles(trans, i1, i2);
 
        return meshData;
    }
    public static MeshData FaceDataSouth(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn, int i1 = 1, int i2 = 2)
    {
 

        if (crn[2])
        {
            meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f), colour);
            meshData.uv.Add(uv[2]); meshData.uv2.Add(uv2[2]);
        }
        if (crn[3])
        {
            meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f), colour);
            meshData.uv.Add(uv[1]); meshData.uv2.Add(uv2[1]);
        }
        if (crn[6])
        {
            meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f), colour);
            meshData.uv.Add(uv[3]); meshData.uv2.Add(uv2[3]);
        }
        if (crn[7])
        {
            meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f), colour);
            meshData.uv.Add(uv[0]); meshData.uv2.Add(uv2[0]);
        }
        if (crn[6] && crn[7]) meshData.AddSlopeTriangles(trans, i1, i2);
        else meshData.AddSlopeTriangles(trans, i2, i1);
 
        return meshData;
    }
    public static MeshData FaceDataEast(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn, int i1 = 1, int i2 = 2)
    {
 

        if (crn[1])
        {
            meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f), colour);            
            meshData.uv.Add(uv[1]); meshData.uv2.Add(uv2[1]);
        }
        if (crn[3])
        {
            meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f), colour);
            meshData.uv.Add(uv[2]); meshData.uv2.Add(uv2[2]);
        }
        if (crn[5])
        {
            meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f), colour);
            meshData.uv.Add(uv[0]); meshData.uv2.Add(uv2[0]);
        }
        if (crn[7])
        {
            meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f), colour);
            meshData.uv.Add(uv[3]); meshData.uv2.Add(uv2[3]);
        }
        if (crn[5] && crn[7]) meshData.AddSlopeTriangles(trans, i1, i2);
        else meshData.AddSlopeTriangles(trans, i2, i1);
 
        return meshData;
    }
    public static MeshData FaceDataWest(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn, int i1 = 1, int i2 = 2)
    {
 

        if (crn[0])
        {
            meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f), colour);
            meshData.uv.Add(uv[1]); meshData.uv2.Add(uv2[1]);
        }
        if (crn[2])
        {
            meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f), colour);
            meshData.uv.Add(uv[2]); meshData.uv2.Add(uv2[2]);
        }
        if (crn[4])
        {
            meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f), colour);
            meshData.uv.Add(uv[0]); meshData.uv2.Add(uv2[0]);
        }
        if (crn[6])
        {
            meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f), colour);
            meshData.uv.Add(uv[3]); meshData.uv2.Add(uv2[3]);
        }
        if (crn[4] && crn[6]) meshData.AddSlopeTriangles(trans, i1, i2);
        else meshData.AddSlopeTriangles(trans, i2, i1);
 
        return meshData;
    }

    //slope faces
    public static MeshData FaceData1256(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn)
    {
 
        int i1 = 2, i2 = 1,  i3 = 4, i4 = 2; if(!crn[3] && !crn[7]) { i1 = 1; i2 = 2; i3 = 2; i4 = 4; }
        if (crn[5]) { meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f), colour); meshData.uv.Add(uv[0]); meshData.uv2.Add(uv2[0]); }
        if (crn[1]) { meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f), colour); meshData.uv.Add(uv[1]); meshData.uv2.Add(uv2[1]); }
        if (crn[2]) { meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f), colour); meshData.uv.Add(uv[2]); meshData.uv2.Add(uv2[2]); }
        if (crn[6]) { meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f), colour); meshData.uv.Add(uv[3]); meshData.uv2.Add(uv2[3]); }
        if (Methods.boolToInt(crn, new int[] { 1, 2, 5, 6 }) == 4)
            meshData.AddQuadTriangles(trans, i3, i4); 
        else
            meshData.AddSlopeTriangles(trans, i1, i2);
 
        return meshData;
    }
    public static MeshData FaceData0347(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn)
    {
 
        int i1 = 2, i2 = 1, i3 = 4, i4 = 2; if (!crn[2] && !crn[6]) { i1 = 1; i2 = 2; i3 = 2; i4 = 4; }
        if (crn[7]) { meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f), colour); meshData.uv.Add(uv[0]); meshData.uv2.Add(uv2[0]); }
        if (crn[3]) { meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f), colour); meshData.uv.Add(uv[1]); meshData.uv2.Add(uv2[1]); }
        if (crn[0]) { meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f), colour); meshData.uv.Add(uv[2]); meshData.uv2.Add(uv2[2]); }
        if (crn[4]) { meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f), colour); meshData.uv.Add(uv[3]); meshData.uv2.Add(uv2[3]); }
        if (Methods.boolToInt(crn, new int[] { 0, 3, 4, 7 }) == 4)
            meshData.AddQuadTriangles(trans, i3, i4);
        else
            meshData.AddSlopeTriangles(trans, i1, i2);
 
        return meshData;
    }
    public static MeshData FaceData0167(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn)
    {
 
        int i1 = 2, i2 = 1, i3 = 4, i4 = 2; if (!crn[4] && !crn[5]) { i1 = 1; i2 = 2; i3 = 2; i4 = 4; }
        if (crn[0]) { meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f), colour); meshData.uv.Add(uv[0]); meshData.uv2.Add(uv2[0]); }
        if (crn[1]) { meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f), colour); meshData.uv.Add(uv[1]); meshData.uv2.Add(uv2[1]); }
        if (crn[7]) { meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f), colour); meshData.uv.Add(uv[2]); meshData.uv2.Add(uv2[2]); }
        if (crn[6]) { meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f), colour); meshData.uv.Add(uv[3]); meshData.uv2.Add(uv2[3]); }
        if (Methods.boolToInt(crn, new int[] { 0, 1, 6, 7 }) == 4)
            meshData.AddQuadTriangles(trans, i3, i4);
        else
            meshData.AddSlopeTriangles(trans, i1, i2);
 
        return meshData;
    }
    public static MeshData FaceData2345(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn)
    {
 
        int i1 = 2, i2 = 1, i3 = 4, i4 = 2; if (!crn[6] && !crn[7]) { i1 = 1; i2 = 2; i3 = 2; i4 = 4; }
        if (crn[4]) { meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f), colour); meshData.uv.Add(uv[0]); meshData.uv2.Add(uv2[0]); }
        if (crn[5]) { meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f), colour); meshData.uv.Add(uv[1]); meshData.uv2.Add(uv2[1]); }
        if (crn[3]) { meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f), colour); meshData.uv.Add(uv[2]); meshData.uv2.Add(uv2[2]); }
        if (crn[2]) { meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f), colour); meshData.uv.Add(uv[3]); meshData.uv2.Add(uv2[3]); }
        if (Methods.boolToInt(crn, new int[] { 2, 3, 4, 5 }) == 4)
            meshData.AddQuadTriangles(trans, i3, i4);
        else
            meshData.AddSlopeTriangles(trans, i1, i2);
 
        return meshData;
    }
    public static MeshData FaceData0257(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn)
    {
 
        int i1 = 2, i2 = 1, i3 = 4, i4 = 2; if (!crn[4] && !crn[6]) { i1 = 1; i2 = 2; i3 = 2; i4 = 4; }
        if (crn[0]) { meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f), colour); meshData.uv.Add(uv[0]); meshData.uv2.Add(uv2[0]); }
        if (crn[5]) { meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f), colour); meshData.uv.Add(uv[1]); meshData.uv2.Add(uv2[1]); }
        if (crn[7]) { meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f), colour); meshData.uv.Add(uv[2]); meshData.uv2.Add(uv2[2]); }
        if (crn[2]) { meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f), colour); meshData.uv.Add(uv[3]); meshData.uv2.Add(uv2[3]); }
        if (Methods.boolToInt(crn, new int[] { 0, 2, 5, 7 }) == 4)
            meshData.AddQuadTriangles(trans, i3, i4);
        else
            meshData.AddSlopeTriangles(trans, i1, i2);
 
        return meshData;
    }
    public static MeshData FaceData1346(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn)
    {
 
        int i1 = 2, i2 = 1, i3 = 4, i4 = 2; if (!crn[5] && !crn[7]) { i1 = 1; i2 = 2; i3 = 2; i4 = 4; }
        if (crn[4]) { meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f), colour); meshData.uv.Add(uv[0]); meshData.uv2.Add(uv2[0]); }
        if (crn[1]) { meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f), colour); meshData.uv.Add(uv[1]); meshData.uv2.Add(uv2[1]); }
        if (crn[3]) { meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f), colour); meshData.uv.Add(uv[2]); meshData.uv2.Add(uv2[2]); }
        if (crn[6]) { meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f), colour); meshData.uv.Add(uv[3]); meshData.uv2.Add(uv2[3]); }
        if (Methods.boolToInt(crn, new int[] { 1, 3, 4, 6 }) == 4)
            meshData.AddQuadTriangles(trans, i3, i4);
        else
            meshData.AddSlopeTriangles(trans, i1, i2);
 
        return meshData;
    }

    //filler tri-faces
    public static MeshData FaceData147(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn)
    {
        int i1 = 2, i2 = 1; if (crn[5]) { i1 = 1; i2 = 2; }
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f), colour);
        meshData.AddSlopeTriangles(trans, i1, i2); meshData.uv.AddRange(uv); meshData.uv2.AddRange(uv2);
        return meshData;
    }
    public static MeshData FaceData247(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn)
    {
        int i1 = 1, i2 = 2; if (crn[6]) { i1 = 2; i2 = 1; }
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f), colour);
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f), colour);
        meshData.AddSlopeTriangles(trans, i1, i2); meshData.uv.AddRange(uv); meshData.uv2.AddRange(uv2);
        return meshData;
    }
    public static MeshData FaceData056(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn)
    {
        int i1 = 1, i2 = 2; if (crn[4]) { i1 = 2; i2 = 1; }
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f), colour);
        meshData.AddSlopeTriangles(trans, i1, i2); meshData.uv.AddRange(uv); meshData.uv2.AddRange(uv2);
        return meshData;
    }
    public static MeshData FaceData356(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn)
    {
        int i1 = 2, i2 = 1; if (crn[7]) { i1 = 1; i2 = 2; }
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f), colour);
        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f), colour);
        meshData.AddSlopeTriangles(trans, i1, i2); meshData.uv.AddRange(uv); meshData.uv2.AddRange(uv2);
        return meshData;
    }

    public static MeshData FaceData503(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn)
    {
        int i1 = 1, i2 = 2; if (crn[1]) { i1 = 2; i2 = 1; }
        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f), colour);
        meshData.AddSlopeTriangles(trans, i1, i2); meshData.uv.AddRange(uv); meshData.uv2.AddRange(uv2);
        return meshData;
    }
    public static MeshData FaceData603(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn)
    {
        int i1 = 2, i2 = 1; if (crn[2]) { i1 = 1; i2 = 2; }
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f), colour);
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f), colour);
        meshData.AddSlopeTriangles(trans, i1, i2); meshData.uv.AddRange(uv); meshData.uv2.AddRange(uv2);
        return meshData;
    }
    public static MeshData FaceData412(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn)
    {
        int i1 = 2, i2 = 1; if (crn[0]) { i1 = 1; i2 = 2; }
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f), colour);
        meshData.AddSlopeTriangles(trans, i1, i2); meshData.uv.AddRange(uv); meshData.uv2.AddRange(uv2);
        return meshData;
    }
    public static MeshData FaceData712(int x, int y, int z, MeshData meshData, Vector2[] uv, Vector2[] uv2, bool trans, bool[] crn)
    {
        int i1 = 1, i2 = 2; if (crn[3]) { i1 = 2; i2 = 1; }
        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f), colour);
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f), colour);
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f), colour);
        meshData.AddSlopeTriangles(trans, i1, i2); meshData.uv.AddRange(uv); meshData.uv2.AddRange(uv2);
        return meshData;
    }
}