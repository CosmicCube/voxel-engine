﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class World : MonoBehaviour {
    private Chunk[,,] chunks;
    public static World main;
    public string Size;
    public V3I size;
    public int Key;
    public Rigidbody rb;


    //SETS THE CHUNKS FOR THIS REGION OF THE WORLD
    public void WorldInit(Chunk[,,] chunks, V3I s)
    {
        if (main == null) main = this;
        this.chunks = chunks;
        size = s;
        Size = s.ToString();
    }

    //CONSTRUCTS THE DISPLAY MESH
    public void RebuildWorld(bool all = false)
    {
        foreach (Chunk chunk in chunks)
        {
            if (chunk == null || !chunk.dirty) continue;
            chunk.Rebuild();
            chunk.dirty = false;
        }
        if (all)
        {
            CCLInit();
            CCLInitialScan();
        }

        //GameManager.Find().RemoveLoadingScene();
    }
    public void DamageBlock(V3I loc, byte dmg)
    {
        int chunkX = loc.x / Chunk.CHUNK_SIZE;
        int chunkY = loc.y / Chunk.CHUNK_SIZE;
        int chunkZ = loc.z / Chunk.CHUNK_SIZE;

        Chunk chunk = GetChunk(chunkX, chunkY, chunkZ);
        if (chunk == null) return;
        
        int blockX = loc.x - (chunkX * Chunk.CHUNK_SIZE);
        int blockY = loc.y - (chunkY * Chunk.CHUNK_SIZE);
        int blockZ = loc.z - (chunkZ * Chunk.CHUNK_SIZE);

        Block block = chunk.GetBlock(blockX, blockY, blockZ);
        if (block == null) return;

        if((dmg * 10 + block.dmg) < 250)
        {
            block.dmg = (byte)(dmg * 10 + block.dmg);
            chunk.SetBlock(blockX, blockY, blockZ, block);
            chunk.dirty = true;
        }
        else        
            SetBlockFast(loc.x, loc.y, loc.z, null);        
    }

    //PLACE BLOCK AT THE GIVEN LOCATION
    public void SetBlock(int x, int y, int z, Block block)
    {
        Block blk = GetBlock(x, y, z);
        CCLComponent removeComponent = null;

        if (block == null && blk != null)
            removeComponent = blk.GetCCLComponent();

        SetBlockFast(x, y, z, block);

        if (removeComponent != null)
            CCLRemoveBlock(x, y, z, removeComponent);
        if (block != null)
            CCLAddBlock(x, y, z, block);
    }
    public void SetBlockFast(int x, int y, int z, Block block)
    {
        Block blk = GetBlock(x, y, z);
        if (blk != null && blk.model != null)
            Destroy(blk.model);
        if (block != null && block.id == 0)
            block = null;

        int chunkX = x / Chunk.CHUNK_SIZE;
        int chunkY = y / Chunk.CHUNK_SIZE;
        int chunkZ = z / Chunk.CHUNK_SIZE;

        int blockX = x - (chunkX * Chunk.CHUNK_SIZE);
        int blockY = y - (chunkY * Chunk.CHUNK_SIZE);
        int blockZ = z - (chunkZ * Chunk.CHUNK_SIZE);

        Chunk cnk = GetChunk(chunkX, chunkY, chunkZ);
        if (cnk == null) { cnk = main.GetChunk(chunkX, chunkY, chunkZ); return; }
        if (cnk == null) { Debug.Log("no chunk error"); return; }
        
        cnk.SetBlock(blockX, blockY, blockZ, block);
        cnk.dirty = true;

        if (blockX % Chunk.CHUNK_SIZE == 0)
            NeighborDirty(GetChunk(chunkX - 1, chunkY, chunkZ));
        if (blockY % Chunk.CHUNK_SIZE == 0)
            NeighborDirty(GetChunk(chunkX, chunkY - 1, chunkZ));
        if (blockZ % Chunk.CHUNK_SIZE == 0)
            NeighborDirty(GetChunk(chunkX, chunkY, chunkZ - 1));

        if ((blockX + 1) % Chunk.CHUNK_SIZE == 0)
            NeighborDirty(GetChunk(chunkX + 1, chunkY, chunkZ));
        if ((blockY + 1) % Chunk.CHUNK_SIZE == 0)
            NeighborDirty(GetChunk(chunkX, chunkY + 1, chunkZ));
        if ((blockZ + 1) % Chunk.CHUNK_SIZE == 0)
            NeighborDirty(GetChunk(chunkX, chunkY, chunkZ + 1));

    }
    public void SetBlock(float x, float y, float z, Block block)
    {
        SetBlock((int)x, (int)y, (int)z, block);
    }
    //SET BLOCK RELATED METHODS
    void NeighborDirty(Chunk chunk)
    {
        if (chunk != null)
            chunk.dirty = true;
    }
    byte Suspension(int x, int y, int z, byte id)
    {
        if (x == 0 || id == 35 || id == 97 || id == 98) return 0;

        Block blk = GetBlock(x, y + 1, z);
        if (blk != null)
            return blk.suspension;

        blk = GetBlock(x, y - 1, z);
        if (blk != null)
            return blk.suspension;
        byte[,] com = new byte[,] { { 1, 2, 3 }, { 0, 2, 3 }, { 0, 1, 3 }, { 0, 1, 2 } };
        Block[] blks = new Block[]
        {
            GetBlock(x, y, z + 1),
            GetBlock(x, y, z - 1),
            GetBlock(x - 1, y, z),
            GetBlock(x + 1, y, z)
        };
        for(int i = 0; i < 4; i++)
        {
            if (blks[i] == null) continue;
            if ((blks[com[i, 0]] == null || blks[i].suspension <= blks[com[i, 0]].suspension) && (blks[com[i, 1]] == null || blks[i].suspension <= blks[com[i, 1]].suspension) && (blks[com[i, 2]] == null || blks[i].suspension <= blks[com[i, 2]].suspension))
                return (byte)(blks[i].suspension + 1);
        }

        return 255;
    }

    //RETURNS THE BLOCK / CHUNK AT THE GIVEN LOCATION
    public Block GetBlock(int x, int y, int z)
    {
        int chunkX = x / Chunk.CHUNK_SIZE;
        int chunkY = y / Chunk.CHUNK_SIZE;
        int chunkZ = z / Chunk.CHUNK_SIZE;
        
        int blockX = x - (chunkX * Chunk.CHUNK_SIZE);
        int blockY = y - (chunkY * Chunk.CHUNK_SIZE);
        int blockZ = z - (chunkZ * Chunk.CHUNK_SIZE);

        Chunk chunk = GetChunk(chunkX, chunkY, chunkZ);

        if (chunk)
            return chunk.GetBlock(blockX, blockY, blockZ);

        return null;
    }
    public Chunk[,,] GetChunks()
    {
        return chunks;
    }
    public Chunk GetChunk(int x, int y, int z)
    {
        if (x < 0 || y < 0 || z < 0)
            return null;

        if (x >= chunks.GetLength(0) || y >= chunks.GetLength(1) || z >= chunks.GetLength(2))
            return null;

        return chunks[x, y, z];
    }
    public Chunk GetChunkAtBlock(int x, int y, int z)
    {
        if (x < 0 || y < 0 || z < 0)
            return null;

        int chunkX = x / Chunk.CHUNK_SIZE;
        int chunkY = y / Chunk.CHUNK_SIZE;
        int chunkZ = z / Chunk.CHUNK_SIZE;

        if (chunkX > chunks.GetLength(0) || chunkY > chunks.GetLength(1) || chunkZ > chunks.GetLength(2))
            return null;

        return chunks[chunkX, chunkY, chunkZ];
    }

    void OnCollisionStay(Collision collisionInfo)
    {
        if (rb == null || this == main) return; 
        if (collisionInfo.gameObject.layer == 8)
        {
            if(name.Contains("FallingPortion"))
                Destroy(rb);
            Chunk chunk = collisionInfo.gameObject.GetComponent<Chunk>();
            if (chunk != null && chunk.world != null)
            {
                V3I pos = new V3I(transform.position - chunk.world.transform.position);
                if (chunk.world.GetBlock(pos.x, pos.y, pos.z) != null) transform.position += new Vector3(0, 1, 0);
                else transform.position = new V3I(transform.position).ToVector3();
            }
            else transform.position = new V3I(transform.position).ToVector3();

            //StartCoroutine(convexOff(false));
        }
    }
    IEnumerator convexOff(bool state)
    {
        yield return new WaitForEndOfFrame();
        foreach (Chunk c in chunks)
            c.GetComponent<MeshCollider>().convex = state;
    }

    void FixedUpdate()
    {
        if (name.Contains("FallingPortion") && rb == null)
        {
            World_Load.WorldInsert(new V3I(transform.position - main.transform.position), size, this, main);
        }
        if (rb != null)
        {
            if (Input.GetKey(KeyCode.U))
                rb.useGravity = !rb.useGravity;
        }
    }

    //if true return Dmg if false return Data
    public static int GetDmgData(byte dmg, bool toggle)
    {
        if (toggle)
            return dmg / 10;
        else
            return dmg - ((dmg / 10) * 10);
    }
}
