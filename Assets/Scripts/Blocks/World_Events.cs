﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World_Events : MonoBehaviour {

    public static void Explosion(Vector3 pos, float radius, float power, float knockback)
    {
        //throw out an overlap sphere to capture all the chunks that need to be effected by the explosion
        Collider[] colliderHits = Physics.OverlapSphere(pos, radius, LayerMask.GetMask(new string[] { "WorldStatic", "WorldMesh", "Player" }), QueryTriggerInteraction.Ignore);
        List<World> worlds = new List<World>();

        foreach (Collider col in colliderHits)
        {
            //to the list all of the worlds that need to have blocks removed
            Chunk chunk = col.gameObject.GetComponent<Chunk>();
            if (chunk != null && !worlds.Contains(chunk.world)) worlds.Add(chunk.world);
        }
        
        //set all the blocks within the radius
        foreach(World world in worlds)
        {
            if (world == null) continue;
            V3I local = new V3I(world.transform.InverseTransformPoint(pos), Vector3.zero);

            for (int x = (int)-radius; x <= radius; x++)
                for (int y = (int)-radius; y <= radius; y++)
                    for (int z = (int)-radius; z <= radius; z++)
                        if (Distance(new Vector3(x, y, z), Vector3.zero) < Sqrt(radius))
                        {
                            Block block = world.GetBlock(local.x + x, local.y + y, local.z + z);
                            if (block == null) continue;
                            Block_Records.BlockRecord br = Block_Records.GetRecord(block.id);
                            if (br == null || br.explosiveDmg == 0) continue;
                            world.DamageBlock(local + new V3I(x, y, z), (byte)(br.explosiveDmg * (1 - (Distance(new Vector3(x, y, z), Vector3.zero) / Sqrt(radius))) * power));
                        }              
            
            world.RebuildWorld();
        }
    }

    static float Distance(Vector3 pos, Vector3 center)
    {
        return Sqrt(pos.x - center.x) + Sqrt(pos.y - center.y) + Sqrt(pos.z - center.z);
    }
    static float Sqrt(float f)
    {
        return f * f;
    }
}
