﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class World_Load : MonoBehaviour {
    public static World_Load self;
    public string map;
    public static Dictionary<int, GameObject> SubWorlds = new Dictionary<int, GameObject>();    
    static GameObject ChunkPrefab;
    static V3I LoadedSize;


    //ENTRY POINT
    void Start () {
        if(self == null) self = this;
        ChunkPrefab = Resources.Load("Prefabs/Main/Chunk") as GameObject;

        map = UI_Mapselect.map;

        if (map != null && map != "")
        {
            string path = Directory.GetCurrentDirectory() + @"\Unmanaged\Maps\" + map + ".bytes";
            Stream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None);
            
            World ws = AddWorld("MainWorld", self.gameObject);
            Chunk[,,] chunks = LoadFromFile(fs, ws);

            ws.WorldInit(chunks, LoadedSize);
            ws.RebuildWorld(true);
            ws.transform.localPosition = new Vector3(-(ws.size.x / 2), 0, -(ws.size.z / 2));
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            if(World.main.CCLCount() == 1) return;
            CCLComponent region = null;
            foreach (CCLComponent cclc in World.main.cclComponents)
            {
                if (cclc != null && cclc.parent.value == cclc.value && !cclc.isGrounded)
                {
                    region = cclc;
                    World.main.cclComponents.Remove(cclc);
                    break;
                }
            }
            if (region == null) return;
            World w = BreakAway(region.minBound, region.maxBound + new V3I(1, 1, 1), World.main, region);
            w.rb = w.gameObject.AddComponent<Rigidbody>();
            w.rb.mass = 1;
            w.rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ; 
        }
    }

    //TAKES A SECTION OF A WORLD AND INSERTS IT INTO A DIFFERENT WORLD
    public static void WorldInsert(V3I offset, V3I size, World origin, World target, bool CleanUp = true)
    {
        Debug.Log("inserting world at offset: " + offset.ToVector3() + " with size: " + size.ToVector3());
        Chunk[,,] Tchunks = target.GetChunks();
        Chunk[,,] Ochunks = origin.GetChunks();
        if (target.GetBlock(offset.x, offset.y, offset.z) != null) { offset.y++; Debug.Log("raised offset by 1 block"); }
        for (int z = 0; z < Mathf.Clamp(size.z, 0, Ochunks.GetLength(2) * Chunk.CHUNK_SIZE); z++)
        {
            for (int y = 0; y < Mathf.Clamp(size.y, 0, Ochunks.GetLength(1) * Chunk.CHUNK_SIZE); y++)
            {
                for (int x = 0; x < Mathf.Clamp(size.x, 0, Ochunks.GetLength(0) * Chunk.CHUNK_SIZE); x++)
                {
                    if (Ochunks[x / Chunk.CHUNK_SIZE, y / Chunk.CHUNK_SIZE, z / Chunk.CHUNK_SIZE] == null) continue;
                    Block b = Ochunks[x / Chunk.CHUNK_SIZE, y / Chunk.CHUNK_SIZE, z / Chunk.CHUNK_SIZE].GetBlock(ChunkCoordinate(x), ChunkCoordinate(y), ChunkCoordinate(z));
                    if (b == null) continue;
                    Tchunks[(x + offset.x) / Chunk.CHUNK_SIZE, (y + offset.y) / Chunk.CHUNK_SIZE, (z + offset.z) / Chunk.CHUNK_SIZE].SetBlock(ChunkCoordinate(x + offset.x), ChunkCoordinate(y + offset.y), ChunkCoordinate(z + offset.z), b);
                    Tchunks[(x + offset.x) / Chunk.CHUNK_SIZE, (y + offset.y) / Chunk.CHUNK_SIZE, (z + offset.z) / Chunk.CHUNK_SIZE].dirty = true;
                    if (b.model != null) b.model.transform.SetParent(Tchunks[(x + offset.x) / Chunk.CHUNK_SIZE, (y + offset.y) / Chunk.CHUNK_SIZE, (z + offset.z) / Chunk.CHUNK_SIZE].transform);
                }
            }
        }
        target.RebuildWorld(true);
        if (CleanUp) RemoveWorld(origin);
    }
    
    //TAKES A SECTION OF A WORLD AND CONVERTS BREAKS IT AWAY
    public static World BreakAway(V3I min, V3I max, World world, CCLComponent region)
    {
        //Create the Subworld
        GameObject sw = new GameObject("World", typeof(World));
        World ws = sw.GetComponent<World>();
        sw.transform.SetParent(self.transform);
        sw.transform.position = world.transform.position + min;

        //Name the world
        for (int i = 0; i <= SubWorlds.Count; i++)
        {
            if (SubWorlds.ContainsKey(i)) continue;
            else
            {
                SubWorlds.Add(i, sw);
                ws.Key = i;
                sw.name = SubWorlds.Count + "_FallingPortion";
                break;
            }
        }

        //Create the chunks
        Chunk[,,] chunks = new Chunk[(max.x - min.x) / Chunk.CHUNK_SIZE + 1, (max.y - min.y) / Chunk.CHUNK_SIZE + 1, (max.z - min.z) / Chunk.CHUNK_SIZE + 1];
        LoadedSize = world.size;

        //Populate the chunks with blocks
        for (int z = min.z; z < max.z; z++)
        {
            for (int y = min.y; y < max.y; y++)
            {
                for (int x = min.x; x < max.x; x++)
                {
                    Block block = world.GetBlock(x, y, z);
                    if (block == null || block.GetCCLComponent() == null || block.GetCCLComponent().value != region.value) continue;

                    Chunk chunk = chunks[(x - min.x) / Chunk.CHUNK_SIZE, (y - min.y) / Chunk.CHUNK_SIZE, (z - min.z) / Chunk.CHUNK_SIZE];
                    if (chunk == null) { chunk = CreateChunk(ChunkPosition(x - min.x), ChunkPosition(y - min.y), ChunkPosition(z - min.z)); chunk.dirty = true; chunk.transform.position += sw.transform.position; }
                    world.SetBlockFast(x, y, z, null);

                    //opperations on the new chunk
                    chunk.SetBlock(ChunkCoordinate(x - min.x), ChunkCoordinate(y - min.y), ChunkCoordinate(z - min.z), block);
                    chunk.GetComponent<MeshCollider>().convex = true;
                    chunk.transform.parent = sw.transform;
                    chunk.world = ws;
                    chunks[(x - min.x) / Chunk.CHUNK_SIZE, (y - min.y) / Chunk.CHUNK_SIZE, (z - min.z) / Chunk.CHUNK_SIZE] = chunk;

                    //block model stuff
                    if (block.model != null)
                    {
                        Vector3 pos = block.model.transform.position;
                        block.model.transform.SetParent(chunk.transform);
                        block.model.transform.position = pos;
                    }
                }
            }
        }
        ws.WorldInit(chunks, max - min);
        world.RebuildWorld();
        ws.RebuildWorld(true);
        return ws;
    }

    //ADD A SUB WORLD
    public static World AddWorld(string name, GameObject parent)
    {
        GameObject sw = new GameObject("_" + name, typeof(World)); 
        for (int i = 0; i <= SubWorlds.Count; i++)
        {
            if (SubWorlds.ContainsKey(i)) continue;
            else
            {
                if (name == null) name = "World";
                sw.name = i + "_" + name;
                sw.GetComponent<World>().Key = i;
                SubWorlds.Add(i, sw);
                break;
            }
        }
        sw.transform.SetParent(parent.transform);
        return sw.GetComponent<World>();
    }
    //CREATE A HULL FOR THE SHIP
    public static GameObject AddHull(V3I size)
    {
        GameObject g = new GameObject("ConvexHull", typeof(Rigidbody));
        for (int x = 0; x <= size.x / Chunk.CHUNK_SIZE; x++)
            for (int y = 0; y <= size.y / Chunk.CHUNK_SIZE; y++)
                for (int z = 0; z <= size.z / Chunk.CHUNK_SIZE; z++)
                {
                    GameObject h = new GameObject("Hull_" + x + "-" + y + "-" + z, typeof(MeshCollider));
                    h.transform.SetParent(g.transform);

                }
        return g;
    }
    //DESTROYS A GIVEN WORLD
    public static void RemoveWorld(World world)
    {
        SubWorlds.Remove(world.Key);
        Destroy(world.gameObject);
    }
    //CREATE A BASIC CHUNK
    public static Chunk CreateChunk(int x, int y, int z, World world = null, GameObject HullParent = null)
    {
        GameObject newChunkObject = Instantiate(ChunkPrefab, new Vector3(x, y, z), Quaternion.Euler(Vector3.zero)) as GameObject;
        Chunk newChunk = newChunkObject.GetComponent<Chunk>();
        GameObject hull = null;

        if(HullParent != null) {
            hull = new GameObject("Hull_" + x / 16 + "-" + y / 16 + "-" + z / 16, typeof(MeshCollider));
            hull.GetComponent<MeshCollider>().convex = true;
            hull.transform.position = new Vector3(x, y, z);
            hull.transform.SetParent(HullParent.transform);
            hull.layer = 10;
            newChunkObject.layer = 9;
        }
        

        newChunk.InitChunk(x, y, z, hull);
        if(world != null)
        {
            newChunk.transform.SetParent(world.transform);
            newChunk.world = world;
        }
        return newChunk;
    }
    public static byte[] ReadAllBytes(BinaryReader reader)
    {
        const int bufferSize = 4096;
        using (var ms = new MemoryStream())
        {
            byte[] buffer = new byte[bufferSize];
            int count;
            while ((count = reader.Read(buffer, 0, buffer.Length)) != 0)
                ms.Write(buffer, 0, count);
            return ms.ToArray();
        }

    }
    //LOAD A SET OF CHUNKS FROM A FILE
    public static Chunk[,,] LoadFromFile(Stream stream, World world)
    {
  

        using (BinaryReader br = new BinaryReader(stream))
        { 

            string name = br.ReadString();
            float killPlane = br.ReadSingle();
            float waterPlane = br.ReadSingle();
            short length = br.ReadInt16();
            short height = br.ReadInt16();
            short width = br.ReadInt16();

            LoadedSize = new V3I(width, height, length);
            int dataSize = length * width * height * 4;

            byte[] data = br.ReadBytes(dataSize);
            Chunk[,,] chunks = new Chunk[width / Chunk.CHUNK_SIZE + 1, height / Chunk.CHUNK_SIZE + 1, length / Chunk.CHUNK_SIZE + 1];
            for (int x = 0; x <= width / Chunk.CHUNK_SIZE; x++)
                for (int y = 0; y <= height / Chunk.CHUNK_SIZE; y++)
                    for (int z = 0; z <= length / Chunk.CHUNK_SIZE; z++)
                        chunks[x, y, z] = CreateChunk(x * Chunk.CHUNK_SIZE, y * Chunk.CHUNK_SIZE, z * Chunk.CHUNK_SIZE, world);

            for (int z = 0; z < length; z++)
            {
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        int idx = 4 *( x + (y * width) + (z * height * width));
                        if (data[idx] == 0)
                            continue;


                        Block b = new Block(data[idx + 0], data[idx + 1], data[idx + 2], data[idx + 3]);
                        chunks[x / Chunk.CHUNK_SIZE, y / Chunk.CHUNK_SIZE, z / Chunk.CHUNK_SIZE].SetBlock(ChunkCoordinate(x), ChunkCoordinate(y), ChunkCoordinate(z), b);

                    }
                }
            }

            return chunks;
        }        
    }

    //MATH STUFF FOR CHUNKS
    public static int ChunkCoordinate(int i)
    {
        return i - ((i / Chunk.CHUNK_SIZE) * Chunk.CHUNK_SIZE);
    }
    public static int ChunkPosition(int i)
    {
        return (i / Chunk.CHUNK_SIZE) * Chunk.CHUNK_SIZE;
    }
}
