﻿using UnityEngine;
using System;

public class CCLComponent : IComparable<CCLComponent>
{
    public CCLComponent parent;
    public int value;
    public V3I minBound;
    public V3I maxBound;
    public bool isGrounded;
    public int blockCount;

    public override string ToString()
    {
        return string.Format("(CCL Value:{0}, Grounded: {1}), size: {2}, grounded: {3}, count: {4}", parent.value, isGrounded, (maxBound - minBound) + new V3I(1, 1, 1), isGrounded, blockCount);
    }
    public CCLComponent(int val)
    {
        parent = this;
        value = val;

        minBound = new V3I(1000, 1000, 1000);
        maxBound = new V3I(-1000,-1000, -1000);
    }

    public void ResetBounds()
    {
        minBound = new V3I(1000, 1000, 1000);
        maxBound = new V3I(-1000, -1000, -1000);
    }

    public CCLComponent GetMasterComponent()
    {
        if (parent != this)
            parent = parent.GetMasterComponent();

        return parent;
    }

    public void Union(CCLComponent other)
    {
        CCLComponent parentA = GetMasterComponent();
        CCLComponent parentB = other.GetMasterComponent();

        if (parentA == parentB)
            return;

        parentA.parent = parentB;

        parentB.ExpandBounds(parentA.minBound, parentA.maxBound);

        if (isGrounded)
            parentB.isGrounded = true;

        parentB.blockCount += parentA.blockCount;
    }

    public bool IsTopLevel()
    {
        return parent == this;
    }

    public void ExpandBounds(int x, int y, int z)
    {
        minBound.x = Mathf.Min(minBound.x, x);
        minBound.y = Mathf.Min(minBound.y, y);
        minBound.z = Mathf.Min(minBound.z, z);

        maxBound.x = Mathf.Max(maxBound.x, x);
        maxBound.y = Mathf.Max(maxBound.y, y);
        maxBound.z = Mathf.Max(maxBound.z, z);
    }

    public void ExpandBounds(V3I a, V3I b)
    {
        minBound.x = Mathf.Min(minBound.x, a.x);
        minBound.y = Mathf.Min(minBound.y, a.y);
        minBound.z = Mathf.Min(minBound.z, a.z);

        maxBound.x = Mathf.Max(maxBound.x, b.x);
        maxBound.y = Mathf.Max(maxBound.y, b.y);
        maxBound.z = Mathf.Max(maxBound.z, b.z);
    }

    public int CompareTo(CCLComponent other)
    {
        return value.CompareTo(other.value);
    }
}