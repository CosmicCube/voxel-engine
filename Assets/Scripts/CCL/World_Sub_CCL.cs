﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

partial class World
{
    public List<CCLComponent> cclComponents;
    private int cclNextLabel;

    public void CCLInit()
    {
        cclNextLabel = 0;
        cclComponents = new List<CCLComponent>();
    }

    public void CCLInitialScan()
    {
        for (int x = 0; x <= size.x; x++)
        {
            for (int y = 0; y <= size.y; y++)
            {
                for (int z = 0; z <= size.z; z++)
                {
                    Block xyzBlock = GetBlock(x, y, z);
                    
                    if (xyzBlock == null)
                        continue;

                    List<CCLComponent> neighbours = CCLGetPreviousNeighboursUnique(x, y, z);
                    
                    if (neighbours.Count > 0)
                    {
                        neighbours.Sort();
                        
                        CCLComponent lowestComponent = neighbours.First();
                        
                        xyzBlock.cclComponent = lowestComponent;
                        xyzBlock.cclComponent.ExpandBounds(x, y, z);

                        if (CCLIsGround(x, y, z))
                            xyzBlock.cclComponent.isGrounded = true;

                        xyzBlock.cclComponent.blockCount += 1;

                        for (int i = 1; i < neighbours.Count; i++)
                            neighbours[i].Union(lowestComponent);
                    }
                    else
                    {
                        xyzBlock.cclComponent = CCLCreateNewComponent();
                        xyzBlock.cclComponent.ExpandBounds(x, y, z);
                        xyzBlock.cclComponent.blockCount += 1;

                        if (CCLIsGround(x, y, z))
                            xyzBlock.cclComponent.isGrounded = true;
                    }
                }
            }
        }

        CCLCleanList();
    }

    public void CCLCleanList()
    {
        for (int i = 0; i < cclComponents.Count; i++)
            if (!cclComponents[i].IsTopLevel())
                cclComponents.RemoveAt(i--);
    }

    public int CCLCount()
    {
        return cclComponents.Count;
    }

    public List<CCLComponent> CCLGetPreviousNeighboursUnique(int x, int y, int z)
    {
        List<CCLComponent> neighbours = new List<CCLComponent>(3);

        CCLAddCCLFromBlockUnique(neighbours, GetBlock(x - 1, y, z));
        CCLAddCCLFromBlockUnique(neighbours, GetBlock(x, y - 1, z));
        CCLAddCCLFromBlockUnique(neighbours, GetBlock(x, y, z - 1));

        return neighbours;
    }

    public void CCLAddCCLFromBlockUnique(List<CCLComponent> list, Block block)
    {
        if (block != null)
            if (!list.Contains(block.GetCCLComponent()))
                list.Add(block.GetCCLComponent());
    }

    public void CCLAddCCLFromBlock(List<CCLComponent> list, Block block)
    {
        if (block != null)
            list.Add(block.GetCCLComponent());
    }

    public void CCLAddBlock(int x, int y, int z, Block xyzBlock)
    {
        List<CCLComponent> neighbours = CCLGetSideNeighbours(x, y, z);

        if (neighbours.Count > 1)
        {
            neighbours.Sort();

            CCLComponent lowestComponent = neighbours.First();

            xyzBlock.cclComponent = lowestComponent;
            xyzBlock.cclComponent.ExpandBounds(x, y, z);
            xyzBlock.cclComponent.blockCount += 1;

            if (CCLIsGround(x, y, z))
                xyzBlock.cclComponent.isGrounded = true;

            for (int i = 1; i < neighbours.Count; i++)
                neighbours[i].Union(lowestComponent);
        }
        else
        {
            if(neighbours.Count == 0) return; 
            CCLComponent lowestComponent = neighbours.First();
            xyzBlock.cclComponent = lowestComponent;
            xyzBlock.cclComponent.ExpandBounds(x, y, z);
            if (CCLIsGround(x, y, z))
                xyzBlock.cclComponent.isGrounded = true;
            xyzBlock.cclComponent.blockCount += 1;
        }

        CCLCleanList();
    }

    public bool CCLIsGround(int x, int y, int z)
    {
        return y == 0;
    }

    public void CCLRemoveBlock(int x, int y, int z, CCLComponent cclc)
    {
        cclc.blockCount -= 1;
        List<CCLComponent> neighbours = CCLGetAllNeighbours(x, y, z);
        
        if (neighbours.Count >= 19)
            return;

        if (!CCLCanDisconnect(x, y, z))
            return;

        CCLRescanComponent(cclc.GetMasterComponent());

        cclComponents.Remove(cclc);
        CCLCleanList();

        CCLClearUngrounded();
    }

    public void CCLClearUngrounded()
    {
        for (int i = 0; i < cclComponents.Count; i++)
            if (!cclComponents[i].GetMasterComponent().isGrounded)
                CCLClearComponent(cclComponents[i].GetMasterComponent());

        RebuildWorld();
    }

    public void CCLClearComponent(CCLComponent cclc)
    {
        for (int x = cclc.minBound.x; x <= cclc.maxBound.x; x++)
        {
            for (int y = cclc.minBound.y; y <= cclc.maxBound.y; y++)
            {
                for (int z = cclc.minBound.z; z <= cclc.maxBound.z; z++)
                {
                    Block xyzBlock = GetBlock(x, y, z);

                    if (xyzBlock == null || xyzBlock.cclComponent.GetMasterComponent() != cclc)
                        continue;

                    if (xyzBlock.model != null)
                        Destroy(xyzBlock.model);

                    SetBlockFast(x, y, z, null);
                }
            }
        }
    }

    public bool CCLCanDisconnect(int x, int y, int z)
    {
        int[,,] tmpLabelArray = new int[3, 3, 3];
        List<CCLComponent> tmpComponents = new List<CCLComponent>();
        int label = 0;

        for (int xx = x - 1; xx <= x + 1; xx++)
        {
            for (int yy = y - 1; yy <= y + 1; yy++)
            {
                for (int zz = z - 1; zz <= z + 1; zz++)
                {
                    if (GetBlock(xx, yy, zz) == null)
                        continue;

                    List<int> neighbours = new List<int>(3);

                    if (xx != x - 1 && GetBlock(xx - 1, yy, zz) != null)
                        neighbours.Add(tmpLabelArray[(xx - (x - 1)) - 1, yy - (y - 1), zz - (z - 1)]);

                    if (yy != y - 1 && GetBlock(xx, yy - 1, zz) != null)
                        neighbours.Add(tmpLabelArray[xx - (x - 1), (yy - (y - 1)) - 1, zz - (z - 1)]);

                    if (zz != z - 1 && GetBlock(xx, yy, zz - 1) != null)
                        neighbours.Add(tmpLabelArray[xx - (x - 1), yy - (y - 1), (zz - (z - 1)) - 1]);

                    if (neighbours.Count > 0)
                    {
                        neighbours.Sort();
                        tmpLabelArray[xx - (x - 1), yy - (y - 1), zz - (z - 1)] = neighbours.First();

                        for (int i = 1; i < neighbours.Count; i++)
                            tmpComponents[neighbours[i]].Union(tmpComponents[tmpLabelArray[xx - (x - 1), yy - (y - 1), zz - (z - 1)]]);
                    }
                    else
                    {
                        tmpComponents.Add(new CCLComponent(label));
                        tmpLabelArray[xx - (x - 1), yy - (y - 1), zz - (z- 1)] = label++;
                    }
                }
            }
        }

        for (int i = 0; i < tmpComponents.Count; i++)
            if (!tmpComponents[i].IsTopLevel())
                tmpComponents.RemoveAt(i--);
        
        // Debug.Log(tmpComponents.Count);
        //Debug.Log(tmpComponents.Count);

        if (tmpComponents.Count > 1)
            return true;
        
     
        return false;
    }

    public void CCLFilterFromList(List<CCLComponent> list, CCLComponent value)
    {
        for (int i = 0; i < list.Count; i++)
            if (list[i].GetMasterComponent() == value)
                list.RemoveAt(i--);
    }

    public void CCLRescanComponent(CCLComponent cclc)
    {
        for (int x = cclc.minBound.x; x <= cclc.maxBound.x; x++)
        {
            for (int y = cclc.minBound.y; y <= cclc.maxBound.y; y++)
            {
                for (int z = cclc.minBound.z; z <= cclc.maxBound.z; z++)
                {
                    Block xyzBlock = GetBlock(x, y, z);

                    if (xyzBlock == null || xyzBlock.GetCCLComponent() != cclc)
                        continue;

                    List<CCLComponent> neighbours = CCLGetPreviousNeighboursUnique(x, y, z);
                    CCLFilterFromList(neighbours, cclc);

                    if (neighbours.Count > 0)
                    {
                        
                        neighbours.Sort();

                        CCLComponent lowestComponent = neighbours.First();

                        xyzBlock.cclComponent = lowestComponent;
                        xyzBlock.cclComponent.ExpandBounds(x, y, z);
                        xyzBlock.cclComponent.blockCount += 1;


                        if (CCLIsGround(x, y, z))
                            xyzBlock.cclComponent.isGrounded = true;

                        for (int i = 1; i < neighbours.Count; i++)
                            neighbours[i].Union(lowestComponent);
                    }
                    else
                    {
                        xyzBlock.cclComponent = CCLCreateNewComponent();
                        xyzBlock.cclComponent.ExpandBounds(x, y, z);
                        xyzBlock.cclComponent.blockCount += 1;

                        if (CCLIsGround(x, y, z))
                            xyzBlock.cclComponent.isGrounded = true;
                    }
                    
                }
            }
        }
    }

    public List<CCLComponent> CCLGetSideNeighbours(int x, int y, int z)
    {
        List<CCLComponent> neighbours = new List<CCLComponent>();

        CCLAddCCLFromBlockUnique(neighbours, GetBlock(x - 1, y, z));
        CCLAddCCLFromBlockUnique(neighbours, GetBlock(x, y, z - 1));
        CCLAddCCLFromBlockUnique(neighbours, GetBlock(x, y - 1, z));
        CCLAddCCLFromBlockUnique(neighbours, GetBlock(x + 1, y, z));
        CCLAddCCLFromBlockUnique(neighbours, GetBlock(x, y, z + 1));
        CCLAddCCLFromBlockUnique(neighbours, GetBlock(x, y + 1, z));

        return neighbours;
    }

    public List<CCLComponent> CCLGetAllNeighbours(int x, int y, int z)
    {
        List<CCLComponent> neighbours = new List<CCLComponent>();

        CCLAddCCLFromBlock(neighbours, GetBlock(x - 1, y - 1, z - 1));
        CCLAddCCLFromBlock(neighbours, GetBlock(x - 1, y - 1, z));
        CCLAddCCLFromBlock(neighbours, GetBlock(x - 1, y - 1, z + 1));
        CCLAddCCLFromBlock(neighbours, GetBlock(x - 1, y, z - 1));
        CCLAddCCLFromBlock(neighbours, GetBlock(x - 1, y, z));
        CCLAddCCLFromBlock(neighbours, GetBlock(x - 1, y, z + 1));
        CCLAddCCLFromBlock(neighbours, GetBlock(x - 1, y + 1, z - 1));
        CCLAddCCLFromBlock(neighbours, GetBlock(x - 1, y + 1, z));
        CCLAddCCLFromBlock(neighbours, GetBlock(x - 1, y + 1, z + 1));
        CCLAddCCLFromBlock(neighbours, GetBlock(x, y - 1, z - 1));
        CCLAddCCLFromBlock(neighbours, GetBlock(x, y - 1, z));
        CCLAddCCLFromBlock(neighbours, GetBlock(x, y - 1, z + 1));
        CCLAddCCLFromBlock(neighbours, GetBlock(x, y, z - 1));
        CCLAddCCLFromBlock(neighbours, GetBlock(x, y, z + 1));
        CCLAddCCLFromBlock(neighbours, GetBlock(x, y + 1, z - 1));
        CCLAddCCLFromBlock(neighbours, GetBlock(x, y + 1, z));
        CCLAddCCLFromBlock(neighbours, GetBlock(x, y + 1, z + 1));
        CCLAddCCLFromBlock(neighbours, GetBlock(x + 1, y - 1, z - 1));
        CCLAddCCLFromBlock(neighbours, GetBlock(x + 1, y - 1, z));
        CCLAddCCLFromBlock(neighbours, GetBlock(x + 1, y - 1, z + 1));
        CCLAddCCLFromBlock(neighbours, GetBlock(x + 1, y, z - 1));
        CCLAddCCLFromBlock(neighbours, GetBlock(x + 1, y, z));
        CCLAddCCLFromBlock(neighbours, GetBlock(x + 1, y, z + 1));
        CCLAddCCLFromBlock(neighbours, GetBlock(x + 1, y + 1, z - 1));
        CCLAddCCLFromBlock(neighbours, GetBlock(x + 1, y + 1, z));
        CCLAddCCLFromBlock(neighbours, GetBlock(x + 1, y + 1, z + 1));

        return neighbours;
    }




    public CCLComponent CCLCreateNewComponent()
    {
        CCLComponent newComponent = new CCLComponent(cclNextLabel);
        cclComponents.Add(newComponent);
        cclNextLabel += 1;

        return newComponent;
    }
}