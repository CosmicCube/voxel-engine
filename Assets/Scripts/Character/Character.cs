﻿using UnityEngine;

public class Character : MonoBehaviour
{
    public static Color[] cols = new Color[] { Color.red, Color.green, Color.blue, Color.black, Color.gray, Color.yellow, Color.magenta, new Color(1, 1, 1, 1), new Color(1, 0.8f, 1, 1), new Color(0.8f, 1, 1, 1) };

    public Vector3 location = Vector3.zero;
    public Vector3 rotation = Vector3.zero;

    public Vector3 velocity = Vector3.zero;
    public Vector3 swing = Vector3.zero;


    public int id;

    public string Name;
    public int health;
    public int weapon;

    void Start()
    {
        if (id < cols.Length)
        {
            Material mat = new Material(transform.GetChild(0).gameObject.GetComponent<Renderer>().material);
            mat.SetColor("_Color", cols[id]);
            transform.GetChild(0).gameObject.GetComponent<Renderer>().material = mat;
        }
    }

    void FixedUpdate()
    {
        //Character c = GetComponent<Character>();
        if(transform.position != location)
            transform.position += velocity / 60;
        if (transform.eulerAngles != rotation)
            transform.eulerAngles += swing / 60;
    }
}