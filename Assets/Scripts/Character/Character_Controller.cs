﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Character_Controller : MonoBehaviour
{
    public static Character_Controller instance;

    private GameObject child;
    private Rigidbody rb;
    MappedInput mi;
    public float jumpHeight = 1.2f;
    public float Speed = 5;
    const float maxVchng = 17.5f; //max accaleration
    static int grounded = 0;

    public Transform weaponSlot;

    public static bool roofAbove;
    public static int dmgBonus = 1;
    public static bool crouching = false;

    private Weapon currentWeapon;
    private int activeWeaponIndex;
    private List<WeaponData> weapons;
    private float lastTimeShot;

    public Camera playerCamera;

    public Vector3 Applied;
    public Vector3 targetVelocity = Vector3.zero;

    void Start()
    {
        instance = this;
        weapons = new List<WeaponData>(); 
        weapons.Add(Resources.Load<WeaponData>("WeaponData/Sniper Rifle"));
        weapons.Add(Resources.Load<WeaponData>("WeaponData/Shotgun"));
        weapons.Add(Resources.Load<WeaponData>("WeaponData/Gravity Gloves"));
        currentWeapon = Instantiate(weapons[activeWeaponIndex].prefab, weaponSlot, false);
        mi = InputManager.GetFrameInput();
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        child = transform.GetChild(0).gameObject;
    }
    int n = 0;
    void FixedUpdate()
    {
        if(Network_Manager.status == NetStatus.InGame && n >= 60)
        {
            Network_Manager.Instance.SendLocation(transform.position, transform.eulerAngles);
            n = 0;
        }
        n++;
    }


    void Update()
    {
        if (!State_Manager.settings)
        {
            if (mi.Crouch)
                Speed = Crouch(0.35f, 0.6f, 0.5f, 3f, true);
            else if (grounded > 0 && !roofAbove)
                Speed = Crouch(1.4f, 1.2f, 1.5f, 7f, false);
            else if(!crouching)
                Speed = 5f;

            if (crouching)
            {
                roofAbove = false;
                RaycastHit hit;
                if (Physics.Raycast(transform.position, Vector3.up * 2, out hit, 1.9f, LayerMask.GetMask(new string[] { "WorldCollidable" })))
                    roofAbove = true;
            }
            

            if (Input.GetKeyDown(KeyCode.M))
            {
                Instantiate(Resources.Load("Prefabs/Weapons/Test Player Object"), new Vector3(10, 35, 10), Quaternion.identity);
            }



            Character_Controller.instance.Walk((int) mi.MovX, (int) mi.MovY);

           // if (mi.Shoot)
            {
                //if (Time.time - lastTimeShot > weapons[activeWeaponIndex].fireRate)
                {
                    currentWeapon.GetComponent<Weapon>().Fire(mi.Shoot, weapons[activeWeaponIndex], playerCamera.transform.rotation, weaponSlot, playerCamera.transform.forward);
                    lastTimeShot = Time.time;
                }
            }

            if (mi.ToggleWeapon)
            {
                if (currentWeapon != null && currentWeapon.gameObject != null)
                Destroy(currentWeapon.gameObject);

                activeWeaponIndex += 1;
                activeWeaponIndex %= weapons.Count;

                currentWeapon = Instantiate(weapons[activeWeaponIndex].prefab, weaponSlot, false);
            }

            if (mi.Jump && grounded > 0)
                rb.velocity = new Vector3(rb.velocity.x, Mathf.Sqrt(2.2f * jumpHeight * 9), rb.velocity.z);
            //walk input

            //rotate character around Y axis
            float rotY = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * (float)Json_Settings.current.Sensitivity * 5;
            transform.localEulerAngles = new Vector3(0, rotY, 0);
            //recenter cursor
            UnityEngine.Cursor.lockState = CursorLockMode.Locked;
            UnityEngine.Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Character_Controller.instance.Walk(0, 0);
        }

        if(rb.velocity.y != 0)
            grounded = 0;
        else
            grounded = 1;
    }

    public void Walk(int dx, int dy)
    {
        //friction
        if (grounded > 0 && Applied != Vector3.zero) { Applied *= 0.85f; }
        else if(Applied != Vector3.zero) { Applied *= 0.995f; }
        if(Mathf.Abs(Applied.x) < 0.01f || Mathf.Abs(Applied.z) < 0.01f) { Applied = Vector3.zero; }

        // Calculate how fast we should be moving
        // Vector3 targetVelocity = transform.TransformDirection(new Vector3(moveAD * Speed, 0, moveSW * Speed));
        Vector3 targetVelocity = transform.TransformDirection(new Vector3(dx * Speed, 0, dy * Speed));
        targetVelocity += Applied;
        if (targetVelocity == Vector3.zero && grounded > 1) rb.velocity = Vector3.zero; 

            // Apply a force that attempts to reach our target velocity
        Vector3 velocityChange = (targetVelocity - rb.velocity);
        velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVchng, maxVchng);
        velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVchng, maxVchng);
        velocityChange.y = 0;
        rb.AddForce(velocityChange, ForceMode.Acceleration);
    }
    public float Crouch(float h, float c, float posY, float spd, bool state)
    {
        GetComponent<CapsuleCollider>().height = h;
        GetComponent<CapsuleCollider>().center = new Vector3(0, c, 0);
        child.transform.localPosition = new Vector3(child.transform.localPosition.x, posY, child.transform.localPosition.z);
        crouching = state;
        return spd;
    }

    void OnCollisionStay(Collision collisionInfo)
    {
        if (grounded == 0)
        {
            foreach (ContactPoint contact in collisionInfo.contacts)
            {
                if (contact.normal.y > 0.99f)
                {
                    grounded = 2;
                    return;
                }
                else if (contact.normal.y > 0.1f)
                    grounded = 1;
            }
        }
    }
}
 
