﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character_Gen : MonoBehaviour {
    public static Character_Gen self;
    public GameObject prefab;
    Dictionary<int, GameObject> chrs = new Dictionary<int, GameObject>();
    
    void Start()
    {
        self = this;
    }
    public void playerMove(int id, Vector3 loc, Vector3 rot)
    {
        if (chrs.ContainsKey(id))
        {
            Character chr = chrs[id].GetComponent<Character>();
            Transform t = chrs[id].transform;

            t.position = chr.location;
            t.eulerAngles = chr.rotation;

            chr.velocity = loc - t.position;
            chr.swing = rot - t.eulerAngles;
            
            chr.location = loc;
            chr.rotation = rot;
        }
        else playerInit(id, "unknown");
    }
    public void playerDestroy(int id)
    {
        if (chrs.ContainsKey(id))
        {
            Destroy(chrs[id]);
            chrs.Remove(id);
        }
    }


    public void playerInit(int id, string name)
    {
        GameObject g = Instantiate(prefab, Vector3.zero, Quaternion.identity);
        Character chr = g.GetComponent<Character>();
        chr.id = id;
        chr.Name = name;
        g.transform.SetParent(transform);
        if (chrs.ContainsKey(id))
        {
            Destroy(chrs[id]);
            chrs.Remove(id);
        }
        chrs.Add(id, g);
    }
}
