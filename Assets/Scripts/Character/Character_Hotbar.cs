﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Character_Hotbar : MonoBehaviour {
    public static Character_Hotbar self;
    float scroll;
    public static int HBpos;
    private List<GameObject> Children = new List<GameObject>();

    void Awake() { self = this; }
    void Start () {
        HBpos = 0;
        for (int i = 0; i < 8; i++) Children.Add(transform.GetChild(i).gameObject);
    }

	void Update () {
        scroll = Input.GetAxis("Mouse ScrollWheel");
        scroll = -scroll * 10;
        if (scroll != 0)
        {
            Children[HBpos].GetComponent<RectTransform>().transform.localScale = new Vector3(1, 1, 1);
            Children[HBpos].GetComponent<RectTransform>().transform.localPosition = new Vector3(Children[HBpos].GetComponent<RectTransform>().transform.localPosition.x, 0, 0);
            HBpos += (int) scroll;
            if (HBpos > 7) HBpos -= 8;
            if (HBpos < 0) HBpos += 8;
            Children[HBpos].GetComponent<RectTransform>().transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);
            Children[HBpos].GetComponent<RectTransform>().transform.localPosition = new Vector3(Children[HBpos].GetComponent<RectTransform>().transform.localPosition.x, 5, 0);
        }
    }
}
