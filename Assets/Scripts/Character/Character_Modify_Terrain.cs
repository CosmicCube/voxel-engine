﻿using UnityEngine;
using System.Collections.Generic;
using System;
//STORAGE OF SOME DATA
internal static class Character_States
{
    internal static int OLactive = 0;

    internal static byte DOP = 0;
    internal static byte BuildTime = 0;

    internal static byte DestroyCooldown = 0;

    internal static byte Placement;
    internal static byte lastPlaced = 0;

    internal static bool freezeMovement = false;
    
    internal static bool placed = false;
    internal static bool beveled = false;
}
public class Character_Modify_Terrain : MonoBehaviour
{
    public static Character_Modify_Terrain self;
    public static int[,] CHB;

    static GameObject ray;
    static GameObject parent;
    static RaycastHit hit;
    static V3I point = new V3I(0, 0, 0);
    static V3I normal = new V3I(0, 0, 0);
    public static World world = World.main;

    //INIALIZE SCRIPT
    void Start()
    {
        if (self == null) self = this;
        ray = transform.GetChild(0).gameObject;
        parent = transform.parent.gameObject;
        CHB = new[,] { { Block_Records.ID_FORCES, 0, 0 }, { Block_Records.ID_CRATES, 0, 0 }, { Block_Records.ID_CONSTUCTS, 0, 0 }, { Block_Records.ID_SANDBAGS, 0, 0 },
            { Block_Records.ID_BOUNCER, 0, 0 }, { Block_Records.ID_FLOATERS, 0, 0 }, { Block_Records.ID_BOMB, 0, 0 }, { Block_Records.ID_BUILDER, 0, 0 } };
    }

    //UPDATE LOOP FOR CAMERA MOTION
    float rotX = 0;
    void Update()
    {
        if (!State_Manager.settings)
        {
            rotX += -Input.GetAxis("Mouse Y") * (float)Json_Settings.current.Sensitivity * 5;
            rotX = Mathf.Clamp(rotX, -90, 90);
            transform.localEulerAngles = new Vector3(rotX, 0, 0);
        }
        else
        {
            Character_States.OLactive = 0;
            if(Menu_Manager.Buildoutline != null) Menu_Manager.Buildoutline.SetActive(false);
        }

        
    }

    void ProcessInput()
    {
        if (Input.GetMouseButtonUp(1) || Input.GetKeyUp(KeyCode.V))
        {
            Character_States.BuildTime = 0;
            if (Character_Placement.Buildtime != null)
                Character_Placement.Buildtime.SetActive(false);
            Character_States.placed = false;
            Character_States.beveled = false;
        }
    }

    //THE MAIN UPDATE LOOP
    void FixedUpdate()
    {
        if ((Character_States.OLactive > 0 || Input.GetMouseButton(0) || Input.GetMouseButton(1) || Input.GetMouseButton(2) || Input.GetKey(KeyCode.V)) && !State_Manager.settings)
        {
            //locate block player is looking at
            point = new V3I(0, -1, 0);
            if (rayCast(transform.position, transform.forward, 5))
                AdjustWorld();
            else if (rayCast(ray.transform.position, parent.transform.forward * -1, 3))
                AdjustWorld();
            
            //Set references
            Character_States.Placement = (byte)CHB[Character_Hotbar.HBpos, 0];
            if (Character_States.OLactive > 0 && world != null)
            {
                if (Menu_Manager.Buildoutline == null) Menu_Manager.UpdateDisplay();
                Menu_Manager.Buildoutline.SetActive(true);
                Menu_Manager.Buildoutline.transform.position = world.transform.TransformPoint(point.ToVector3());
                Menu_Manager.Buildoutline.transform.rotation = world.transform.rotation;
            }

            //act on the current input state
            if (Input.GetMouseButton(0) && Character_States.DestroyCooldown == 0)
                //World_Events.Explosion(world.transform.TransformPoint(point.ToVector3()), 2.5f, 2.25f, 1);
                damageBlock(point + normal);
            else if (Input.GetMouseButton(1) && Character_Placement.CanBuild && !Character_States.placed)
                placeBlock(point);
            else if (Input.GetMouseButton(2))
                interactBlock(point + normal); //debugBlock(point + normal);
            else if (Input.GetKey(KeyCode.V) && !Character_States.beveled)
                bevelBlock(point + normal, hit.point - world.transform.position);            
        }

        if (Character_States.DestroyCooldown > 0)
            Character_States.DestroyCooldown--;

        //If mouse press draw block bounds for short time
        if (!State_Manager.settings && (Input.GetMouseButton(1) || Input.GetAxis("Mouse ScrollWheel") != 0))
        {
            if(Input.GetAxis("Mouse ScrollWheel") != 0)
                Character_States.BuildTime = 0;
            if (Character_States.OLactive > 0)
                Character_States.OLactive = 359;
            else
                Character_States.OLactive = 360;
        }


        //play a short delay after a block is placed
        if (Character_States.DOP > 0)
        {
            Character_Placement.Buildtime.SetActive(true);
            float percent = Character_States.DOP * (1 / (float)Block_Records.GetCost(Character_States.lastPlaced).placedDelay);
            Character_Placement.Buildtime.transform.localPosition = new Vector3(0, 0, 0);
            Character_Placement.Buildtime.transform.localScale = new Vector3((0.99f * percent), (0.99f * percent), (0.99f * percent));
            if (Character_States.DOP == 1)
                Character_Placement.Buildtime.SetActive(false);
            Character_States.DOP--;
        }
    }
    
    //DAMAGE BLOCK
    void damageBlock(V3I l)
    {
        if (world == null) return;
        Block block = world.GetBlock(l.x, l.y, l.z);
        if (block == null) return;
        Block_Records.BlockRecord br = Block_Records.GetRecord(block.id);
        if(br == null) return;
        int dmg = (br.toolDmg * Character_Controller.dmgBonus);
        if ((dmg * 10 + block.dmg) >= 250)
        {
            if (Network_Manager.status == NetStatus.InGame)
                Network_Manager.Instance.SetBlock(l.x, l.y, l.z, new Block(0));
            else {
                world.SetBlock(l.x, l.y, l.z, null);
                world.RebuildWorld();
            }
        }
        else
        {
            block.dmg = (byte)(dmg * 10 + block.dmg);
            world.SetBlockFast(l.x, l.y, l.z, block);
            world.RebuildWorld();

            if (block.model != null)
            {
                if (block.model.GetComponent<Device_Interactable>() != null) block.model.GetComponent<Device_Interactable>().DisplayHealth();
                else block.model.AddComponent<Device_Interactable>().DisplayHealth();
            }
        }
        Character_States.DestroyCooldown = 10;
    }
    //PLACE BLOCK
    void placeBlock(V3I l)
    {
        Block_Records.BlockCost stats = Block_Records.GetCost((byte)CHB[Character_Hotbar.HBpos, 0]);
        if (stats == null) return;
        
        //Build time animation
        if (stats.buildTime > 0)
        {
            Character_States.freezeMovement = stats.freezeMovement;
            float percent = Character_States.BuildTime * (1 / (float)stats.buildTime);

            Character_Placement.Buildtime.SetActive(false);
            Character_Placement.Buildtime.gameObject.SetActive(true);
            Character_Placement.Buildtime.transform.localPosition = new Vector3(0, (-0.5f * (percent * -1)) - 0.5f, 0);
            Character_Placement.Buildtime.transform.localScale = new Vector3(0.99f, (0.99f * percent), 0.99f);
            Character_States.BuildTime++;
        }
        if (Character_States.BuildTime == stats.buildTime)
        {
            //Delete previous block model
            Block blk = world.GetBlock(l.x, l.y, l.z);
            if (blk != null && blk.model != null)
                Destroy(blk.model);

            //set block id, dmg, rotation etc
            Block block = new Block(Character_States.Placement);
            //block.tint = (byte)(Character_Hotbar.HBpos + 4);
            block.dmg = (byte)CHB[Character_Hotbar.HBpos, 1];
            block.form = (byte)CHB[Character_Hotbar.HBpos, 2];
            if (stats.rotatable)
                block.form = Rotation(point, world.transform.InverseTransformPoint(hit.point));

            //Determine weather to place locally or on the server
            if (Network_Manager.status == NetStatus.InGame)
                Network_Manager.Instance.SetBlock(l.x, l.y, l.z, block);
            else {
                world.SetBlock(l.x, l.y, l.z, block);
                world.RebuildWorld();
            }

            //clean up variables
            Character_States.placed = true;
            Character_States.BuildTime = 0;
            Character_States.lastPlaced = (byte)CHB[Character_Hotbar.HBpos, 0];
            Character_States.DOP = stats.placedDelay;
            if (Character_States.DOP == 0)
                Character_Placement.Buildtime.SetActive(false);
        }
    }
    //SHOWS DUBUG INFO LIKE BLOCK DMG
    void debugBlock(V3I l)
    {
        if (world.GetBlock(l.x, l.y, l.z) != null)
        {
            if (Input.GetKey(KeyCode.LeftShift))
                l.y++;
            Block blk = world.GetBlock(l.x, l.y, l.z);
            Debug.Log(l + " " + world.name + " " + blk + " Converted Dmg: " + World.GetDmgData(blk.dmg, true) + "  Converted Data: " + World.GetDmgData(blk.dmg, false) + " suspention: " + blk.suspension);
            Debug.Log("total CCL's: " + world.CCLCount() + " " + blk.GetCCLComponent());
            //bool[] crn = Methods.byteToBool(blk.form);
            //Debug.Log("Bytes: " + crn[0] + " " + crn[1] + " " + crn[2] + " " + crn[3] + " " + crn[4] + " " + crn[5] + " " + crn[6] + " " + crn[7] + " ");
            //Debug.Log("Size of block: " + Methods.boolToByte(Methods.byteToBool(blk.form), 4, 0) + "  Last 8 bits: " + Methods.boolToByte(Methods.byteToBool(blk.form), 1, 5));
        }
    }
    //ACTIVE DEVICE BLOCK FUNTIONALITY
    void interactBlock(V3I l)
    {
        Block block = world.GetBlock(l.x, l.y, l.z);
        if (block != null && block.model != null)
        {
            Device_Interactable inter = block.model.GetComponent<Device_Interactable>();
            if (inter == null) inter = block.model.AddComponent<Device_Interactable>();

            if (Block_Records.GetCost(block.id).advancedGUI)
                inter.DisplayGUI();
            else
                inter.DisplayHealth();
        }
    }
    //SUBTRACTS A CORNER FROM THE BLOCK
    void bevelBlock(V3I l, Vector3 c)
    {
        Block blk = world.GetBlock(l.x, l.y, l.z);
        if(blk == null) { Debug.Log("no block at " + l + " " + world); return; }
        if (blk != null && blk.dmg == 0)
        {
            blk.dmg = 1;
            blk.form = 255;
        }
        bool[] crn = Methods.byteToBool(blk.form);
        if (c.x > l.x && c.y > l.y && c.z > l.z)
            crn[1] = false;
        else if (c.x < l.x && c.y > l.y && c.z > l.z)
            crn[0] = false;
        else if (c.x > l.x && c.y > l.y && c.z < l.z)
            crn[3] = false;
        else if (c.x < l.x && c.y > l.y && c.z < l.z)
            crn[2] = false;
        else if (c.x > l.x && c.y < l.y && c.z > l.z)
            crn[5] = false;
        else if (c.x < l.x && c.y < l.y && c.z > l.z)
            crn[4] = false;
        else if (c.x > l.x && c.y < l.y && c.z < l.z)
            crn[7] = false;
        else if (c.x < l.x && c.y < l.y && c.z < l.z)
            crn[6] = false;
        blk.form = Methods.boolToByte(crn);
        world.SetBlock(l.x, l.y, l.z, blk);
        world.RebuildWorld();
        Character_States.beveled = true;
    }

    //METHODS TO SIMPLIFY THINGS
    static bool rayCast(Vector3 origin, Vector3 direction, int maxDistance)
    {
        return Physics.Raycast(origin, direction, out hit, maxDistance, LayerMask.GetMask(new string[] { "WorldStatic" , "WorldMesh"}));
    }
    static V3I CanCombine()
    {
        Block blk = world.GetBlock(point.x + normal.x, point.y + normal.y, point.z + normal.z);
        if (blk != null)
        {
            if (Block_Records.GetCrafting(blk.id) != null)
            {
                byte[,] CW = Block_Records.GetCrafting(blk.id).CombinesWith;
                for (int i = 0; i < CW.Length / 2; i++)
                    if (CW[i, 0] == CHB[Character_Hotbar.HBpos, 0])
                    {
                        Character_States.Placement = CW[i, 1];
                        return point + normal;
                    }
            }
        }
        return point;
    }
    static byte Rotation(V3I l, Vector3 p)
    {
        float x = Mathf.Abs(p.x - l.x);
        float y = Mathf.Abs(p.y - l.y);
        float z = Mathf.Abs(p.z - l.z);
        byte v = 0;
        if (p.y - l.y > 0) v = 5;

        if (z > y && z > x)
        {
            if (p.z - l.z > 0)
                v += 1;
            if (p.z - l.z < 0)
                v += 2;
        }
        else if (x > y && x > z)
        {
            if (p.x - l.x > 0)
                v += 3;
            if (p.x - l.x < 0)
                v += 4;
        }
        else if (y > z && y > x)
        {
            if (p.y - l.y > 0)
                return 5;
            if (p.y - l.y < 0)
                return 0;
        }
        return v;
    }
    static void AdjustWorld()
    {
        //ajust world to allow for sub worlds
        if (hit.transform != null)
        {
            world = null;
            Chunk cnk = hit.transform.GetComponent<Chunk>();
            if (cnk == null) cnk = hit.transform.parent.transform.GetComponent<Chunk>();
            if (cnk != null) { world = cnk.world; }
            if (world == null) { world = hit.transform.gameObject.GetComponent<World>(); }
        }
        if (world == null || world.transform == null) { world = World.main; }

        //the point to place the block at
        point = new V3I(world.transform.InverseTransformPoint(hit.point + (hit.normal / 2)) + new Vector3(0.5f, 0.5f, 0.5f));
        if (world != null && world.GetBlock(point.x, point.y, point.z) != null) normal = new V3I(0, 0, 0);
        else normal = new V3I(world.transform.InverseTransformDirection(-hit.normal * 1.45f));
    }
}