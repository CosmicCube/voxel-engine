﻿using UnityEngine;

public class Character_Placement : MonoBehaviour {
    public static GameObject Buildtime;
    public static bool CanBuild = true;
    void Start()
    {
        Buildtime = transform.GetChild(0).gameObject;
    }
    void OnTriggerEnter(Collider colision)
    {
        if (colision.gameObject.tag == "Player")
        {
            GetComponent<Renderer>().material.SetColor("_Color", new Vector4(1, 0, 0, 0.5f));
            CanBuild = false;
        }
    }
    void OnTriggerExit(Collider colision)
    {
        if (colision.gameObject.tag == "Player")
        {
            GetComponent<Renderer>().material.SetColor("_Color", new Vector4(0, 1, 0, 0.5f));
            CanBuild = true;
        }
    }
}
