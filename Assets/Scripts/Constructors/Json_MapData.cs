﻿using System;

[Serializable]
public class Json_MapData{
    public int width;
    public int height;
    public int length;
    public float killPlane;
    public float waterPlane;
    public string name;
    public string desc;
    public byte[] mapBlockIds;
    public byte[] mapBlockDmg;
    public byte[] mapBlockForm;
    public byte[] mapBlockTint;
}
