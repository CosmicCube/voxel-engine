﻿using System;
using System.Collections.Generic;

[Serializable]
public class Json_ServerList
{
    public int version;
    public string updated;
    public List<Json_Server> servers;

    public Json_ServerList()
    {
        version = 2;
        updated = DateTime.Now.ToString();
        servers = new List<Json_Server>();
        //servers.Add(new Json_Server());
    }
}

[Serializable]
public class Json_Server
{
    public string name;
    public string desc;
    public string region;
    public string ip;
    public string url;
    public int port;
    public string host;
    public string pass;
    public string map;

    public Json_Server(string ip, int port, string map)
    {
        this.ip = ip;
        this.port = port;
        this.map = map;
    }
    public Json_Server(string name, string desc, string region, string ip, int port, string map)
    {
        this.name = name;
        this.desc = desc;
        this.region = region;
        this.ip = ip;
        this.port = port;
        this.map = map;
    }
    public Json_Server(string name, string desc, string region, string ip, string url, int port, string host, string pass, string map)
    {
        this.name = name;
        this.desc = desc;
        this.region = region;
        this.ip = ip;
        this.url = url;
        this.port = port;
        this.host = host;
        this.pass = pass;
        this.map = map;
    }
}