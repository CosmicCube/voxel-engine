﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Json_Settings
{
    public static Json_Settings current;

    public int fileVersion;

    //GENERAL
    public bool JumpHold;
    public int Sensitivity;
    public bool InvertY;

    //CONTROLS
    public KeyCode MoveN;
    public KeyCode MoveS;
    public KeyCode MoveW;
    public KeyCode MoveE;
    public KeyCode Crouch;
    public KeyCode Jump;

    public KeyCode WeaponPrimary;
    public KeyCode WeaponSecondary;
    public KeyCode WeaponToggle;
    public KeyCode WeaponFire;
    public KeyCode WeaponAlt;

    public KeyCode Hotbar1;
    public KeyCode Hotbar2;
    public KeyCode Hotbar3;
    public KeyCode Hotbar4;
    public KeyCode Hotbar5;
    public KeyCode Hotbar6;
    public KeyCode Hotbar7;
    public KeyCode Hotbar8;
    public KeyCode Hotbar9;
    public KeyCode Hotbar0;
    public KeyCode ToolbarSlot;
    public KeyCode ToolbarLeft;
    public KeyCode ToolbarRight;

    public KeyCode OpenChat;
    public KeyCode GearSlot;
    public KeyCode Reload;
    public KeyCode QuickChat;
    public KeyCode LoadoutHelp;
    public KeyCode OpenShop;
    public KeyCode Settings;
    public KeyCode AbilityGear;
           
    public KeyCode AbilityToggle;
    public KeyCode Inventory;
    
    public KeyCode Ability1;
    public KeyCode Ability2;
    public KeyCode Ability3;
    public KeyCode Ability4;
    public KeyCode Ability5;
    public KeyCode Ability7;
    public KeyCode Ability8;
    public KeyCode Ability9;
    public KeyCode Ability0;




    public Json_Settings()
    {
        fileVersion = 2;

        //GENERAL
        JumpHold = true;
        Sensitivity = 2;
        InvertY = false;

        //CONTROLS
        MoveN = KeyCode.W;
        MoveS = KeyCode.S;
        MoveW = KeyCode.A;
        MoveE = KeyCode.D;

        Crouch = KeyCode.LeftControl;
        Jump   = KeyCode.Space;

        WeaponPrimary   = KeyCode.Q;
        WeaponSecondary = KeyCode.E;
        WeaponToggle    = KeyCode.None;
        WeaponFire      = KeyCode.Mouse0;
        WeaponAlt       = KeyCode.Mouse1;

        Hotbar1 = KeyCode.Alpha1;
        Hotbar2 = KeyCode.Alpha2;
        Hotbar3 = KeyCode.Alpha3;
        Hotbar4 = KeyCode.Alpha4;
        Hotbar5 = KeyCode.Alpha5;
        Hotbar6 = KeyCode.Alpha6;
        Hotbar7 = KeyCode.Alpha7;
        Hotbar8 = KeyCode.Alpha8;
        Hotbar9 = KeyCode.Alpha9;
        Hotbar0 = KeyCode.Alpha0;
        ToolbarSlot  = KeyCode.Mouse2;
        ToolbarLeft  = (KeyCode) 1;
        ToolbarRight = (KeyCode) 2;
         
        OpenChat    = KeyCode.Return;
        GearSlot    = KeyCode.F;
        Reload      = KeyCode.R;
        QuickChat   = KeyCode.C;
        LoadoutHelp = KeyCode.G;
        OpenShop    = KeyCode.V;
        Settings    = KeyCode.Escape;
        
        //old
        AbilityToggle = KeyCode.None;
        Inventory     = KeyCode.Tab;

        Ability1 = KeyCode.None;
        Ability2 = KeyCode.None;
        Ability3 = KeyCode.None;
        Ability4 = KeyCode.None;
        Ability5 = KeyCode.None;
        Ability7 = KeyCode.None;
        Ability8 = KeyCode.None;
        Ability9 = KeyCode.None;
        Ability0 = KeyCode.None;
    }
}
