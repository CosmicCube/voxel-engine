﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manifold : MonoBehaviour
{

    //Exclusive to Ingame
    public static GameObject Prefab_Hotbar { get => Resources.Load<GameObject>("Prefabs/UI/Ingame/Hotbar"); }
    public static GameObject Prefab_Crosshair { get => Resources.Load<GameObject>("Prefabs/UI/Ingame/Crosshair"); }
    public static GameObject Prefab_Buildoutline { get => Resources.Load<GameObject>("Prefabs/UI/Ingame/Buildoutline"); }
    public static GameObject Prefab_Chatbox { get => Resources.Load<GameObject>("Prefabs/UI/Ingame/Chatbox"); }
    public static GameObject Prefab_Chathistory { get => Resources.Load<GameObject>("Prefabs/UI/Ingame/Chathistory"); }

    //Can be seen in both
    public static GameObject Prefab_Topbar { get => Resources.Load<GameObject>("Prefabs/UI/Global/Topbar"); }
    public static GameObject Prefab_Settings { get => Resources.Load<GameObject>("Prefabs/UI/Global/Settings"); }
    //tabs
    public static GameObject Prefab_Tab_General { get => Resources.Load<GameObject>("Prefabs/UI/Global/Settings_Tabs/General"); }
    public static GameObject Prefab_Tab_Controls { get => Resources.Load<GameObject>("Prefabs/UI/Global/Settings_Tabs/Controls"); }
    public static GameObject Prefab_Tab_Graphics { get => Resources.Load<GameObject>("Prefabs/UI/Global/Settings_Tabs/Graphics"); }

    //Exclusive to Main menu
    public static GameObject Prefab_Sidebar { get => Resources.Load<GameObject>("Prefabs/UI/Menu/Sidebar"); }
    public static GameObject Prefab_Serverlist { get => Resources.Load<GameObject>("Prefabs/UI/Menu/Serverlist"); }
    public static GameObject Prefab_Mapselect { get => Resources.Load<GameObject>("Prefabs/UI/Menu/Mapselect"); }
}
