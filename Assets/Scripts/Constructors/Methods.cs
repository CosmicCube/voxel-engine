﻿using UnityEngine;
using System.Collections;

public class Methods
{
    public static int boolToInt(bool[] b)
    {
        int r = 0;
        foreach (bool bl in b)
            if(bl)
                r++;
        return r;
    }
    public static int boolToInt(bool[] b, int[] n)
    {
        int r = 0;
        for(int i = 0; i < n.Length; i++)
            if (b[n[i]])
                r++;
        return r;
    }
    public static bool[] byteToBool(int by)
    {
        bool[] crn = new bool[8];
        int p = 0;
        for (int i = 128; i >= 1; i = i / 2)
        {
            if (by >= i)
            {
                crn[p] = true;
                by = by - i;
            }
            else
                crn[p] = false;
            p++;
        }
        return crn;
    }

    //array of bools || p = start at || L = stop x away from end of bools 
    public static byte boolToByte(bool[] crn, int p = 1, int L = 0)
    {
        byte by = 0;
        for (int i = 1; p <= crn.Length - L; i = i * 2)
        {
            if (crn[crn.Length - p])
                by += (byte)i;
            p++;
        }
        
        return by;
    }
}
