﻿using UnityEngine;

public struct V3I
{
    //public static zero = new V3I();
    public int x;
    public int y;
    public int z;

    //Different ways to define a V3I
    public V3I(V3I v)
    {
        x = v.x;
        y = v.y;
        z = v.z;
    }
    public V3I(Vector3 v)
    {
        x = (int)v.x;
        y = (int)v.y;
        z = (int)v.z;
    }
    public V3I(Vector3 v, Vector3 v2)
    {
        x = Mathf.RoundToInt(v.x + v2.x);
        y = Mathf.RoundToInt(v.y + v2.y);
        z = Mathf.RoundToInt(v.z + v2.z);
    }
    public V3I(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public V3I(float x, float y, float z)
    {
        this.x = (int)x;
        this.y = (int)y;
        this.z = (int)z;
    }

    //Convert to some other data type
    public Vector3 ToVector3()
    {
        return new Vector3(x, y, z);
    }
    public override string ToString()
    {
        return string.Format("({0}, {1}, {2})", x, y, z);
    }
    public override bool Equals(object obj)
    {
        string str = obj.ToString();
        return str == ToString();
    }
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    //Math with V3I
    public static V3I operator +(V3I a, V3I b)
    {
        return new V3I(a.x + b.x, a.y + b.y, a.z + b.z);
    }
    public static V3I operator -(V3I a, V3I b)
    {
        return new V3I(a.x - b.x, a.y - b.y, a.z - b.z);
    }
    public static V3I operator -(V3I a)
    {
        return new V3I(-a.x, -a.y, -a.z);
    }

    //More Math
    public static V3I operator *(float d, V3I a)
    {
        return new V3I(a.x * d, a.y * d, a.z * d);
    }
    public static V3I operator *(V3I a, float d)
    {
        return new V3I(a.x * d, a.y * d, a.z * d);
    }
    public static V3I operator /(V3I a, float d)
    {
        return new V3I(a.x / d, a.y / d, a.z / d);
    }
    public static V3I operator %(V3I a, float d)
    {
        Vector3 v = new Vector3(a.x / d, a.y / d, a.z / d);
        v = v - new V3I(v);
        return new V3I(v * d);
    }

    //addition / subtraction with V3I and Vector3 (the second type becomes the same as the first)
    public static V3I     operator +(V3I a, Vector3 b)
    {
        return new V3I(a.x + (int)b.x, a.y + (int)b.y, a.z + (int)b.z);
    }
    public static Vector3 operator +(Vector3 a, V3I b)
    {
        return new Vector3(a.x + b.x, a.y + b.y, a.z + b.z);
    }
    public static V3I     operator -(V3I a, Vector3 b)
    {
        return new V3I(a.x - (int)b.x, a.y - (int)b.y, a.z - (int)b.z);
    }
    public static Vector3 operator -(Vector3 a, V3I b)
    {
        return new Vector3(a.x - b.x, a.y - b.y, a.z - b.z);
    }
    
    //stuff for IF() statments
    public static bool operator ==(V3I a, V3I b)
    {
        return a.x == b.x && a.y == b.y && a.z == b.z;
    }
    public static bool operator !=(V3I a, V3I b)
    {
        return !(a.x == b.x && a.y == b.y && a.z == b.z);
    }
}

