﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Vector3 direction;

    public void Awake()
    {
        Destroy(this.gameObject, 3);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Rigidbody rd = GetComponent<Rigidbody>();

        Destroy(this.gameObject);
        /*
        World world = World.main;
        V3I pos = new V3I(world.transform.InverseTransformPoint(gameObject.transform.position + direction / 4f));

        Block block = world.GetBlock(pos.x, pos.y, pos.z);

        if (block == null) return;
        Block_Records.BlockRecord br = Block_Records.GetRecord(block.id);
        if (br == null) return;
        int dmg = (br.toolDmg * Character_Controller.dmgBonus);

        if ((dmg * 10 + block.dmg) >= 250)
        {
           
                world.SetBlock(pos.x, pos.y, pos.z, null);
                world.RebuildWorld();
            
        }
        else
        {
            block.dmg = (byte)(dmg * 10 + block.dmg);
            world.SetBlockFast(pos.x, pos.y, pos.z, block);
            world.RebuildWorld();

            if (block.model != null)
            {
                if (block.model.GetComponent<Device_Interactable>() != null) block.model.GetComponent<Device_Interactable>().DisplayHealth();
                else block.model.AddComponent<Device_Interactable>().DisplayHealth();
            }
        }*/

    }
}