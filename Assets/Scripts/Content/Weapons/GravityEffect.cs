﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityEffect : MonoBehaviour
{
    public static GravityEffect SpawnEffect(Vector3 position, Quaternion rotation)
    {
        return null;
    }



    List<TestPlayer> testPlayers;
    GameObject attacker;
    public float maxDistance = 20f;
    public float strength = 20;
    public float stopDistance = 2.5f;
    float timeStarted = 0.0f;

	void Start()
    {
        timeStarted = Time.time;
        testPlayers = LevelManager.GetTestPlayers();

        // Remove players from list that can't be affected.
        for (int i = 0; i < testPlayers.Count; i++)
        {
            Vector3 posDiff = testPlayers[i].transform.position - attacker.transform.position;

            if (posDiff.magnitude > maxDistance || EasyDot(posDiff, Character_Controller.instance.playerCamera.transform.forward) < Mathf.Cos(10 * Mathf.Deg2Rad))
                testPlayers.RemoveAt(i--);
        }
	}

    public void SetAttacker(GameObject attacker)
    {
        this.attacker = attacker;
    }
	
	void Update()
    {
        // Clear all velocities incase effect must be destroyed.
        foreach (TestPlayer tp in testPlayers)
            tp.rigidbodyCache.velocity = Vector3.zero;

        // Destroy after half a second.
        if (Time.time - timeStarted > 0.5)
        {
            Destroy(gameObject);
            return;
        }

        foreach (TestPlayer tp in testPlayers)
        {
            Vector3 posDiff = tp.transform.position - attacker.transform.position;

            if (posDiff.magnitude < stopDistance)
                tp.rigidbodyCache.velocity = Vector3.zero;
            else
                tp.rigidbodyCache.velocity = posDiff.normalized * -strength;
        } 
    }

    private float EasyDot(Vector3 a, Vector3 b)
    {
        return Vector3.Dot(a.normalized, b.normalized);
    }
}
