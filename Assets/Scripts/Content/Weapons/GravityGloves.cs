﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityGloves : Weapon
{
    public float waitPerShot;

    private bool hasFiredThisHold;
    public GravityEffect gravityPrefab;

    public float gravityStopDistance = 2.5f;
    public float gravityStrength = 30f;

    public override void Fire(bool held, WeaponData desc, Quaternion direction, Transform position, Vector3 forward)
    {
        if (!held)
        {
            hasFiredThisHold = false;
            return;
        }

        if (held && !hasFiredThisHold)
        {
            Quaternion bulletRotation = direction * Quaternion.Euler(90, 0, 0);
            forward = Quaternion.Euler(Random.value * 5 - 2.5f, Random.value * 5 - 2.5f, 0) * forward;

            var d = Instantiate(gravityPrefab, position.position, bulletRotation);
                
                d.SetAttacker(Character_Controller.instance.gameObject);
            d.stopDistance = gravityStopDistance;
            d.strength = gravityStrength;

            //Bullet bullet = Instantiate<Bullet>(pelletPrefab, position.position, bulletRotation);
            //bullet.GetComponent<Rigidbody>().AddForce(forward * 100, ForceMode.VelocityChange);
            //bullet.direction = forward;

            hasFiredThisHold = true;
        }
    }
}
