﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Weapon
{
    private const int SHELLS = 12;

    public Bullet pelletPrefab;
    public float waitPerShot;

    private bool hasFiredThisHold;

    public override void Fire(bool held, WeaponData desc, Quaternion direction, Transform position, Vector3 forward)
    {
        if (!held)
        {
            hasFiredThisHold = false;
            return;
        }

        if (held && !hasFiredThisHold)
        {
            for (int i = 0; i < SHELLS; i++)
            {
                Quaternion bulletRotation = direction * Quaternion.Euler(90, 0, 0);
                forward = Quaternion.Euler(Random.value * 5 - 2.5f, Random.value * 5 - 2.5f, 0) * forward;

                Bullet bullet = Instantiate<Bullet>(pelletPrefab, position.position, bulletRotation);
                bullet.GetComponent<Rigidbody>().AddForce(forward * 100, ForceMode.VelocityChange);
                bullet.direction = forward;
            }

            hasFiredThisHold = true;
        }
    }
    
    void Start()
    {
		
	}
	
	void Update()
    {
		
	}
}
