﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperRifle : Weapon
{
    public Bullet pelletPrefab;
    public float waitPerShot;

    private bool hasFiredThisHold;

    public override void Fire(bool held, WeaponData desc, Quaternion direction, Transform position, Vector3 forward)
    {
        if (!held)
        {
            hasFiredThisHold = false;
            return;
        }

        if (held && !hasFiredThisHold)
        { 
            Quaternion bulletRotation = direction * Quaternion.Euler(90, 0, 0);
            Bullet bullet = Instantiate<Bullet>(pelletPrefab, position.position, bulletRotation);
            bullet.GetComponent<Rigidbody>().AddForce(forward * 100, ForceMode.VelocityChange);

            hasFiredThisHold = true;
         }
    }
    
    void Start()
    {
		
	}
	
	void Update()
    {
		
	}
}
