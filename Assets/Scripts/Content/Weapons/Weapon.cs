﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class Weapon : MonoBehaviour
{
    public abstract void Fire(bool held, WeaponData desc, Quaternion direction, Transform position, Vector3 forward);
}