﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Voxel/Weapon")]
public class WeaponData : ScriptableObject
{ 
    public string title = "Weapon";
    public int price = 100;
    public Weapon prefab;
}
