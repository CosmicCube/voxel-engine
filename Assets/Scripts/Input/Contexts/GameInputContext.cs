﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class GameInputContext : InputContext
{
    public KeyCode toggleMenu;
    public KeyCode displayContexts;

    public override InputAction MapKeyToAction(KeyCode kc)
    {
        if (kc == toggleMenu)
            return InputAction.TOGGLE_MENU;
        else if (kc == displayContexts)
            return InputAction.DISPLAY_CONTEXTS;

        return InputAction.NONE;
    }

    public override InputState MapKeyToState(KeyCode kc)
    {
        return InputState.NONE;
    }

    public override InputAction MapMouseToAction(int mc)
    {
        return InputAction.NONE;
    }

    public override InputState MapMouseToState(int mc)
    {
        return InputState.NONE;
    }

    public GameInputContext()
    {
        toggleMenu = KeyCode.Escape;
        displayContexts = KeyCode.Alpha8;

        sensitivity = 8;
        CursorLocked = false;
    }
}