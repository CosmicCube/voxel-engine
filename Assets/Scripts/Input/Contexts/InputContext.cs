﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;

public abstract class InputContext
{
    public int sensitivity;
    public bool CursorLocked;


    public abstract InputAction MapKeyToAction(KeyCode kc);
    public abstract InputState MapKeyToState(KeyCode kc);
    public abstract InputAction MapMouseToAction(int mc);
    public abstract InputState MapMouseToState(int mc);
}