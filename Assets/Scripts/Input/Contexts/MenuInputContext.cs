﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class MenuInputContext : InputContext
{
    public override InputAction MapKeyToAction(KeyCode kc)
    {
        return InputAction.NONE;
    }

    public override InputState MapKeyToState(KeyCode kc)
    {
        return InputState.NONE;
    }

    public override InputAction MapMouseToAction(int mc)
    {
        return InputAction.NONE;
    }

    public override InputState MapMouseToState(int mc)
    {
        return InputState.NONE;
    }

    public MenuInputContext()
    {
        sensitivity = 4;
        CursorLocked = false;
    }
}