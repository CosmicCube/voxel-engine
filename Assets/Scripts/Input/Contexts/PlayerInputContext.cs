﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;

public class PlayerInputContext : InputContext
{
    public KeyCode jump;
    public KeyCode forwards;
    public KeyCode backwards;
    public KeyCode left;
    public KeyCode right;
    public KeyCode toggleWeapon;

    public int shoot;

    public override InputAction MapKeyToAction(KeyCode kc)
    {
        if (kc == toggleWeapon)
            return InputAction.TOGGLE_WEAPON;

        return InputAction.NONE;
    }

    public override InputState MapKeyToState(KeyCode kc)
    {
        if (kc == forwards)
            return InputState.MOVE_FORWARD;
        else if (kc == backwards)
            return InputState.MOVE_BACKWARD;
        else if (kc == left)
            return InputState.MOVE_LEFT;
        else if (kc == right)
            return InputState.MOVE_RIGHT;
        else if (kc == jump)
            return InputState.JUMP;

        return InputState.NONE;
    }

    public override InputAction MapMouseToAction(int mc)
    {
        return InputAction.NONE;
    }

    public override InputState MapMouseToState(int mc)
    {
        if (mc == shoot)
            return InputState.SHOOT;

        return InputState.NONE;
    }

    public PlayerInputContext()
    {
        sensitivity = 12;
        CursorLocked = true;

        jump = KeyCode.Space;
        forwards = KeyCode.W;
        backwards = KeyCode.S;
        left = KeyCode.A;
        right = KeyCode.D;
        toggleWeapon = KeyCode.Q;
        shoot = 0;
    }
}