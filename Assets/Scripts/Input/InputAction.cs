﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum InputAction
{
    NONE,
    TOGGLE_MENU,
    DISPLAY_CONTEXTS,
    TOGGLE_WEAPON
}
