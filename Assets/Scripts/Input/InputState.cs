﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum InputState
{
    NONE,
    MOVE_FORWARD,
    MOVE_BACKWARD,
    MOVE_LEFT,
    MOVE_RIGHT,
    SHOOT,
    JUMP,
    CROUCH
}
