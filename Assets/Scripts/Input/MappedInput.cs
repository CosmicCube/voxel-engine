﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class MappedInput
{
    public List<InputAction> actions;
    public List<InputState> states;

    public MappedInput()
    {
        actions = new List<InputAction>();
        states = new List<InputState>();
    }

    public void Clear()
    {
        actions.Clear();
        states.Clear();
    }


    public bool Jump
    {
        get
        {
            return states.Contains(InputState.JUMP);
        }
    }

    public float MovY
    {
        get
        {
            float result = 0;

            if (states.Contains(InputState.MOVE_FORWARD))
                result += 1;
            
            if (states.Contains(InputState.MOVE_BACKWARD))
                result -= 1;

            return result;
        }
    }

    public float MovX
    {
        get
        {
            float result = 0;

            if (states.Contains(InputState.MOVE_RIGHT))
                result += 1;

            if (states.Contains(InputState.MOVE_LEFT))
                result -= 1;

            return result;
        }
    }

    public bool Shoot
    {
        get
        {
            return states.Contains(InputState.SHOOT);
        }
    }

    public bool Crouch
    {
        get
        {
            return states.Contains(InputState.CROUCH);
        }
    }

    public bool AnyInput
    {
        get
        {
            return actions.Count != 0 || states.Count != 0;
        }
    }

    public bool ToggleWeapon
    {
        get
        {
            return actions.Contains(InputAction.TOGGLE_WEAPON);
        }
    }

    public bool ToggleMenu
    {
        get
        {
            return actions.Contains(InputAction.TOGGLE_MENU);
        }
    }
}