﻿using UnityEngine;
[System.Serializable]
public class DiscordJoinEvent : UnityEngine.Events.UnityEvent<string> { }

[System.Serializable]
public class DiscordSpectateEvent : UnityEngine.Events.UnityEvent<string> { }

[System.Serializable]
public class DiscordJoinRequestEvent : UnityEngine.Events.UnityEvent<DiscordRpc.JoinRequest> { }

public class Discord_Manager : MonoBehaviour
{
    public static Discord_Manager instance;
    public DiscordRpc.RichPresence presence;
    readonly string applicationId = "380290135939350528";
    public string optionalSteamId;
    //public UnityEngine.Events.UnityEvent onConnect;
    //public UnityEngine.Events.UnityEvent onDisconnect;
    //public DiscordJoinEvent onJoin;
    //public DiscordJoinEvent onSpectate;
    //public DiscordJoinRequestEvent onJoinRequest;

    DiscordRpc.EventHandlers handlers;

    public void UpdateDiscord()
    {
        //presence.partyId = "vct";
        //presence.partySize = 1;
        //presence.partyMax = 8;
        //presence.joinSecret = "e1cb30d2ee025ed05c71ea495f770b76454ee4ea";
        if ((int)State_Manager.state < 16)
        {
            presence.largeImageKey = "logo";
            presence.largeImageText = "In Menus";

            presence.details = "Testing code";
            presence.state = "Flying Solo";
        }
        else
        {
            presence.largeImageKey = "block_brick";
            presence.largeImageText = "Playing in " + State_Manager.state.ToString().Substring(2) + " mode";
            presence.smallImageKey = "char_tank";
            presence.smallImageText = "Tank";

            presence.details = "Testing code";
            presence.state = "Flying Solo";

            System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
            int unix_timestamp = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
            presence.endTimestamp = unix_timestamp + 600;
            //presence.matchSecret = "4b2fdce12f639de8bfa7e3591b71a0d679d7c931";
            //presence.spectateSecret = "e7eb30d2ee025ed05c71ea495f770b76454ee4ea";
        }
#if UNITY_EDITOR
        presence.largeImageKey = "unity";
        presence.details = "Testing code";
#endif
        DiscordRpc.UpdatePresence(ref presence);
    }


    public void ReadyCallback()
    {
        Debug.Log("Discord: ready");
        UpdateDiscord();
    }

    public void DisconnectedCallback(int errorCode, string message)
    {
        Debug.Log(string.Format("Discord: disconnect {0}: {1}", errorCode, message));
    }

    public void ErrorCallback(int errorCode, string message)
    {
        Debug.Log(string.Format("Discord: error {0}: {1}", errorCode, message));
    }

    public void JoinCallback(string secret)
    {
        Debug.Log(string.Format("Discord: join ({0})", secret));
    }

    public void SpectateCallback(string secret)
    {
        Debug.Log(string.Format("Discord: spectate ({0})", secret));
    }

    public void RequestCallback(DiscordRpc.JoinRequest request)
    {
        Debug.Log(string.Format("Discord: join request {0}: {1}", request.username, request.userId));
    }

    void Start()
    {
    }

    void Update()
    {
        DiscordRpc.RunCallbacks();
    }

    void OnEnable()
    {
        Debug.Log("Discord: init");
        if(instance == null) instance = this;
        handlers = new DiscordRpc.EventHandlers();
        handlers.readyCallback = ReadyCallback;
        handlers.disconnectedCallback += DisconnectedCallback;
        handlers.errorCallback += ErrorCallback;
        handlers.joinCallback += JoinCallback;
        handlers.spectateCallback += SpectateCallback;
        handlers.requestCallback += RequestCallback;
        DiscordRpc.Initialize(applicationId, ref handlers, true, optionalSteamId);
    }

    void OnDisable()
    {
        Debug.Log("Discord: shutdown");
        DiscordRpc.Shutdown();
    }

    void OnDestroy()
    {

    }
}
