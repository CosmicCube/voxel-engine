﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Input_Manager : MonoBehaviour
{
    bool settingsOpen = false;
    GameObject Topbar;
    GameObject Settings;

    void Start()
    {
        //File.Create(Directory.GetCurrentDirectory() + "/ test_asdf.txt");

    }

    void Update()
    {
        MappedInput mi = InputManager.GetFrameInput();

        if (mi.actions.Contains(InputAction.DISPLAY_CONTEXTS))
            foreach (InputContext ic in InputManager.self.contextStack)
                Debug.Log(ic.GetType());

        if (mi.AnyInput)
        {
            if (mi.ToggleMenu)
            {
                State_Manager.Toggle_Settings();
            }
        }
    }
}
