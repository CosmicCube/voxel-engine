﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu_Manager : MonoBehaviour
{
    public static Transform canvas;

    //Exclusive to Ingame
    public static GameObject Crosshair;
    public static GameObject Hotbar;
    public static GameObject Buildoutline;
    public static GameObject Chatbox;
    public static GameObject Chathistory;

    //Can be seen in both
    public static GameObject Topbar;
    public static GameObject Settings;

    //Exclusive to Main menu
    public static GameObject Serverlist;
    public static GameObject Mapselect;
    public static GameObject Sidebar;
    public static Vector3 MinimizeMenu;

    void Start()
    {
        canvas = GetCanvas().transform;
        UpdateDisplay();
    }
    //void Update()
    //{
    //    if (Mainmenu != null)
    //    {
    //        if (MinimizeMenu != null && MinimizeMenu != Vector3.zero)
    //            Mainmenu.GetComponent<RectTransform>().SetPositionAndRotation(Vector3.Lerp(Mainmenu.transform.position, MinimizeMenu, Time.deltaTime * 3), Quaternion.identity);
    //    }
    //}

    public static void UpdateDisplay()
    {
        if (canvas == null)
            canvas = GetCanvas().transform;

        switch (State_Manager.state)
        {
            case State.M_Main:
                if (Serverlist != null) Destroy(Serverlist);
                if (Mapselect != null) Destroy(Mapselect);
                if (Sidebar == null) Sidebar = Instantiate(Manifold.Prefab_Sidebar, canvas);
                if (Topbar == null) Topbar = Instantiate(Manifold.Prefab_Topbar, canvas);
                Sidebar.GetComponent<UI_Sidebar>().UpdateButtons();
                break;
            case State.M_Customs:
                if (Mapselect != null) Destroy(Mapselect);
                if (Serverlist == null) Serverlist = Instantiate(Manifold.Prefab_Serverlist, canvas);
                Sidebar.GetComponent<UI_Sidebar>().UpdateButtons();
                break;
            case State.M_Map:
                if (Serverlist != null) Destroy(Serverlist);
                if (Mapselect == null) Mapselect = Instantiate(Manifold.Prefab_Mapselect, canvas);
                Sidebar.GetComponent<UI_Sidebar>().UpdateButtons();
                break;


            case State.G_Casual:
                if (Hotbar == null && !State_Manager.settings) Hotbar = Instantiate(Manifold.Prefab_Hotbar, canvas);
                if (Crosshair == null && !State_Manager.settings) Crosshair = Instantiate(Manifold.Prefab_Crosshair, canvas);
                if (Buildoutline == null) Buildoutline = Instantiate(Manifold.Prefab_Buildoutline);
                UI_Chathistory.CloseHistory();
                break;
        }
        if (State_Manager.settings)
        {
            if (Settings == null) Settings = Instantiate(Manifold.Prefab_Settings, canvas);
            if ((int)State_Manager.state > 15)
            {
                if (Hotbar != null) Destroy(Hotbar);
                if (Hotbar != null) Destroy(Crosshair);
                if (Topbar == null) Topbar = Instantiate(Manifold.Prefab_Topbar, canvas);
                if (Chatbox == null) Chatbox = Instantiate(Manifold.Prefab_Chatbox, canvas);
            }
            Topbar.transform.SetAsLastSibling();
        }
        else
        {
            if (Topbar != null && (int)State_Manager.state > 15) Destroy(Topbar);
            if (Settings != null) Destroy(Settings);
            if (Chatbox != null) Destroy(Chatbox);
        }
    }
    public static void CreateChatHistory()
    {
        if (Chathistory == null) Chathistory = Instantiate(Manifold.Prefab_Chathistory, canvas);
    }
    public static GameObject GetCanvas()
    {
        GameObject canvas = GameObject.Find("Canvas");
        if (canvas == null || canvas.GetComponent<Canvas>() == null || canvas.name != "Canvas")
        {
            canvas = new GameObject("Canvas", typeof(RectTransform));
            canvas.AddComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            canvas.AddComponent<CanvasScaler>();
            canvas.AddComponent<GraphicRaycaster>();
        }
        return canvas;
    }
}
