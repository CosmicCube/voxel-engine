﻿using System.Net.Sockets;
using UnityEngine;
using System;
using System.IO;
using System.Net;
using UnityEngine.SceneManagement;
using System.Text.RegularExpressions;

public enum NetStatus
{
    None,
    Unidentified,
    Master,
    InGame,
}

public class Network_Manager : MonoBehaviour
{
    public static Network_Manager Instance;
    public static Connection connection;
    public static NetStatus status;

    static IPAddress address;

    public int id;

    void Start()
    {
        Instance = this;
        status = NetStatus.None;

        //try {
        //    address = Dns.GetHostAddresses("voxel.prosnap.co")[0];
        //    Connect(address, 9825);
        //    connection.ConnectFailed += new Connection.OnConnectFailedHandler(Client_ConnectFailed);
        //}
        //catch(Exception ex){ }
    }

    public void Connect(IPAddress ip, int port)
    {
        if (connection != null) connection.Disconnect();
        if (ip == null) return;
        connection = new Connection();
        connection.OnConnect += new Connection.OnConnectEventHandler(Client_OnConnect);
        connection.DataReceived += new Connection.DataReceivedEventHandler(Client_DataReceived);
        connection.OnDisconnect += new Connection.OnDisconnectEventHandler(Client_Disconnected);
        connection.Connect(ip, port);
    }
    //RETRY CONNECTION
    //void Client_ConnectFailed(Connection sender, string ex)
    //{
    //    Debug.Log(address + " " + ex);
    //}
    //CONNECTION HAS BEEN MADE
    void Client_OnConnect(Connection sender, bool connected)
    {
        if (connected)
        {
            Debug.Log("> Connection established " + sender.ip.ToString());
            connection.receive();
            status = NetStatus.Unidentified;
        }
    }
    void Client_Disconnected(Connection sender)
    {
        connection = null;
        status = NetStatus.None;
    }

    //RECIEVE REQUESTS
    void Client_DataReceived(Connection sender, ReceiveBuffer e)
    {
        BinaryReader br = new BinaryReader(e.BufStream);
        int header = br.ReadInt32();
        if (header == (int)Packets.Type)
        {
            string type = br.ReadString();
            if (type == "master")
            {
                status = NetStatus.Master;
                connection.DataReceived -= new Connection.DataReceivedEventHandler(Client_DataReceived);
                connection.DataReceived += new Connection.DataReceivedEventHandler(Client_MasterReceived);
            }
            else if (new Regex("game", RegexOptions.IgnoreCase).Match(type).Success)
            {
                //Debug.Log("server is a game");
                status = NetStatus.InGame;
                connection.DataReceived -= new Connection.DataReceivedEventHandler(Client_DataReceived);
                connection.DataReceived += new Connection.DataReceivedEventHandler(Client_GameReceived);
                TaskExecutorScript.que.ScheduleTask(new Task(delegate { SceneManager.LoadScene("Game"); }));
                id = byte.Parse(new Regex(@"(-id) (\d)", RegexOptions.IgnoreCase).Match(type).Groups[2].ToString());
            }
        }

    }
    void Client_MasterReceived(Connection sender, ReceiveBuffer e)
    {
        BinaryReader br = new BinaryReader(e.BufStream);
        Commands header = (Commands)br.ReadInt32();
        switch (header)
        {
            case Commands.ServerList:
                string serverlist = br.ReadString();
                serverlist = serverlist.Replace("\"updated\": \"\"", string.Format("\"updated\": \"{0}\"", DateTime.Now));
                File.WriteAllText(Application.persistentDataPath + "/ServerList.json", serverlist);
                TaskExecutorScript.que.ScheduleTask(new Task(delegate
                {
                    UI_Serverlist.ClearEntries();
                    UI_Serverlist.LoadServerList();
                    UI_Serverlist.self.UpdateServers();
                }));
                break;
        }
    }
    void Client_GameReceived(Connection sender, ReceiveBuffer e)
    {
        BinaryReader br = new BinaryReader(e.BufStream);
        Packets header = (Packets)br.ReadInt32();
        byte b;
        string s;
        switch (header)
        {
            case Packets.ChatMsg:
                s = br.ReadString();
                TaskExecutorScript.que.ScheduleTask(new Task(delegate { UI_Chathistory.PostChat($"> {s}"); }));
                break;
            case Packets.BlockPlacement:
                BlockPlaced(br);
                break;
            case Packets.PlayerJoined:
                b = br.ReadByte();
                s = br.ReadString();
                TaskExecutorScript.que.ScheduleTask(new Task(delegate { Debug.Log(b + " player joined: " + s); Character_Gen.self.playerInit(b, s); }));
                break;
            case Packets.PlayerLeft:
                b = br.ReadByte();
                s = br.ReadString();
                TaskExecutorScript.que.ScheduleTask(new Task(delegate { Debug.Log(b + " player left: " + s); Character_Gen.self.playerDestroy(b); }));
                break;
            case Packets.Location:
                b = br.ReadByte();
                Vector3 loc = new Vector3((float)br.ReadInt64() / 10, (float)br.ReadInt64() / 10, (float)br.ReadInt64() / 10);
                Vector3 rot = new Vector3((float)br.ReadInt64() / 10, (float)br.ReadInt64() / 10, (float)br.ReadInt64() / 10);
                TaskExecutorScript.que.ScheduleTask(new Task(delegate { Character_Gen.self.playerMove(b, loc, rot); }));
                break;
            case Packets.WorldName:
                s = br.ReadString();
                //TaskExecutorScript.que.ScheduleTask(new Task(delegate { World_Sub.main.LoadFromFile(s);}));
                break;
        }
    }

    //RECIEVE A BLOCK PLACEMENT
    void BlockPlaced(BinaryReader br)
    {
        int x = br.ReadInt32();
        int y = br.ReadInt32();
        int z = br.ReadInt32();
        Block blk = new Block(br.ReadByte(), br.ReadByte(), br.ReadByte(), br.ReadByte());
        TaskExecutorScript.que.ScheduleTask(new Task(delegate
        {
            World.main.SetBlock(x, y, z, blk);
            World.main.RebuildWorld(false);
        }));
    }

    //SEND TEXT
    public void SendPacket(string text, int pkt = (int)Packets.ChatMsg)
    {
        BinaryWriter bw = new BinaryWriter(new MemoryStream());
        bw.Write(pkt);
        if (text != null)
            bw.Write(text);
        bw.Close();
        byte[] data = ((MemoryStream)bw.BaseStream).ToArray();
        bw.BaseStream.Dispose();
        connection.Send(data, 0, data.Length);
        data = null;
    }
    //SEND TEXT
    public void SendLocation(Vector3 loc, Vector3 rot)
    {
        BinaryWriter bw = new BinaryWriter(new MemoryStream());
        bw.Write((int)Packets.Location);
        bw.Write(id);
        bw.Write((long)(loc.x * 10));
        bw.Write((long)(loc.y * 10));
        bw.Write((long)(loc.z * 10));
        bw.Write((long)(rot.x * 10));
        bw.Write((long)(rot.y * 10));
        bw.Write((long)(rot.z * 10));
        bw.Close();
        byte[] data = ((MemoryStream)bw.BaseStream).ToArray();
        bw.BaseStream.Dispose();
        connection.Send(data, 0, data.Length);
        data = null;
    }
    //SEND SET BLOCK
    public void SetBlock(int x, int y, int z, Block block)
    {
        BinaryWriter bw = new BinaryWriter(new MemoryStream());
        bw.Write((int)Packets.BlockPlacement);
        bw.Write(x);
        bw.Write(y);
        bw.Write(z);
        bw.Write(block.id);
        bw.Write(block.dmg);
        bw.Write(block.form);
        bw.Write(block.tint);
        bw.Close();
        byte[] data = ((MemoryStream)bw.BaseStream).ToArray();
        bw.BaseStream.Dispose();
        connection.Send(data, 0, data.Length);
        data = null;
    }
#if !UNITY_EDITOR
    //CLOSE CONNECTION ON EXIT
    void OnApplicationQuit() => CloseAllConnections();
#endif
    void OnDestroy() => CloseAllConnections();
    void CloseAllConnections()
    {
        Debug.Log("Closing network");
        if (connection != null)
            connection.Disconnect();
        Network_Localhost.Close_Localhost();
    }



    //public void SendChat()
    //{
    //    InputField IF = Options.transform.GetChild(1).GetComponent<InputField>();
    //    if(IF.text != null || IF.text != "")
    //    {
    //        if (Network_Manager.netInstance != null)
    //        {
    //            Network_Manager.netInstance.SendPacket(IF.text);
    //            IF.text = "";
    //        }
    //    }
    //}
}
