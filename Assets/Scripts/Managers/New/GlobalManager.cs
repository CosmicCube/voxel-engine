﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class GlobalManager : MonoBehaviour
{
    public static GlobalManager Instance
    {
        get;
        private set;
    }

    public InputManager InputManager
    {
        get;
        private set;
    }

    // Awake should not be called more than once.
    void Awake()
    {
        Instance = this;
        gameObject.AddComponent(typeof(InputManager));
        InputManager = gameObject.GetComponent<InputManager>();
        InputManager.playerContext = InputManager.LoadContext<PlayerInputContext>("/playerInput.txt");
        InputManager.menuContext = InputManager.LoadContext<MenuInputContext>("/menuInput.txt");
        InputManager.gameContext = InputManager.LoadContext<GameInputContext>("/gameInput.txt");
    }

    void Start()
    {
#if !UNITY_EDITOR
        SceneManager.LoadScene("Menu-Main Testing", LoadSceneMode.Additive);
#endif
    }

    public static void EnsureCreation()
    {
#if UNITY_EDITOR
        SceneManager.LoadScene("Start", LoadSceneMode.Additive);
#endif
    }
}