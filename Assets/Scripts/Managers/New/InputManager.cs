﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using Newtonsoft.Json;

public class InputManager : MonoBehaviour
{
    public static InputManager self;

    public PlayerInputContext playerContext;
    public MenuInputContext menuContext;
    public GameInputContext gameContext;

    public MappedInput currentMappedInput;
    public List<InputContext> contextStack;

    public static MappedInput GetFrameInput()
    {
        return self.currentMappedInput;
    }

    private void Awake()
    {        
        if (self == null)
        {
            self = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        playerContext = LoadContext<PlayerInputContext>("/playerInput.txt");
        menuContext   = LoadContext<MenuInputContext>("/menuInput.txt");
        gameContext   = LoadContext<GameInputContext>("/gameInput.txt");

        contextStack = new List<InputContext>();
        currentMappedInput = new MappedInput();

        SetContext(gameContext);
    }

    public static T LoadContext<T>(string fileName) where T : InputContext
    {
        try
        {
            return JsonConvert.DeserializeObject<T>(File.ReadAllText(Application.persistentDataPath + fileName));
        }
        catch
        {
            T newT = (T) Activator.CreateInstance(typeof(T));
            File.WriteAllText(Application.persistentDataPath + fileName, JsonConvert.SerializeObject(newT, Formatting.Indented));
            return newT;
        }
    }

    public void SetContext(InputContext ic)
    {
        contextStack.Clear();
        contextStack.Add(ic);
    }

    public void PushContext(InputContext ic)
    {
        contextStack.Add(ic);
    }

    public void PopContext()
    {
        contextStack.RemoveAt(contextStack.Count - 1);
    }

    public void Update()
    {
        List<KeyCode> keysAction = new List<KeyCode>();
        List<KeyCode> keysState  = new List<KeyCode>();
        List<int> mouseAction = new List<int>();
        List<int> mouseState = new List<int>();

        foreach (KeyCode kc in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKey(kc))
                keysState.Add(kc);

            if (Input.GetKeyDown(kc))
                keysAction.Add(kc);
        }

        for (int i = 0; i < 3; i++)
        {
            if (Input.GetMouseButton(i))
                mouseState.Add(i);

            if (Input.GetMouseButtonDown(i))
                mouseAction.Add(i);
        }

        currentMappedInput.Clear();

        foreach (KeyCode kc in keysAction)
        {
            foreach (InputContext ic in contextStack.AsEnumerable().Reverse())
            {
                InputAction ia = ic.MapKeyToAction(kc);
                if (ia != InputAction.NONE)
                {
                    currentMappedInput.actions.Add(ia);
                    break;
                }
            }
        }

        foreach (KeyCode kc in keysState)
        {
            foreach (InputContext ic in contextStack.AsEnumerable().Reverse())
            {
                InputState isa = ic.MapKeyToState(kc);
                if (isa != InputState.NONE)
                {
                    currentMappedInput.states.Add(isa);
                    break;
                }
            }
        }

        foreach (int mc in mouseState)
        {
            foreach (InputContext ic in contextStack.AsEnumerable().Reverse())
            {
                InputState isa = ic.MapMouseToState(mc);
                if (isa != InputState.NONE)
                {
                    currentMappedInput.states.Add(isa);
                    break;
                }
            }
        }

        foreach (int mc in mouseAction)
        {
            foreach (InputContext ic in contextStack.AsEnumerable().Reverse())
            {
                InputAction isa = ic.MapMouseToAction(mc);
                if (isa != InputAction.NONE)
                {
                    currentMappedInput.actions.Add(isa);
                    break;
                }
            }
        }
    }
}