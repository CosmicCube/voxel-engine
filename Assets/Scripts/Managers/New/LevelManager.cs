﻿using UnityEngine;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance
    {
        get;
        private set;
    }

    public List<TestPlayer> testPlayers;

    void Awake()
    {
        Instance = this;
        // GlobalManager.EnsureCreation();
        
    }

    void Start()
    {
        testPlayers = new List<TestPlayer>(GameObject.FindObjectsOfType<TestPlayer>());
    }

    public static List<TestPlayer> GetTestPlayers()
    {
        return new List<TestPlayer>(GameObject.FindObjectsOfType<TestPlayer>());
    }
}