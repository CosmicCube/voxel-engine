﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public Canvas menuCanvas;

    void Awake()
    {
        GlobalManager.EnsureCreation();
    }

    void Start()
    {
        
    }
        
    void Update()
    {           

    }

    public void OnPlayLocal()
    {
        // GameManager.SetGameMode(GameMode.LOCAL);
        // GameManager.ChangeScene(gameObject.scene, "Scenes/New/Level");
    }

    public void OnPlayMulti()
    {
        // GameManager.SetGameMode(GameMode.MULTI);
        // GameManager.ChangeScene(gameObject.scene, "Scenes/New/Level");
    }

    public void OnExit()
    {

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}