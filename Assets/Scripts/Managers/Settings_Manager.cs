﻿using UnityEngine;
using System.IO;
using System.Text.RegularExpressions;

public class Settings_Manager : MonoBehaviour {
    Json_Settings Key;
    static string json;

    void Awake () {
        if (File.Exists(Application.persistentDataPath + "/Settings.json")) { LoadSettings(); }
        else { Json_Settings.current = new Json_Settings(); SaveSettings(); }
    }

    static void SaveSettings()
    {
        json = JsonUtility.ToJson(Json_Settings.current);
        File.WriteAllText(Application.persistentDataPath + "/Settings.json", json);
    }
    public static string GetSetting_int(string name) {
        return new Regex("\"" + name + "\":(\\d+)").Match(json).Groups[1].Value;
    }
    public static bool GetSetting_bool(string name)
    {
        return (new Regex("\"" + name + "\":(\\w+)").Match(json).Groups[1].Value == "true");
    }
    public static void SetSetting(string name, int value)
    {
        Regex rgx = new Regex("\"" + name + "\":\\d+");
        json = rgx.Replace(json, "\"" + name + "\":" + value);
        Json_Settings.current = JsonUtility.FromJson<Json_Settings>(json);
        SaveSettings();
    }
    public static void SetSetting(string name, bool value)
    {
        Regex rgx = new Regex("\"" + name + "\":\\w+");
        json = rgx.Replace(json, "\"" + name + "\":" + value.ToString().ToLower() + "");
        Json_Settings.current = JsonUtility.FromJson<Json_Settings>(json);
        SaveSettings();
    }
    static void LoadSettings()
    {
        json = File.ReadAllText(Application.persistentDataPath + "/Settings.json");
        Json_Settings settings = JsonUtility.FromJson<Json_Settings>(json);
        Json_Settings.current = settings;
        if(settings.fileVersion != 2)
        {
            Json_Settings.current = new Json_Settings();
            SaveSettings();
        }
    }
}
