﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum State
{
    None,
    M_Main,
    M_Customs,
    M_Map,

    G_Casual = 16,
    G_Offline = 17,
    G_Brawl = 18,
}


public class State_Manager : MonoBehaviour {
    public static State state;
    public static bool settings;
    public static State_Manager instance;

    void Awake() {
        //Move this object to a scene outside of the other scene
        //or destroy it if  a copy of it already exists
        if (instance == null) {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else
            Destroy(gameObject);

        //set the state manager according to which scene is active
        if (SceneManager.GetActiveScene().name == "Menu-Main")
        {
            state = State.M_Main;
        }
        else
        {
            state = State.G_Casual;
            UnityEngine.Cursor.visible = false;
        }
    
    }

    //LOAD SCENES
    public static void Load_Game(){
        state = State.G_Casual;
        AsyncOperation load = SceneManager.LoadSceneAsync("Game");
        load.completed += Load_completed;
        InputManager.self.SetContext(InputManager.self.gameContext);
        InputManager.self.PushContext(InputManager.self.playerContext);
    }
    public static void Load_Menu(){
        state = State.M_Main;
        settings = false;
        AsyncOperation load = SceneManager.LoadSceneAsync("Menu-Main");
        load.completed += Load_completed;

        InputManager.self.SetContext(InputManager.self.gameContext);


    }
    //UPDATE DISPLAY AFTER LOADING SCENE
    static void Load_completed(AsyncOperation obj) {
        Menu_Manager.UpdateDisplay();
        Discord_Manager.instance.UpdateDiscord();

    }

    public static void Toggle_Settings()
    {
        settings = !settings;

        InputManager.self.SetContext(InputManager.self.gameContext);

        if (settings)
        {
            InputManager.self.PushContext(InputManager.self.menuContext);
        }
        else
        {
            InputManager.self.SetContext(InputManager.self.gameContext);

            if ((int)state > 15)
                InputManager.self.PushContext(InputManager.self.playerContext);          
        }
        
        if ((int)state > 15)
            UnityEngine.Cursor.visible = settings;
        else
            UnityEngine.Cursor.visible = true; 
        Menu_Manager.UpdateDisplay();   
    }
}
