﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.IO;

namespace Refactor
{
    public class Map : MonoBehaviour
    {
        private string mapName;

        private MapChunk[,,] chunks;

        public GameObject chunkPrefab;

        public void ModifyBlock(V3I pos, Block block)
        {
            chunks[pos.x / MapChunk.CHUNK_SIZE,
                   pos.y / MapChunk.CHUNK_SIZE,
                   pos.z / MapChunk.CHUNK_SIZE].ModifyBlock(pos % Chunk.CHUNK_SIZE, block);
        }

        public void ApplyChanges()
        {
            foreach (MapChunk chunk in chunks)
                if (chunk.isDirty)
                    chunk.Rebuild();
        }

        public void LoadFromFile(string file)
        {
            using (MemoryStream ms = new MemoryStream(Resources.Load<TextAsset>(file).bytes))
            using (BinaryReader br = new BinaryReader(ms))
            {
                string name = br.ReadString();
                float killPlane = br.ReadSingle();
                float waterPlane = br.ReadSingle();
                short length = br.ReadInt16();
                short height = br.ReadInt16();
                short width = br.ReadInt16();

                chunks = new MapChunk[width / Chunk.CHUNK_SIZE + 1, height / Chunk.CHUNK_SIZE + 1, length / Chunk.CHUNK_SIZE + 1];

                for (int x = 0; x <= width / Chunk.CHUNK_SIZE; x++)
                    for (int y = 0; y <= height / Chunk.CHUNK_SIZE; y++)
                        for (int z = 0; z <= length / Chunk.CHUNK_SIZE; z++)
                            chunks[x, y, z] = CreateChunk(x * Chunk.CHUNK_SIZE, y * Chunk.CHUNK_SIZE, z * Chunk.CHUNK_SIZE, this);

                int dataSize = length * width * height * 4;

                byte[] data = br.ReadBytes(dataSize);
            }
        }

        private MapChunk CreateChunk(int x, int y, int z, Map map)
        {
            GameObject newChunkObject = Instantiate(chunkPrefab, new Vector3(x, y, z), Quaternion.Euler(Vector3.zero));
            MapChunk newChunk = newChunkObject.GetComponent<MapChunk>();
            newChunkObject.transform.parent = map.transform;

            newChunk.Init(new V3I(x, y, z));

            return newChunk;
        }
    }
}
