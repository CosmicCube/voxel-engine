﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Refactor
{
    public class MapChunk : MonoBehaviour
    {
        public const int CHUNK_SIZE = 16;

        private new MeshCollider collider;
        private MeshFilter filter;

        private Map map;
        private V3I pos;

        private Block[,,] blocks;
        public bool isDirty { get; set; }

        public void Start()
        {
            blocks = new Block[CHUNK_SIZE, CHUNK_SIZE, CHUNK_SIZE];
        }

        public void Init(V3I pos)
        {
            this.pos = pos;
            name = "Chunk_" + pos.x / CHUNK_SIZE + "-" + pos.y / CHUNK_SIZE + "-" + pos.z / CHUNK_SIZE;
        }

        public void ModifyBlock(V3I pos, Block block)
        {
            blocks[pos.x, pos.y, pos.z] = block;
        }

        public void Rebuild()
        {
            MeshData meshData = new MeshData();

            foreach (Block block in blocks)
            {
                ConstructAndAddMeshData(meshData, block);
            }

            filter.sharedMesh.vertices = meshData.vertices.ToArray();
        }

        private void ConstructAndAddMeshData(MeshData meshDat, Block block)
        {

        }
    }
}