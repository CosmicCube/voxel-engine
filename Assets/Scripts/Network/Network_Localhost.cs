﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using UnityEngine;

public class Network_Localhost
{

    public static Network_Localhost instance;
    public static Process local_server;
    private static int port = 9837;
    public Network_Localhost()
    {
        if (instance != null)
            Close_Localhost();
        instance = this;
    }

    public static void Close_Localhost()
    {
        if (local_server == null) return;
        if (!local_server.HasExited)
            local_server.Kill();
        local_server.Close();
        local_server.Dispose();
        local_server = null;
    }

    //LAUNCH LOCALHOST
    public void Launch_Localhost()
    {
        if (local_server != null && !local_server.HasExited)
        {
            UnityEngine.Debug.Log("Already running");
            return;
        }


        string path = Directory.GetCurrentDirectory();
        if (File.Exists(path + "/StandaloneNetworkStuff/StandaloneGame/bin/Debug/StandaloneGame.exe"))
            path = path + "/StandaloneNetworkStuff/StandaloneGame/bin/Debug/StandaloneGame.exe";
        else if (File.Exists(path + "/Unmanaged/server_game.exe"))
            path = path + "/Unmanaged/server_game.exe";
        else
        {
#if UNITY_EDITOR
            UnityEngine.Debug.Log($"ERROR: unable to find server at {path}");
#else
            UI_Chathistory.PostChat($"ERROR: unable to find server at {path}");
#endif
            return;
        }
        UnityEngine.Debug.Log($"Launching server from: {path}");

        ProcessStartInfo psi = new ProcessStartInfo(path);
        psi.WorkingDirectory = Directory.GetCurrentDirectory() + "/Unmanaged";
        psi.UseShellExecute = false;
        psi.LoadUserProfile = false;
#if UNITY_EDITOR
        psi.RedirectStandardOutput = true;
        psi.RedirectStandardError = true;
        psi.WindowStyle = ProcessWindowStyle.Hidden;
        psi.CreateNoWindow = true;
#else
        psi.RedirectStandardOutput = false;
        psi.RedirectStandardError = false;
        psi.WindowStyle = ProcessWindowStyle.Hidden;
        psi.CreateNoWindow = false;
#endif
        psi.Arguments = $"-mode 0 -map \"{UI_Mapselect.map}\" -port {port}";

        local_server = new Process();
        local_server.StartInfo = psi;
        try
        {
            local_server.Start();
            local_server.BeginOutputReadLine();
            if (psi.RedirectStandardOutput) local_server.OutputDataReceived += new DataReceivedEventHandler(DataReceived);
            if (psi.RedirectStandardOutput) local_server.ErrorDataReceived += new DataReceivedEventHandler(DataReceived);
        }
        catch (Exception ex)
        {
            UnityEngine.Debug.Log(ex);
        }

    }

    static void DataReceived(object sender, DataReceivedEventArgs e)
    {
        if (!string.IsNullOrEmpty(e.Data))
        {
            UnityEngine.Debug.Log(e.Data);
        }
    }
}
