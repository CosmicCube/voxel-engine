﻿using System.Net;
using UnityEngine;
using System.Collections;


class Network_Ping : MonoBehaviour{
    public IPAddress ip;

    public Network_Ping(IPAddress ip)
    {
        Debug.Log("made; " + ip);
        this.ip = ip;
    }
    public IEnumerator PingIP()
    {
        Ping ping = new Ping(ip.ToString());
        Debug.Log("pinging: " + ip);
        while(!ping.isDone)
            yield return null;
        PingCompleted(this, ping);
    }
    
    public delegate void OnPingCompleted(Network_Ping np, Ping p);
    public event OnPingCompleted PingCompleted;
}


