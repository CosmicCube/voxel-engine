﻿using UnityEngine;

class PlaySelection : MonoBehaviour
{
    public void Selection(int mode)
    {
        // 0 = public, 1 = create, 2 = join, 3 = offline, can enum later

        switch (mode)
        {
            case 0:
                // message master server
                // receive command to be placed in match
                break;
            case 1:
                // Start server_game.exe bound to all IPs
                // Tell master server about game
                break;
            case 2:
                // Ask for custom lobbies list from master
                // Select a game and join
                break;
            case 3:
                // Start server_game.exe bound to 127.0.0.1
                GameObject go = new GameObject("Network_Localhost", typeof(Network_Localhost));
                var local = go.GetComponent<Network_Localhost>();
                local.Launch_Localhost();
                break;
        }
    }
}