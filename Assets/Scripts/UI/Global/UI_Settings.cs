﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.EventSystems;

enum MenuTabs
{
    General,
    Controls,
    Graphics
}
public class UI_Settings : MonoBehaviour {
    static MenuTabs CurrentTab = MenuTabs.General;

    public GameObject Current_Tab;
    Button ActiveButton = null;

    //Sart
    void Start() {
        ChangeTab((int)MenuTabs.General);
    }
    //remove listeners
    void OnDestroy() {
        foreach (Button button in Current_Tab.GetComponentsInChildren<Button>())
            button.onClick.RemoveListener(() => Listener(button));
    }
    //INSTANCIATE THE TAB THAT WAS SELECTED
    public void ChangeTab(int Tab) {
        if (Current_Tab != null) {
            if (CurrentTab == (MenuTabs)Tab)
                return;
            Destroy(Current_Tab);
        }

        switch ((MenuTabs)Tab)
        {
            case MenuTabs.Controls:
                Current_Tab = Instantiate(Manifold.Prefab_Tab_Controls, transform);
                CurrentTab = MenuTabs.Controls;
                break;
            case MenuTabs.Graphics:
                Current_Tab = Instantiate(Manifold.Prefab_Tab_Graphics, transform);
                CurrentTab = MenuTabs.Graphics;
                break;
            default:
                Current_Tab = Instantiate(Manifold.Prefab_Tab_General, transform);
                CurrentTab = MenuTabs.General;
                break;
        }

        Button[] keys = Current_Tab.GetComponentsInChildren<Button>();
        foreach (Button button in keys)
            AttachListner(button);

        if(CurrentTab == MenuTabs.General) {
            Slider slider = Current_Tab.GetComponentInChildren<Slider>();
            slider.onValueChanged.AddListener((float value) => Listener_Slider(value, slider));
            slider.value = Json_Settings.current.Sensitivity;
        }
    }

    //ATTACH LISTNERS
    void AttachListner(Button button)
    {
        button.onClick.AddListener(() => Listener(button));


        switch (CurrentTab)
        {
            case MenuTabs.General:
                switch (button.gameObject.name) //
                {
                    case "InvertY":
                    case "JumpHold":
                        foreach (Text text in button.GetComponentsInChildren<Text>())
                            if (text.gameObject.name != "Section Label")
                                text.text = Settings_Manager.GetSetting_bool(button.gameObject.name) ? "[ ON ]" : "[ OFF ]";
                        break;
                    case "Disconnect":
                        button.GetComponentInChildren<Text>().text = ((int)State_Manager.state > 15) ? "Leave Match" : "Exit Game";
                        break;
                }

                break;
            case MenuTabs.Controls:
                LabelButton(button);
                break;
            default:

                break;
        }
    }

    void Update()
    {
        if (ActiveButton != null && Input.anyKeyDown)
        {
            KeyCode input = KeyCode.None;
            string keyname = Input.inputString.ToUpper();
            if (keyname == " ")
                input = KeyCode.Space;
            else if (Input.GetKey(KeyCode.Return))
                input = KeyCode.Return;
            else if (keyname != "")
            {
                try
                {
                    int num = int.Parse(keyname);
                    if (num <= 9 && 0 <= num)
                        input = (KeyCode)(num + 48);
                }
                catch
                {
                    input = (KeyCode)Enum.Parse(typeof(KeyCode), keyname.First().ToString().ToUpper());
                }
            }
            else
            {
                if (Input.GetKey(KeyCode.LeftControl))
                    input = KeyCode.LeftControl;
                else if (Input.GetKey(KeyCode.LeftShift))
                    input = KeyCode.LeftShift;
                else if (Input.GetKey(KeyCode.LeftAlt))
                    input = KeyCode.LeftAlt;
                else if (Input.GetKey(KeyCode.Return))
                    input = KeyCode.Return;
                else if (Input.GetKey(KeyCode.Escape))
                    input = KeyCode.Escape;
                else if (Input.GetKey(KeyCode.Tab))
                    input = KeyCode.Tab;
                for (int i = 0; i <= 6; i++)
                    if (Input.GetMouseButton(i) && EventSystem.current.IsPointerOverGameObject())
                    {
                        GameObject selected = EventSystem.current.currentSelectedGameObject;
                        if (selected != null && selected == ActiveButton.gameObject) {
                            if (Input.GetMouseButton(0))
                                Changed = true;
                            input = (KeyCode)(323 + i);
                        }
                        else {
                            AttachListner(ActiveButton);
                            ActiveButton = null;
                            return;
                        }
                    }
            }
            Settings_Manager.SetSetting(ActiveButton.name, (int)input);
            LabelButton(ActiveButton);
            ActiveButton = null;
        }
    }

    //LISTENERS
    public void ToggleSettings()
    {
        State_Manager.Toggle_Settings();
    }
    public bool Changed = false;
    public void Listener(Button button)
    {
        switch (CurrentTab)
        {
            case MenuTabs.General:
                switch (button.gameObject.name) //
                {
                    case "InvertY":
                    case "JumpHold":
                        Settings_Manager.SetSetting(button.name, !Settings_Manager.GetSetting_bool(button.gameObject.name));
                        foreach (Text text in button.GetComponentsInChildren<Text>())
                            if (text.gameObject.name != "Section Label")
                                text.text = Settings_Manager.GetSetting_bool(button.gameObject.name) ? "[ ON ]" : "[ OFF ]" ;
                        break;
                    case "Disconnect":
                        if ((int)State_Manager.state > 15)
                        {
                            Network_Localhost.Close_Localhost();
                            Network_Manager.Instance.Connect(null, 0);
                            State_Manager.Load_Menu();
                        }
                        else if ((int)State_Manager.state < 16)
                        {
#if UNITY_EDITOR
                            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
                        }
                        break;
                }
                break;
            case MenuTabs.Controls:
                if (!Changed)
                {
                    button.GetComponentInChildren<Text>().text = "_";
                    ActiveButton = button;
                }
                else
                    Changed = !Changed;
                break;
            case MenuTabs.Graphics:
                break;
        }
    }
    public void Listener_Slider(float value, Slider slider)
    {
        foreach(Text text in slider.transform.parent.GetComponentsInChildren<Text>()) {
            if (text.gameObject.name == "Text") text.text = "" + string.Format("{0:0.0}", value);
        }
        Settings_Manager.SetSetting("Sensitivity", (int)value);

    }

    //CHANGES NAMES TO SHORTER VERSIONS
    void LabelButton(Button button)
    {
        string label = Settings_Manager.GetSetting_int(button.gameObject.name);
        int code = int.Parse(label);
        label = ((KeyCode)code).ToString();

        button.GetComponent<Image>().color = new Color(0.9f, 0.9f, 0.9f, 1f);
        button.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 70);

        if (code >= 49 && code <= 57)
            label = "" + (code - 48).ToString();
        else
            switch ((KeyCode)code)
            {
                case KeyCode.None:
                    label = " ";
                    button.GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f, 1f);
                    break;
                case KeyCode.Mouse0:
                    label = "M1";
                    break;
                case KeyCode.Mouse1:
                    label = "M2";
                    break;
                case KeyCode.Mouse2:
                    label = "M3";
                    break;
                case KeyCode.Return:
                    label = "⏎";
                    //button.GetComponentInChildren<Text>().fontStyle = FontStyle.Bold;
                    break;
                case KeyCode.LeftControl:
                    label = "CTRL";
                    button.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 90);
                    break;
                case KeyCode.LeftAlt:
                    label = "ALT";
                    button.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 90);
                    break;
                case KeyCode.LeftShift:
                    label = "Shift";
                    button.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 90);
                    break;
                case KeyCode.Space:
                    label = "▃▃";
                    button.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 90);
                    break;
                case KeyCode.Escape:
                    label = "ESC";
                    button.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 90);
                    break;
                case (KeyCode)1:
                    label = "SD";
                    break;
                case (KeyCode)2:
                    label = "SU";
                    break;
            }
        button.GetComponentInChildren<Text>().text = label;
    }


}
