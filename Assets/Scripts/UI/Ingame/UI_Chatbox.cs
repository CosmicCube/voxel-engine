﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Chatbox : MonoBehaviour {

	public void SendChat()
    {
        InputField t = GetComponentInChildren<InputField>();
        if (t.text == "") return;
        if (Network_Manager.status == NetStatus.InGame)
            Network_Manager.Instance.SendPacket(t.text);
        else
            UI_Chathistory.PostChat(t.text);
        t.text = "";
    }
    public void ChatHistory()
    {
        if(UI_Chathistory.open)
            UI_Chathistory.CloseHistory();
        else
            UI_Chathistory.DisplayHistory();
    }
}
