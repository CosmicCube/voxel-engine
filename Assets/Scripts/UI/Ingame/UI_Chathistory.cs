﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Chathistory : MonoBehaviour
{
    static List<chats> history = new List<chats>();
    static int active = 0;
    public static bool open = false;

    void FixedUpdate()
    {
        if (active > 0)
        {
            foreach (chats msg in history)
                if (msg.life >= 0)
                {
                    if (msg.life == 0 && msg.gameObject != null)
                    {
                        if (open)
                            msg.life = -1;
                        else
                            Destroy(msg.gameObject);
                        active--;
                    }
                    msg.life--;
                }
        }
    }
    public static void DisplayHistory()
    {
        if (open) return;
        open = true;
        foreach (chats msg in history)
            if (msg.gameObject == null)
                msg.Init(-2);
            else
                msg.gameObject.transform.SetAsLastSibling();
    }
    public static void CloseHistory()
    {
        if (!open) return;
        open = false;
        foreach (chats msg in history)
            if (msg.life == -2)
            {
                msg.life = -1;
                Destroy(msg.gameObject);
            }
    }
    public static void PostChat(string chat)
    {
        Menu_Manager.CreateChatHistory();
        if (chat == null || chat == "")
            return;
        history.Add(new chats(chat).Init());
        active++;
    }

    private class chats
    {
        public GameObject gameObject;
        public int life;
        public string text;
        public chats(string t)
        {
            text = t;
        }
        public chats Init(int l = 700)
        {
            life = l;
            gameObject = new GameObject("Chat", typeof(Text), typeof(ContentSizeFitter));
            gameObject.transform.SetParent(Menu_Manager.Chathistory.transform.GetChild(0));
            gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(400, 400);

            Text t = gameObject.GetComponent<Text>();
            t.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
            t.fontSize = 24;
            t.color = Color.black;
            t.text = text;

            gameObject.GetComponent<ContentSizeFitter>().verticalFit = ContentSizeFitter.FitMode.PreferredSize;
            return this;
        }
    }
}
