﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class UI_Mapselect : MonoBehaviour
{

    static GameObject Prefab_Entry;
    public static string map = "Beach Bash";
    void Start()
    {
        if (Prefab_Entry == null)
            Prefab_Entry = Manifold.LoadPrefab("UI/Menu/Mapselect_Entry");
        string path = Directory.GetCurrentDirectory() + @"\Unmanaged\Maps";


        foreach (string file in Directory.GetFileSystemEntries(path))
        {
            if (file.EndsWith(".bytes"))
            {
                GameObject entry = Instantiate(Prefab_Entry, transform);
                string name = file.Substring(path.Length + 1).Replace(".bytes", "");
                entry.GetComponentInChildren<Text>().text = name;
                if (File.Exists(path + @"\Icons\" + name + ".png"))
                {
                    Texture2D tex = new Texture2D(1, 1);
                    tex.LoadImage(File.ReadAllBytes(path + @"\Icons\" + name + ".png"));
                    entry.GetComponent<Image>().sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
                }
                if (name == map)
                    entry.GetComponent<Button>().interactable = false;
            }
        }
    }

    public void SelectMap(Button button)
    {
        // foreach (Button b in button.transform.parent.GetComponentsInChildren<Button>())
        //     b.interactable = true;
        // button.interactable = false;
        map = button.GetComponentInChildren<Text>().text;
        State_Manager.state = State.M_Customs;
        Menu_Manager.UpdateDisplay();
    }

}
