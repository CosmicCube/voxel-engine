﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;

public class UI_Serverlist : MonoBehaviour
{
    public static Json_ServerList serverList = new Json_ServerList();
    public static List<GameObject> Entries = new List<GameObject>();
    public static UI_Serverlist_Entry selected;
    static GameObject PrefabEntry;
    static GameObject Prefab_Entry
    {
        get
        {
            if (PrefabEntry == null)
                PrefabEntry = Manifold.LoadPrefab("UI/Menu/Serverlist_Entry");
            return PrefabEntry;
        }
    }
    public static UI_Serverlist self;

    void Start()
    {
        self = this;
        LoadServerList();
        UpdateServers();
    }
    public static void LoadServerList()
    {
        string json;
        if (File.Exists(Application.persistentDataPath + "/ServerList.json"))
        {
            json = File.ReadAllText(Application.persistentDataPath + "/ServerList.json");
            serverList = JsonUtility.FromJson<Json_ServerList>(json);
            if (serverList.version < 2)
            {
                serverList = new Json_ServerList();
                json = JsonUtility.ToJson(serverList);
                File.WriteAllText(Application.persistentDataPath + "/ServerList.json", json);
            }
        }
        else
        {
            serverList = new Json_ServerList();
            json = JsonUtility.ToJson(serverList);
            File.WriteAllText(Application.persistentDataPath + "/ServerList.json", json);
        }
    }
    public void UpdateServers()
    {
        foreach (Json_Server s in serverList.servers)
            Instantiate(Prefab_Entry, transform).GetComponent<UI_Serverlist_Entry>().Init(s);
    }
    public static void QuickConnectServer(Json_Server s, bool quick)
    {
        Transform p = self != null ? self.transform : Menu_Manager.canvas;
        UI_Serverlist_Entry sle = Instantiate(Prefab_Entry, p).GetComponent<UI_Serverlist_Entry>();
        sle.QuickConnect = quick;
        sle.Init(s);
    }
    public static void ClearEntries()
    {
        foreach (GameObject g in Entries)
            Destroy(g);
        Entries = new List<GameObject>();
    }
}
