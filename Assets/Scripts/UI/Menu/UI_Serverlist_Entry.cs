﻿using System;
using System.Collections;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.UI;

public class UI_Serverlist_Entry : MonoBehaviour
{
    public string Name;
    public string Domain;
    public IPAddress Ip;
    public int Port = 0;
    public string Region;
    public string Host;
    public string Desc;
    public bool Selected;
    public bool QuickConnect;
    public bool Pinged;
    Ping ping;
    Ping p2;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(SelectServer);
    }
    public IEnumerator PingIP()
    {
        string ip = Ip.ToString();
        try
        {
            ping = new Ping(ip);
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
        }

        while (!ping.isDone)
            yield return null;

        yield return new WaitForSeconds(1);

        PingCompleted(ping);
    }
    private void PingCompleted(Ping p)
    {
        gameObject.transform.Find("Ping").GetComponent<Text>().text = "" + p.time + "ms";
        Pinged = true;
        setColor();
        if (QuickConnect)
        {
            State_Manager.Load_Game();
            Network_Manager.Instance.Connect(Ip, Port);
        }
        else
            StartCoroutine(PingIP());
    }

    public void Init(Json_Server s, Color NameCol = new Color(), Color DescCol = new Color())
    {
        Name = s.name == "" ? s.map : s.name;
        Domain = s.url;
        Port = s.port;
        Region = s.region;
        Host = s.host;
        Desc = s.desc;

        //attempt to look up domain name if one is provided
        try
        {
            if (s.ip == "")
            {
                if (Domain != "")
                    Ip = Dns.GetHostAddresses(Domain)[0];
                else if (Host != "Error") return;
            }
            else
                Ip = IPAddress.Parse(s.ip);
        }
        catch (Exception ex)
        {
            Debug.Log(Name + " " + Ip + " " + ex);
        }

        StartCoroutine(PingIP());
        string path = Directory.GetCurrentDirectory() + $@"\Unmanaged\Maps\Icons\{s.map}.png";
        foreach (Image image in GetComponentsInChildren<Image>())
        {
            Debug.Log($"{image.name} is not it {path}");
            if (image.name == "Map Icon" && File.Exists(path))
            {
                Texture2D tex = new Texture2D(1, 1);
                tex.LoadImage(File.ReadAllBytes(path));
                image.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector3.zero);
            }
        }
        foreach (Text text in GetComponentsInChildren<Text>())
        {
            switch (text.name)
            {
                case "Name":
                    text.text = Name;
                    if (NameCol != new Color()) text.color = NameCol;
                    break;
                case "Desc":
                    if (!string.IsNullOrEmpty(Desc))
                        text.text = Desc;
                    else
                        text.text = "No description.";

                    break;
                case "Region":
                    if (!string.IsNullOrEmpty(Region))
                        text.text = Region;
                    else
                        text.text = "<REG>";

                    break;
                case "Address":
                    text.text = ((Domain != "") ? Domain : s.ip) + " : " + Port;
                    if (DescCol != new Color()) text.color = DescCol;
                    break;
            }
        }
        setColor();
        UI_Serverlist.Entries.Add(gameObject);
    }
    public void setColor()
    {
        Image img = gameObject.GetComponent<Image>();
        if (QuickConnect)
            img.color = new Color(0.65f, 0.95f, 0.7f);
        else if (Pinged)
            img.color = new Color(1, 0.95f, 0.85f);
        else if (Selected)
            img.color = new Color(0.45f, 0.85f, 1);
        else
            img.color = new Color(0.79f, 0.81f, 0.81f);
    }
    public void SelectServer()
    {
        if (Port > 0 && Ip != null)
        {
            if (UI_Serverlist.selected != null)
            {
                UI_Serverlist.selected.Selected = false;
                UI_Serverlist.selected.setColor();
            }
            UI_Serverlist.selected = this;
            Selected = true; setColor();
            Menu_Manager.Sidebar.GetComponent<UI_Sidebar>().GetButton("Button_1").interactable = true;
        }
    }
}