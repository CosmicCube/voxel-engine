﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UI_Sidebar : MonoBehaviour
{
    Color defualt_color = new Color(0.3f, 0.3f, 0.4f, 1);
    void Start()
    {
        foreach (Button button in GetComponentsInChildren<Button>())
            button.onClick.AddListener(() => Listener(button));
    }

    void SetComponents(Button b, Color c, string t = "", bool i = true)
    {
        b.GetComponentInChildren<Text>().text = t;
        b.interactable = i;
        b.GetComponent<Image>().color = c;
    }
    void SetComponents(Button b, string t = "", bool i = true)
    {
        b.GetComponentInChildren<Text>().text = t;
        b.interactable = i;
        b.GetComponent<Image>().color = new Color(0.3f, 0.3f, 0.36f, 1);
    }

    public Button GetButton(string name)
    {
        foreach (Button button in GetComponentsInChildren<Button>())
            if (button.name == name)
                return button;
        return null;
    }

    public void UpdateButtons()
    {

        switch (State_Manager.state)
        {
            case State.M_Main:
                foreach (Button button in GetComponentsInChildren<Button>())
                {
                    switch (button.name)
                    {
                        case "Button_0":
                            SetComponents(button, new Color(0.1f, 0.8f, 0.1f), "Quick Play");
                            break;
                        case "Button_1":
                            SetComponents(button, "Custom Game");
                            break;
                        case "Button_2":
                            SetComponents(button, new Color(0.4f, 0.3f, 0.3f), "Profile", false);
                            break;
                        case "Button_3":
                            SetComponents(button, new Color(0.4f, 0.3f, 0.3f), "Shop", false);
                            break;
                        case "Button_4":
                            SetComponents(button, new Color(0.4f, 0.3f, 0.3f), "File");
                            //SetComponents(button, new Color(0, 0, 0, 0));
                            break;
                    }
                }
                break;
            case State.M_Customs:
                foreach (Button button in GetComponentsInChildren<Button>())
                {
                    switch (button.name)
                    {
                        case "Button_0":
                            SetComponents(button, new Color(0.1f, 0.8f, 0.1f), "Return to Menu");
                            break;
                        case "Button_1":
                            SetComponents(button, "Join Game", UI_Serverlist.selected != null);
                            break;
                        case "Button_2":
                            SetComponents(button, "Play Offline");
                            break;
                        case "Button_3":
                            SetComponents(button, "Select Map");
                            break;
                        case "Button_4":
                            if (Network_Manager.status == NetStatus.Master)
                                SetComponents(button, "Refresh");
                            else
                                SetComponents(button, new Color(0.4f, 0.3f, 0.3f), "No Connection");
                            break;
                    }
                }
                break;
            case State.M_Map:
                foreach (Button button in GetComponentsInChildren<Button>())
                {
                    switch (button.name)
                    {
                        case "Button_0":
                            SetComponents(button, new Color(0.1f, 0.8f, 0.1f), "Return to Menu");
                            break;
                        case "Button_1":
                            SetComponents(button, "Custom Game");
                            break;
                        case "Button_2":
                            SetComponents(button, "Play Offline", false);
                            break;
                        case "Button_3":
                        case "Button_4":
                            SetComponents(button, new Color(0, 0, 0, 0));
                            break;
                    }
                }
                break;
        }
    }
    void Listener(Button button)
    {
        switch (button.name)
        {
            case "Button_0":
                if (State_Manager.state == State.M_Main)
                {
                    new Network_Localhost().Launch_Localhost();
                    UI_Serverlist.QuickConnectServer(new Json_Server("127.0.0.1", 9836, UI_Mapselect.map), true);
                }
                else
                {
                    State_Manager.state = State.M_Main;
                    Menu_Manager.UpdateDisplay();
                }
                break;
            case "Button_1":
                if (State_Manager.state != State.M_Customs)
                    State_Manager.state = State.M_Customs;
                else
                {
                    State_Manager.Load_Game();
                    UI_Serverlist_Entry s = UI_Serverlist.selected;
                    Network_Manager.Instance.Connect(s.Ip, s.Port);
                }
                Menu_Manager.UpdateDisplay();
                break;
            case "Button_2":
                if (State_Manager.state == State.M_Main)
                {

                }
                else
                {
                    SetComponents(button, new Color(0.1f, 0.8f, 0.1f), "Loading", false);
                    new Network_Localhost().Launch_Localhost();
                    Json_Server local = new Json_Server("", "Play offline", "Local", "127.0.0.1", 9836, UI_Mapselect.map);
                    UI_Serverlist.QuickConnectServer(local, !Debug.isDebugBuild);
                }
                break;
            case "Button_3":
                if (State_Manager.state == State.M_Main)
                {

                }
                else if (State_Manager.state == State.M_Customs)
                {
                    State_Manager.state = State.M_Map;
                    Menu_Manager.UpdateDisplay();
                }
                break;
            case "Button_4":
                if (State_Manager.state == State.M_Main)
                    Application.OpenURL(Application.persistentDataPath);
                else if (State_Manager.state == State.M_Customs)
                {
                    if (Network_Manager.status == NetStatus.Master)
                    {
                        SetComponents(button, "Downloading", false);
                        Network_Manager.Instance.SendPacket("", (int)Commands.RequestServers);
                    }
                    else
                    {
                        try
                        {
                            Network_Manager.Instance.Connect(System.Net.Dns.GetHostAddresses("voxel.prosnap.co")[0], 9825);
                            Network_Manager.connection.ConnectFailed += new Connection.OnConnectFailedHandler((Connection sender, string ex) => Client_ConnectFailed(sender, ex, button));
                            SetComponents(button, "Connecting", false);
                        }
                        catch (Exception ex)
                        {
                            SetComponents(button, new Color(0.8f, 0.1f, 0.1f), "ERROR" + ex.Data, false);
                        }
                    }
                }
                break;
        }
    }
    void Client_ConnectFailed(Connection sender, string ex, Button button)
    {
        SetComponents(button, new Color(0.4f, 0.3f, 0.3f), "Connection Failed", true);
    }

    public void register()
    {
        //if (Network_Manager.netInstance != null && user != null && pass != null)
        //{
        //    string reg = user.GetComponent<Text>().text + " :: " + pass.GetComponent<Text>().text;
        //    Network_Manager.netInstance.SendPacket(reg + " :: e@t.c",(int)Commands.Register);
        //}
    }
}
