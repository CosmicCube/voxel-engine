﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Import_bnl_bin : MonoBehaviour {
    string jsonMapData;
    public string map = "map.json";

    void Start () {
        jsonMapData = LoadJsonMapFile(map);
        var jsonMap = JsonUtility.FromJson<RootObject>(jsonMapData);
        debug(jsonMap);
    }
    void debug(RootObject jsonMap){
        Debug.Log("Name " + jsonMap.name + " : Desc " + jsonMap.description);
        Debug.Log("X " + jsonMap.map.size.x + " : Y " + jsonMap.map.size.y + " : Z " + jsonMap.map.size.z);
        Debug.Log("plane " + jsonMap.map.properties.plane_position + " : kill " + jsonMap.map.properties.kill_position);

        for (int i = 0; i <= 1; i++)
        {
            Debug.Log("r " + jsonMap.map.color_palette[i].r + " : g " + jsonMap.map.color_palette[i].g + " : b " + jsonMap.map.color_palette[i].b + " : a " + jsonMap.map.color_palette[i].a);
        }
        Debug.Log(jsonMap.map.blocks_data);
    }



    public static string LoadJsonMapFile(string path)
    {
        string filePath = path.Replace(".json","");
        TextAsset targetFile = Resources.Load(filePath) as TextAsset;
        Debug.Log(targetFile.text);
        return targetFile.text;
    }
    [Serializable]
    public class Map
    {
        public int version;
        public int schema;
        public string match;
        public List<ColorPalette> color_palette;
        public List<SpawnPoint> spawn_points;
        public List<Unit> units;
        public List<object> cameras;
        public List<object> triggers;
        public Properties properties;
        public Size size;
        public string blocks_data;
        public string colors_data;
    }
    [Serializable]
    public class RootObject
    {
        public string name;
        public string description;
        public bool default_image;
        public int publish_id;
        public bool is_published;
        public Map map;
    }
    [Serializable]
    public class ColorPalette
    {
        public int r;
        public int g;
        public int b;
        public int a;
    }
    [Serializable]
    public class Position
    {
        public double x;
        public int y;
        public double z;
    }
    [Serializable]
    public class SpawnPoint
    {
        public string team;
        public Position position;
        public string direction;
        public string label;
    }
    [Serializable]
    public class Position2
    {
        public double x;
        public double y;
        public double z;
    }
    [Serializable]
    public class Rotation
    {
        public int x;
        public int y;
        public int z;
    }
    [Serializable]
    public class Unit
    {
        public Position2 position;
        public Rotation rotation;
        public string unit_key;
        public string team;
    }
    [Serializable]
    public class Properties
    {
        public string audio_ambience;
        public string render;
        public string plane;
        public double plane_position;
        public double kill_position;
        public double barrier_1_team_1;
        public double barrier_1_team_2;
        public double barrier_2_team_1;
        public double barrier_2_team_2;
        public int min_fall_height;
        public int max_fall_height;
    }
    [Serializable]
    public class Size
    {
        public int x;
        public int y;
        public int z;
    }
}
