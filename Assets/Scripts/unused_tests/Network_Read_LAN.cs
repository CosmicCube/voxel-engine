﻿using System;
using System.Net.NetworkInformation;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;

class Network_Read_LAN
{
    static int upCount = 0;
    static object lockObj = new object();
    const bool resolveNames = false;
    static Thread ping;

    public static void Main(int startIp, int endIp, string ipBase = "192.168.1.")
    {

        PingStart(startIp, endIp, ipBase);

    }
    static void PingStart(int startIp, int endIp, string ipBase = "192.168.1.")
    {
        Stopwatch sw = new Stopwatch();
        sw.Start();
        for (int i = startIp; i <= endIp; i++)
        {
            string ip = ipBase + i.ToString();

            Ping p = new Ping();
            p.PingCompleted += new PingCompletedEventHandler(PingCompleted);
            UnityEngine.Debug.Log(ip);
            p.SendAsync(ip, 100, ip);
        }
        sw.Stop();
        //TimeSpan span = new TimeSpan(sw.ElapsedTicks);
        UnityEngine.Debug.Log(string.Format("Took {0} milliseconds. {1} hosts active.", sw.ElapsedMilliseconds, upCount));
        Console.ReadLine();
    }

    static void PingCompleted(object sender, PingCompletedEventArgs e)
    {
        UnityEngine.Debug.Log(string.Format("callback thrown"));
        string ip = (string)e.UserState;
        if (e.Reply != null && e.Reply.Status == IPStatus.Success)
        {
            TaskExecutorScript.que.ScheduleTask(new Task(delegate
            {
                UnityEngine.Debug.Log(string.Format("{0} is up: ({1} ms)", ip, e.Reply.RoundtripTime));
            }));

            lock (lockObj)
            {
                upCount++;
            }
        }
        else if (e.Reply == null)
        {
            TaskExecutorScript.que.ScheduleTask(new Task(delegate {
                UnityEngine.Debug.Log(string.Format("Pinging {0} failed. (Null Reply object?)", ip));
            }));
        }
        ping.Abort();
    }
}


