﻿using System;
using System.IO;
using Protocol;
using System.Collections.Generic;
using Json;
using System.Runtime.Serialization.Formatters.Binary;

namespace MapConverter
{
    internal sealed class Program
    {
        private struct BlockInfo
        {
            public byte newID;
            public byte structure;

            public BlockInfo(byte newID, byte structure)
            {
                this.newID     = newID;
                this.structure = structure;
            }
        }
        private static Dictionary<int, BlockInfo> idMap;

        static void Main(string[] args)
        {
            InitIDMap();

            // Enter console mode
            if (args.Length == 0)
            {
                //string command;

                do
                {
                    // convert <file> <out>, convert all [dir] [dir out]
                } while (true);
            }
            // Drag and drop.
            else
            {
                ReadConvertProcessMap(args[0]);
            }
        }

        //map of BNL ID's and conversion returns
        static void InitIDMap()
        {
            idMap = new Dictionary<int, BlockInfo>();
            idMap[0] = new BlockInfo(0, 0);     //air
            idMap[1] = new BlockInfo(4, 1);     //dirt
            idMap[2] = new BlockInfo(7, 1);     //sand
            idMap[3] = new BlockInfo(14, 1);    //stone
            idMap[4] = new BlockInfo(0, 0);     //lava ---
            idMap[5] = new BlockInfo(3, 1);     //turf
            idMap[6] = new BlockInfo(0, 0);     //tallgrass ---
            idMap[7] = new BlockInfo(30, 0);    //bricks
            idMap[8] = new BlockInfo(22, 0);    //planks
            idMap[9] = new BlockInfo(18, 1);    //cement
            idMap[10] = new BlockInfo(97, 0);   //metal
            idMap[11] = new BlockInfo(20, 0);   //logs
            idMap[12] = new BlockInfo(21, 1);   //leaves 
            idMap[16] = new BlockInfo(43, 0);   //speedpads
            idMap[17] = new BlockInfo(31, 0);   //crates
            idMap[18] = new BlockInfo(44, 0);   //bouncepads
            idMap[21] = new BlockInfo(33, 0);   //sandbagz
            idMap[27] = new BlockInfo(15, 1);   //tiled-stone
            idMap[29] = new BlockInfo(40, 0);   //glue
            idMap[31] = new BlockInfo(41, 0);   //spikes
            idMap[33] = new BlockInfo(34, 0);   //Forcegates
            idMap[35] = new BlockInfo(15, 2);   //stone POST
            idMap[37] = new BlockInfo(42, 0);   //beartraps
            idMap[38] = new BlockInfo(16, 2);   //brick-stone POST
            idMap[41] = new BlockInfo(22, 2);   //wood POST
            idMap[44] = new BlockInfo(98, 0);   //IFgate
            idMap[45] = new BlockInfo(2, 0);    //grass -> MTurf
            idMap[46] = new BlockInfo(12, 1);   //granite
            idMap[51] = new BlockInfo(2, 0);    //bush -> MTurf
            idMap[53] = new BlockInfo(17, 1);   //cementwhite -> clay
            idMap[58] = new BlockInfo(31, 0);   //boxs -> crates
            idMap[59] = new BlockInfo(99, 0);   //no-build
            idMap[60] = new BlockInfo(9, 0);    //snow
            idMap[61] = new BlockInfo(5, 0);    //ice
        }

        //where all the conversion is done
        static void ReadConvertProcessMap(string file)
        {
            // Load BnL map data
            JsonData jsonData = JsonParser.Parse(MapStoreUtils.Decode(File.ReadAllBytes(file)));
            MapCustomData mcd = new MapCustomData();
            mcd.FromJsonData(jsonData);

            using (BinaryWriter bw       = new BinaryWriter(File.Create(mcd.Name + ".bytes")))
            using (BinaryReader mcID     = new BinaryReader(ZLibHelper.UnZip(mcd.Map.BlocksData)))
            using (BinaryReader mcColour = new BinaryReader(ZLibHelper.UnZip(mcd.Map.ColorsData)))
            {
                bw.Write(mcd.Name);
                bw.Write(mcd.Map.Properties.KillPosition);
                bw.Write(mcd.Map.Properties.PlanePosition);

                bw.Write(mcd.Map.Size.x);
                bw.Write(mcd.Map.Size.y);
                bw.Write(mcd.Map.Size.z);

                // So we can write all data out at once
                byte[] blockData = new byte[mcd.Map.Size.x *
                                            mcd.Map.Size.y *
                                            mcd.Map.Size.z * 4];

                for (int i = 0; i < blockData.Length; i += 4)
                {
                    byte blockID     = mcID.ReadByte();
                    byte blockDmg    = mcID.ReadByte();
                    byte blockForm   = mcID.ReadByte();
                    byte blockTint   = (byte)(mcID.ReadByte() * 2); // apply team as color.

                    byte Colour      = mcColour.ReadByte(); 
                    if (blockTint < 1) blockTint = Colour; // if the block has no team then try applying a color

                    // Change ID to match my convention, if no match leave as is.
                    if (idMap.ContainsKey(blockID))
                    {
                        byte structure = idMap[blockID].structure;
                        if (blockForm > 0 && structure < 2) // If the block is beveled correct the bevel
                        {
                            blockDmg = 1;
                            blockForm = CorrectBevel(blockForm);
                        }
                        else if (structure == 2) // Post
                        {
                            blockDmg = 2;
                            blockForm = 140;
                        }

                        blockID = idMap[blockID].newID;
                    }

                    blockData[i + 0] = blockID;
                    blockData[i + 1] = blockDmg;
                    blockData[i + 2] = blockForm;
                    blockData[i + 3] = blockTint;
                }

                bw.Write(blockData);
            }
        }

        //method to correct the bevel of blocks
        static byte CorrectBevel(byte form)
        {
            int p = 0; int by = 255 - form; bool[] crn = new bool[8], crn2 = new bool[8];
            for (int i = 128; i >= 1; i = i / 2)
            {
                if (by >= i) { crn[p] = true; by = by - i; }
                else crn[p] = false;
                p++;
            }
            crn2[0] = crn[3];
            crn2[1] = crn[0];
            crn2[2] = crn[5];
            crn2[3] = crn[2];
            crn2[4] = crn[6];
            crn2[5] = crn[1];
            crn2[6] = crn[7];
            crn2[7] = crn[4];
            by = 0; p = 0;
            for (int i = 128; i >= 1; i = i / 2)
            {
                if (crn2[p]) by += (byte)i;
                p++;
            }
            return (byte)by;
        }
    }
}
