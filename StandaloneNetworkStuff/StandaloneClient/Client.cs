﻿using System.Net.Sockets;
using System;
using System.IO;

namespace StandaloneClient
{
    enum Commands : int
    {
        Login = 0,
        Register = 1,
        RequestServers = 2,
        RequestProfile = 3,
        InvitePlayer = 4,

        ChatMsg = 9,
        Type = 10,

        LoginReturn = 11,
        ServerList = 12,
        ProfileInfo = 13,
        InviteAccept = 14,
        InviteDecline = 15,
    }
    enum Packets : int
    {
        Location = 0,
        BlockPlacement = 2,
        BlockDamage = 3,

        ChatMsg = 9,
        Type = 10,
    }
    //STRUCT TO STORE DATA
    struct ReceiveBuffer
    {
        public const int BUFFER_SIZE = 1024;
        public byte[] Buffer;
        public int ToReceive;
        public MemoryStream BufStream;

        public ReceiveBuffer(int toRec)
        {
            Buffer = new byte[BUFFER_SIZE];
            ToReceive = toRec;
            BufStream = new MemoryStream(toRec);
        }
        public void Dispose()
        {
            Buffer = null;
            ToReceive = 0;
            if (BufStream != null)
                BufStream.Dispose();
        }

    }
    internal class Client
    {

        ReceiveBuffer buffer;
        byte[] lenBuffer;
        Socket skt;
        Socket socket()
        {
            return new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }
        public bool Connected
        {
            get
            {
                if (skt != null)
                    return skt.Connected;
                return false;
            }
        }

        //MAIN ENTRY
        public Client()
        {
            lenBuffer = new byte[4];
            skt = socket();
        }

        //CONNECT TO SERVER
        public void Connect(string ip, int port)
        {
            if (skt == null)
                skt = socket();
            skt.BeginConnect(ip, port, connectCallback, null);
        }
        //CONNECTION ESTABLISHED
        void connectCallback(IAsyncResult ar)
        {
            try
            {
                skt.EndConnect(ar);
                if (skt.Connected)
                    OnConnect(this, Connected);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        //SEND DATA
        public void Send(byte[] data, int index, int length)
        {
            skt.BeginSend(BitConverter.GetBytes(length), 0, 4, SocketFlags.None, sendCallback, null);
            skt.BeginSend(data, index, length, SocketFlags.None, sendCallback, null);
        }
        //COMPLETE DATA SENDING
        void sendCallback(IAsyncResult ar)
        {
            try
            {
                int sent = skt.EndSend(ar);
                if (OnSend != null)
                    OnSend(this, sent);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        //AWAIT DATA RECIEVED
        public void receive()
        {
            skt.BeginReceive(lenBuffer, 0, lenBuffer.Length, SocketFlags.None, receiveCallback, null);
        }
        //MARKER PACKET WAS RECIEVED
        void receiveCallback(IAsyncResult ar)
        {
            try
            {
                int rec = skt.EndReceive(ar);
                if (rec != 4)
                {
                    if (rec == 0 && OnDisconnect != null)
                    {
                        OnDisconnect(this);
                        return;
                    }
                    else
                        throw new Exception();
                }
            }
            catch (SocketException ex)
            {
                switch (ex.SocketErrorCode)
                {
                    case SocketError.ConnectionAborted:
                    case SocketError.ConnectionReset:
                        if (OnDisconnect != null) { OnDisconnect(this); return; }
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            buffer = new ReceiveBuffer(BitConverter.ToInt32(lenBuffer, 0));
            skt.BeginReceive(buffer.Buffer, 0, buffer.Buffer.Length, SocketFlags.None, receivePacketCallback, null);
        }
        //DATA PACKET WAS RECIEVED
        void receivePacketCallback(IAsyncResult ar)
        {
            int rec = skt.EndReceive(ar);
            if (rec <= 0)
                return;
            buffer.BufStream.Write(buffer.Buffer, 0, rec);
            buffer.ToReceive -= rec;

            if (buffer.ToReceive > 0)
            {
                Array.Clear(buffer.Buffer, 0, buffer.Buffer.Length);
                skt.BeginReceive(buffer.Buffer, 0, buffer.Buffer.Length, SocketFlags.None, receivePacketCallback, null);
                return;
            }
            if (DataReceived != null)
            {
                buffer.BufStream.Position = 0;
                DataReceived(this, buffer);
            }
            buffer.Dispose();
            receive();
        }

        //DISCONNECT
        public void Disconnect()
        {
            try
            {
                if (skt.Connected)
                {
                    skt.Close();
                    skt = null;
                    if (OnDisconnect != null)
                        OnDisconnect(this);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public delegate void OnConnectEventHandler(Client sender, bool connected);
        public delegate void OnSendEventHandler(Client sender, int sent);
        public delegate void OnDataReceivedEventHandler(Client sender, ReceiveBuffer e);
        public delegate void OnDisconnectEventHandler(Client sender);
        public event OnConnectEventHandler OnConnect;
        public event OnSendEventHandler OnSend;
        public event OnDataReceivedEventHandler DataReceived;
        public event OnDisconnectEventHandler OnDisconnect;

    }

}