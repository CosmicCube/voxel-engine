﻿using System;
using System.IO;
using System.Net.Sockets;

namespace StandaloneClient
{
    class Program
    {
        Client client;
        string ip = "52.38.41.68";
        int port = 9825;
        
        public static void Main(string[] args)
        {
            new Program().Start();
        }
        void Start()
        {
            ip = Console.ReadLine();
            if (ip == "1")
                ip = "127.0.0.1";
            else if (ip == "2")
                ip = "52.38.41.68";

            client = new Client();
            client.OnConnect += new Client.OnConnectEventHandler(Client_OnConnect);
            client.OnSend += new Client.OnSendEventHandler(Client_OnSend);
            client.OnDisconnect += new Client.OnDisconnectEventHandler(Client_OnDisconnect);
            if (!client.Connected)
                client.Connect(ip, port);
            string log;
            do
            {
                Console.Write("Enter text:");
                log = Console.ReadLine();
                SendText(log);
            }
            while (log != null);
        }

        //CONNECTION HAS BEEN MADE
        private void Client_OnConnect(Client sender, bool connected)
        {
            if (connected)
                Console.WriteLine(DateTime.Now + "> Connection established");
            client.DataReceived += new Client.OnDataReceivedEventHandler(Client_DataReceived);
            client.receive();
        }
        //CALLED WHEN MESSAGE IS SENT
        private void Client_OnSend(Client sender, int sent)
        {
            //Console.WriteLine(string.Format("{0}> Data Sent: {1}", DateTime.Now, sent));
        }
        //RECIEVE REQUESTS
        private void Client_DataReceived(Client sender, ReceiveBuffer e)
        {
            BinaryReader r = new BinaryReader(e.BufStream);
            Packets header = (Packets)r.ReadInt32();
            switch (header)
            {
                case Packets.ChatMsg:
                    string s = r.ReadString();
                    Console.WriteLine("{0}> {1}", DateTime.Now, s);
                    break;
            }
        }
        //CLIENT HAS DC'D
        private void Client_OnDisconnect(Client sender)
        {
            Console.WriteLine("DC'd");
        }

        //PACKET OF TEXT
        void SendText(string text)
        {
            BinaryWriter bw = new BinaryWriter(new MemoryStream());
            bw.Write((int)Packets.ChatMsg);
            bw.Write(text);
            bw.Close();
            byte[] data = ((MemoryStream)bw.BaseStream).ToArray();
            bw.BaseStream.Dispose();
            client.Send(data, 0, data.Length);
            data = null;
        }

        Socket socket()
        {
            return new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }
    }
}

