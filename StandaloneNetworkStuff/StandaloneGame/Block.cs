﻿namespace StandaloneGame
{
    using System;
    
    [Serializable]
    public class Block
    {
        public byte id;
        public byte dmg;
        public byte form;
        public byte tint;

        public Block(byte id, byte dmg = 0, byte form = 0, byte tint = 0)
        {
            this.id = id;
            this.dmg = dmg;
            this.form = form;
            this.tint = tint;
        }
    }
}