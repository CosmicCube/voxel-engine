﻿namespace StandaloneGame
{
    public static class BlockRecords
    {
        public const int ID_AIR = 0;
        public const int ID_DEVICE = 1;
        //grass etc
        public const int ID_MTURF = 2;
        public const int ID_TURF = 3;
        public const int ID_DIRT = 4;
        public const int ID_ICE = 5;
        //soft blocks
        public const int ID_SAND = 7;
        public const int ID_GRAVEL = 8;
        public const int ID_SNOW = 9;
        //hard blocks
        public const int ID_SANDSTONE = 10;
        public const int ID_QUARTZ = 11;
        public const int ID_GRANITE = 12;
        //map stucture blocks
        public const int ID_STONE = 14;
        public const int ID_TILEDSTONE = 15;
        public const int ID_STONEBRICK = 16;
        public const int ID_HARDCLAY = 17;
        public const int ID_CEMENT = 18;
        //organic blocks
        public const int ID_LOGS = 20;
        public const int ID_LEAVES = 21;
        public const int ID_PLANKS = 22;
        //player blocks
        public const int ID_BRICKS = 30;
        public const int ID_CRATES = 31;
        public const int ID_CONSTUCTS = 32;
        public const int ID_SANDBAGS = 33;
        //semi-device blocks
        public const int ID_FORCES = 34;
        public const int ID_FLOATERS = 35;
        //constructs of blocks
        public const int ID_CBRICKS = 36;
        public const int ID_CCRATES = 37;
        public const int ID_CSANDBAGS = 38;
        public const int ID_CFORCES = 39;
        //semi-device tiles
        public const int ID_GLUE = 40;
        public const int ID_SPIKE = 41;
        public const int ID_TRAP = 42;
        public const int ID_BOUNCER = 43;
        public const int ID_BOASTER = 44;
        public const int ID_WHEEL = 45;
        //map components
        public const int ID_METAL = 97;
        public const int ID_IFGATE = 98;
        public const int ID_NOBUILD = 99;
        public class BlockRecord
        {
            public byte sideTexU;
            public byte sideTexV;
            public byte topTexU;
            public byte topTexV;
            public byte botTexU;
            public byte botTexV;
            public byte transparent;
            public byte toolDmg;
            public byte explosiveDmg;
            public byte brickReturns;

            public BlockRecord(byte su, byte sv, byte tu, byte tv, byte bu, byte bv, byte tp, byte td, byte ed, byte br)
            {
                sideTexU = su;
                sideTexV = sv;
                topTexU = tu;
                topTexV = tv;
                botTexU = bu;
                botTexV = bv;
                transparent = tp;
                toolDmg = td;
                explosiveDmg = ed;
                brickReturns = br;
            }
        }

        public class BlockCrafting
        {
            public byte[,] CombinesWith;

            public BlockCrafting(byte[,] cw)
            {
                CombinesWith = cw;
            }
        }

        public class BlockCost
        {
            public int blockCost;
            public bool freezeMovement;
            public byte buildTime;
            public byte placedDelay;
            public bool rotatable;
            public string prefabName;
            public BlockCost(int bc, bool fm, byte bt, byte pd, bool rt, string pn = null)
            {
                blockCost = bc;
                freezeMovement = fm;
                buildTime = bt;
                placedDelay = pd;
                rotatable = rt;
                prefabName = pn;
            }
        }

        private static BlockRecord[] blockRecords = new BlockRecord[256];
        private static BlockCrafting[] craftingList = new BlockCrafting[256];
        private static BlockCost[] costList = new BlockCost[256];

        static BlockRecords()
        {                                                  //uvs             trans 
            blockRecords[ID_MTURF] = new BlockRecord(3, 7, 0, 7, 0, 4, 0, 10, 6, 3);
            blockRecords[ID_TURF] = new BlockRecord(1, 7, 0, 7, 2, 7, 0, 16, 8, 3);
            blockRecords[ID_DIRT] = new BlockRecord(2, 7, 2, 7, 2, 7, 0, 18, 9, 3);
            blockRecords[ID_ICE] = new BlockRecord(7, 5, 7, 5, 7, 5, 0, 10, 10, 3);
            //soft blocks
            blockRecords[ID_SAND] = new BlockRecord(7, 6, 7, 6, 7, 6, 0, 20, 20, 3);
            blockRecords[ID_GRAVEL] = new BlockRecord(6, 6, 6, 6, 6, 6, 0, 15, 5, 3);
            blockRecords[ID_SNOW] = new BlockRecord(7, 0, 7, 0, 7, 0, 0, 25, 25, 3);
            //hard blocks
            blockRecords[ID_SANDSTONE] = new BlockRecord(0, 6, 0, 6, 0, 6, 0, 2, 1, 2);
            blockRecords[ID_QUARTZ] = new BlockRecord(1, 6, 1, 6, 1, 6, 0, 3, 3, 6);
            blockRecords[ID_GRANITE] = new BlockRecord(2, 6, 2, 6, 2, 6, 0, 4, 2, 4);
            //structure blocks
            blockRecords[ID_STONE] = new BlockRecord(0, 4, 0, 4, 0, 4, 0, 9, 5, 3);
            blockRecords[ID_TILEDSTONE] = new BlockRecord(1, 4, 1, 4, 1, 4, 0, 10, 6, 3);
            blockRecords[ID_STONEBRICK] = new BlockRecord(3, 0, 3, 0, 3, 0, 0, 10, 6, 3);
            blockRecords[ID_HARDCLAY] = new BlockRecord(2, 4, 2, 4, 2, 4, 0, 12, 7, 3);
            blockRecords[ID_CEMENT] = new BlockRecord(3, 4, 3, 4, 3, 4, 0, 12, 7, 3);
            //woody blocks
            blockRecords[ID_LOGS] = new BlockRecord(0, 5, 1, 5, 1, 5, 0, 13, 13, 3);
            blockRecords[ID_LEAVES] = new BlockRecord(3, 5, 3, 5, 3, 5, 1, 25, 25, 3);
            blockRecords[ID_PLANKS] = new BlockRecord(2, 5, 2, 5, 2, 5, 0, 14, 15, 3);
            //player blocks
            blockRecords[ID_BRICKS] = new BlockRecord(3, 0, 3, 0, 3, 0, 0, 4, 12, 1);
            blockRecords[ID_CRATES] = new BlockRecord(4, 0, 4, 0, 4, 0, 0, 15, 12, 1);
            blockRecords[ID_CONSTUCTS] = new BlockRecord(5, 0, 5, 0, 5, 0, 1, 25, 25, 15);
            blockRecords[ID_SANDBAGS] = new BlockRecord(0, 0, 1, 0, 2, 0, 0, 9, 2, 4);
            //semi-device blocks
            blockRecords[ID_FORCES] = new BlockRecord(6, 0, 6, 0, 6, 0, 2, 9, 3, 12);
            blockRecords[ID_FLOATERS] = new BlockRecord(7, 0, 7, 0, 7, 0, 1, 5, 1, 75);
            //constructs of blocks
            blockRecords[ID_CBRICKS] = new BlockRecord(3, 1, 3, 1, 3, 1, 0, 5, 10, 2);
            blockRecords[ID_CCRATES] = new BlockRecord(4, 1, 4, 1, 4, 1, 0, 15, 10, 2);
            blockRecords[ID_CSANDBAGS] = new BlockRecord(0, 1, 1, 1, 2, 0, 0, 8, 2, 6);
            blockRecords[ID_CFORCES] = new BlockRecord(6, 1, 6, 1, 6, 1, 2, 9, 3, 15);
            //semi-device tiles
            blockRecords[ID_BOUNCER] = new BlockRecord(0, 0, 0, 0, 0, 0, 1, 5, 10, 2);
            blockRecords[ID_WHEEL] = new BlockRecord(0, 2, 7, 0, 7, 0, 1, 5, 10, 2);
            //Indestructible Blocks
            blockRecords[ID_METAL] = new BlockRecord(0, 2, 0, 2, 0, 2, 0, 0, 0, 3);
            blockRecords[ID_IFGATE] = new BlockRecord(1, 2, 1, 2, 1, 2, 2, 0, 0, 3);
            blockRecords[ID_NOBUILD] = new BlockRecord(8, 8, 1, 2, 1, 2, 2, 0, 0, 3);

            //COST LIST
            costList[ID_BRICKS] = new BlockCost(12, false, 10, 3, false);
            costList[ID_CRATES] = new BlockCost(5, false, 0, 0, false);
            costList[ID_CONSTUCTS] = new BlockCost(15, false, 0, 0, false);
            costList[ID_SANDBAGS] = new BlockCost(20, true, 15, 30, false);
            costList[ID_FORCES] = new BlockCost(40, false, 10, 60, false);
            costList[ID_FLOATERS] = new BlockCost(500, true, 120, 0, false, "Floater");
            //Device Blocks
            costList[ID_GLUE] = new BlockCost(30, false, 0, 2, false);
            costList[ID_SPIKE] = new BlockCost(30, false, 0, 2, true);
            costList[ID_TRAP] = new BlockCost(50, false, 5, 5, false);
            costList[ID_BOUNCER] = new BlockCost(75, false, 4, 15, true, "Bouncer");
            costList[ID_BOASTER] = new BlockCost(75, false, 10, 5, true);

            costList[ID_LEAVES] = new BlockCost(0, false, 0, 0, false);
            costList[ID_METAL] = new BlockCost(59, false, 0, 0, false);

            //CRAFTING RECORDS
            craftingList[ID_BRICKS] = new BlockCrafting(new byte[,] { { 32, 36 } });
            craftingList[ID_CRATES] = new BlockCrafting(new byte[,] { { 32, 37 } });
            craftingList[ID_CONSTUCTS] = new BlockCrafting(new byte[,] { { 30, 36 }, { 31, 37 }, { 33, 38 }, { 34, 39 }, { 44, 45 } });
            craftingList[ID_SANDBAGS] = new BlockCrafting(new byte[,] { { 32, 38 } });
            craftingList[ID_FORCES] = new BlockCrafting(new byte[,] { { 32, 39 } });
            craftingList[ID_BOASTER] = new BlockCrafting(new byte[,] { { 32, 45 } });
        }
        public static BlockRecord GetBlockRecord(byte id)
        {
            return blockRecords[id];
        }
        public static BlockCrafting GetCraftingList(byte id)
        {
            return craftingList[id];
        }
        public static BlockCost GetBlockCost(byte id)
        {
            return costList[id];
        }
    }
}