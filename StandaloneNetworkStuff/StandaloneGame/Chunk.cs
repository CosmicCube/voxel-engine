﻿using System.Collections.Generic;
namespace StandaloneGame
{
    public class Chunk
    {
        public static int CHUNK_SIZE = 16;
        public Block[,,] blocks = new Block[CHUNK_SIZE, CHUNK_SIZE, CHUNK_SIZE];
        public int posX;
        public int posY;
        public int posZ;        
        public World world;

        public Block GetBlock(int x, int y, int z)
        {
            if (x < 0 || x >= CHUNK_SIZE)
                return null;

            if (y < 0 || y >= CHUNK_SIZE)
                return null;

            if (z < 0 || z >= CHUNK_SIZE)
                return null;

            return blocks[x, y, z];
        }

        public void SetBlock(int x, int y, int z, Block block)
        {
            blocks[x, y, z] = block;
        }
        public int maxIndex(List<int> arr)
        {
            int max = 0;
            foreach (int i in arr)
            {
                if (i >= max)
                    max = i;
            }
            return max;
        }
    }
}