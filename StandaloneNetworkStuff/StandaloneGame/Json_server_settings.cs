﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace StandaloneGame
{
    public partial class Json_server_settings
    {
        [JsonProperty("desc")]
        public string Desc { get; set; }

        [JsonProperty("host")]
        public string Host { get; set; }

        [JsonProperty("ip")]
        public string Ip { get; set; }

        [JsonProperty("map")]
        public string Map { get; set; }

        [JsonProperty("mode")]
        public int Mode { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("pass")]
        public string Pass { get; set; }

        [JsonProperty("port")]
        public int Port { get; set; }

        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }

    public partial class Json_server_settings
    {
        public static Json_server_settings FromJson(string json)
        {
            return JsonConvert.DeserializeObject<Json_server_settings>(json, Converter.Settings);
        }
        public Json_server_settings()
        {
            Mode = 0;
            Map = "Beach Bash";
            Ip = "127.0.0.1";
            Port = 49825;
            Name = "Server";
            Desc = "";
            Host = "Localhost";
        }
    }

    public static class Serialize
    {
        public static string ToJson(this Json_server_settings self)
        {
            return JsonConvert.SerializeObject(self, Converter.Settings);
        }
    }

    public class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
