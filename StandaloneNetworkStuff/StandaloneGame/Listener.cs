﻿using System;
using System.Net;
using System.Net.Sockets;

namespace StandaloneGame
{
    internal class Listener
    {
        Socket skt;
        public bool Running { get; private set; }
        public delegate void SocketAcceptedHandler(Socket e);
        public event SocketAcceptedHandler Accepted;
        Socket socket()
        {
            return new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        //LAUNCH LISTENER - AWAIT CONNECTIONS
        public void listen(int port)
        {
            if (Running)
            {
                Console.WriteLine("already running");
                return;
            }
            try
            {
                skt = socket();
                skt.Bind(new IPEndPoint(0, port));
                skt.Listen(0);

                skt.BeginAccept(listenCallback, null);
                Running = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        //CONNECTION RECIEVED CALL BACK
        public void listenCallback(IAsyncResult ar)
        {
            try
            {
                Socket s = skt.EndAccept(ar);
                if (Accepted != null)
                    Accepted(s);
                skt.BeginAccept(listenCallback, null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (Running)
            {
                try
                {
                    skt.BeginAccept(listenCallback, null);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
        //STOP LISTENING
        public void Stop()
        {
            Console.WriteLine("listener stopped");
            if (!Running)
                return;
            skt.Close();
            Running = false;
        }
    }
}