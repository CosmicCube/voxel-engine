﻿using System;
using System.Net;
using System.Net.Sockets;

namespace StandaloneGame
{
    enum Commands : int
    {
        ServerDetails = 30,
    }
    public class MasterServer
    {
        public IPAddress ip;
        Socket skt;
        Socket socket()
        {
            return new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }
        public bool Connected
        {
            get
            {
                if (skt != null)
                    return skt.Connected;
                return false;
            }
        }

        //CONNECT TO SERVER
        internal void Connect()
        {
            try
            {
                ip = Dns.GetHostAddresses("block2.co")[0];
                //ip = IPAddress.Parse("127.0.0.1");
                if (skt == null)
                    skt = socket();
                skt.BeginConnect(ip, 9825, connectCallback, null);


            }
            catch (Exception ex) {}

        }
        //CONNECTION ESTABLISHED
        void connectCallback(IAsyncResult ar)
        {
            try
            {
                skt.EndConnect(ar);
                if (skt.Connected)
                    OnConnect(this, Connected);
            }
            catch { }
        }
        //SEND DATA
        public void Send(byte[] data, int index, int length)
        {
            skt.BeginSend(BitConverter.GetBytes(length), 0, 4, SocketFlags.None, sendCallback, null);
            skt.BeginSend(data, index, length, SocketFlags.None, sendCallback, null);
        }
        //COMPLETE DATA SENDING
        void sendCallback(IAsyncResult ar)
        {
            try
            {
                int sent = skt.EndSend(ar);                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        //DISCONNECT
        public void Disconnect()
        {
            try
            {
                if (skt.Connected)
                {
                    skt.Close();
                    skt = null;
                }
            }
            catch { }
        }

        public delegate void OnConnectEventHandler(MasterServer sender, bool connected);
        public event OnConnectEventHandler OnConnect;
    }
}