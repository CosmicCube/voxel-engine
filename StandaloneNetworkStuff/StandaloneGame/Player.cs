﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace StandaloneGame
{
    //DATA TYPES TO RECIEVE
    enum Packets : int
    {
        Location = 0,
        BlockPlacement = 2,
        BlockDamage = 3,

        ChatMsg = 9,
        Type = 10,

        WorldName = 11,
        WorldFile = 12,
        PlayerJoined = 13,
        PlayerLeft = 14,
    }
    //STRUCT TO STORE DATA
    struct ReceiveBuffer
    {
        public const int BUFFER_SIZE = 1024;
        public byte[] Buffer;
        public int ToReceive;
        public MemoryStream BufStream;

        public ReceiveBuffer(int toRec)
        {
            Buffer = new byte[BUFFER_SIZE];
            ToReceive = toRec;
            BufStream = new MemoryStream(toRec);
        }
        public void Dispose()
        {
            Buffer = null;
            ToReceive = 0;
            if (BufStream != null)
                BufStream.Dispose();
        }

    }
    internal class Player
    {
        //NETWORK DATA
        public byte ID;
        public PlayerData data;
        Socket skt;
        ReceiveBuffer buffer;
        byte[] lenBuffer;
        public IPEndPoint EndPoint { get { if (skt != null && skt.Connected) return (IPEndPoint)skt.RemoteEndPoint; return new IPEndPoint(IPAddress.None, 0); } }
        
        //SET PLAYER ID AND SOCKET ETC
        public Player(Socket s)
        {
            data = new PlayerData();
            skt = s;
            lenBuffer = new byte[4];
        }
        //CLOSE CONNECTION
        public void Close()
        {
            if (skt != null)
            {
                skt.Disconnect(false);
                skt.Close();
            }
            skt.Dispose();
            skt = null;
            lenBuffer = null;
            Disconnected = null;
            DataReceived = null;
        }

        //SEND DATA
        public void Send(byte[] data, int index, int length)
        {
            skt.BeginSend(BitConverter.GetBytes(length), 0, 4, SocketFlags.None, sendCallback, null);
            skt.BeginSend(data, index, length, SocketFlags.None, sendCallback, null);
        }
        //COMPLETE DATA SENDING
        void sendCallback(IAsyncResult ar)
        {
            try
            {
                int sent = skt.EndSend(ar);
                if (OnSend != null)
                    OnSend(this, sent);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        //AWAIT DATA RECIEVED
        public void receive()
        {
            skt.BeginReceive(lenBuffer, 0, lenBuffer.Length, SocketFlags.None, receiveCallback, null);
        }
        //MARKER PACKET WAS RECIEVED
        void receiveCallback(IAsyncResult ar)
        {
            try
            {
                int rec = skt.EndReceive(ar);
                if (rec != 4)
                {
                    if (rec == 0 && Disconnected != null)
                    {
                        Disconnected(this);
                        return;
                    }
                    else
                        throw new Exception();
                }
            }
            catch (SocketException ex)
            {
                switch (ex.SocketErrorCode)
                {
                    case SocketError.ConnectionAborted:
                    case SocketError.ConnectionReset:
                        if (Disconnected != null) { Disconnected(this); return; }
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            buffer = new ReceiveBuffer(BitConverter.ToInt32(lenBuffer, 0));
            skt.BeginReceive(buffer.Buffer, 0, buffer.Buffer.Length, SocketFlags.None, receivePacketCallback, null);
        }
        //DATA PACKET WAS RECIEVED
        void receivePacketCallback(IAsyncResult ar)
        {
            int rec = skt.EndReceive(ar);
            if (rec <= 0)
                return;
            buffer.BufStream.Write(buffer.Buffer, 0, rec);
            buffer.ToReceive -= rec;

            if (buffer.ToReceive > 0)
            {
                Array.Clear(buffer.Buffer, 0, buffer.Buffer.Length);
                skt.BeginReceive(buffer.Buffer, 0, buffer.Buffer.Length, SocketFlags.None, receivePacketCallback, null);
                return;
            }
            if (DataReceived != null)
            {
                buffer.BufStream.Position = 0;
                DataReceived(this, buffer);
            }
            buffer.Dispose();
            receive();
        }

        public delegate void DataReceivedEventHandler(Player sender, ReceiveBuffer e);
        public delegate void OnSendEventHandler(Player sender, int sent);
        public delegate void DisconnectedEventHandler(Player sender);
        public event OnSendEventHandler OnSend;
        public event DataReceivedEventHandler DataReceived;
        public event DisconnectedEventHandler Disconnected;
    }

    public class PlayerData
    {
        int Location_X;
        int Location_Y;
        int Location_Z;

        int LastLoc_X;
        int LastLoc_Y;
        int LastLoc_Z;
        public PlayerData()
        {
            Location_X = 0;
            Location_Y = 0;
            Location_Z = 0;
        }
        public void Location(int x, int y, int z)
        {
            Location_X = x;
            Location_Y = y;
            Location_Z = z;
        }
        public void Update()
        {
            LastLoc_X = Location_X;
            LastLoc_Y = Location_Y;
            LastLoc_Z = Location_Z;
        }
            
    }
}