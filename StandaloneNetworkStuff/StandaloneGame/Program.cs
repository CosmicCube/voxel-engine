﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace StandaloneGame
{
    class Program
    {
        public static World world;
        static List<Player> players;
        static MasterServer master;
        static Listener listener;
        static List<byte> freeIds = new List<byte>();
        static string dir = Directory.GetCurrentDirectory();

        static Json_server_settings settings;


        //LAUNCH PROGRAM
        static void Main(string[] args)
        {
            players = new List<Player>();
            // dir = @"D:\Programing\Repo\voxel-engine\Unmanaged";
            //Directory.SetCurrentDirectory(dir);

            #region Load Settings
            if (File.Exists(dir + "/Data/server.json"))
                settings = Json_server_settings.FromJson(File.ReadAllText(dir + "/Data/server.json"));
            else
                settings = new Json_server_settings();
            #endregion
            #region Parse args to settings
            if (args.Length > 0)
            {
                for (int i = 0; i < args.Length - 1; i += 2)
                {
                    Console.WriteLine($"Game Server > Using arg: {args[i]} {args[i + 1]}");
                    try
                    {
                        switch (args[i])
                        {
                            case "-mode":
                                settings.Mode = int.Parse(args[i + 1]);
                                break;
                            case "-map":
                                settings.Map = args[i + 1];
                                break;
                            case "-port":
                                settings.Port = int.Parse(args[i + 1]);
                                break;
                        }
                    }
                    catch { }
                }
            }
            #endregion

            //Socket master = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //master.Connect(IPAddress.Parse(MASTER_SERVER_IP), 49826);
            //Console.Out.WriteLine("Connected to master.");
            //StartServer(MASTER_SERVER_PORT);

            StartServer(settings.Port);
            Load_Map();

            //Needed as callback for Unity
            Console.Out.WriteLine("COMPLETED LOADING");

            string log;
            ConsoleReader CS = new ConsoleReader();
            do
            {
                log = Console.ReadLine();
                CS.Translate(log);
            }
            while (log != null);
            Console.Out.WriteLine("---");
        }
        static void Load_Map()
        {
            if (!File.Exists(dir + @"\Maps\" + settings.Map + ".bytes"))
            {
                Console.WriteLine("specified map [" + settings.Map + ".bytes] not found in the the directory [" + dir + @"\Maps\], try using one of the following instead:");

                foreach (string file in Directory.GetFileSystemEntries(dir + @"\Maps"))
                    if (file.EndsWith(".bytes"))
                        Console.WriteLine(file);
            }
            else
            {
                Console.WriteLine("loading map: " + settings.Map);
                world = new World(dir + @"\Maps\" + settings.Map + ".bytes");
            }
        }

        //CONNECT TO THE MASTER SERVER TO ADD SELF TO LIST
        static void PhoneHome()
        {

            master = new MasterServer();
            master.OnConnect += new MasterServer.OnConnectEventHandler(Master_OnConnect);
            master.Connect();
        }
        private static void Master_OnConnect(MasterServer sender, bool connected)
        {

            BinaryWriter bw = new BinaryWriter(new MemoryStream());
            bw.Write((int)Commands.ServerDetails);
            bw.Write(Variables.JsonSettings);
            bw.Close();
            byte[] data = ((MemoryStream)bw.BaseStream).ToArray();
            master.Send(data, 0, data.Length);
            bw.BaseStream.Dispose();
            data = null;
        }

        //APPLY CONSOLE OUTPUT
        public static void Execute(int Case, string arg1 = "", int arg2 = 0)
        {
            switch (Case)
            {
                case 0: StartServer(arg2); break;
                case 1: Environment.Exit(0); break;
                case 2: ListClients(arg2); break;
                case 3: SendText(arg1, arg2); break;
                case 4: Packet_Send(Packets.WorldName, "map-12"); break;
            }
        }
        //LIST CURRENT CLIENTS
        static void ListClients(int s)
        {
            if (players.Count == 0)
            {
                Console.WriteLine("{0}> there are no clients connected:", DateTime.Now);
            }
            if (s == 1)
            {
                Console.WriteLine("client list:");
                Console.WriteLine("clients {0}", players.Count);
            }
            else
            {
                int i = 0;
                foreach (Player c in players)
                {
                    i++;
                    Console.WriteLine("client list {0}: {1}", i, c.ID);
                }
            }
        }


        //IT ALL BEGINS HERE
        static void StartServer(int port)
        {
            if (port == 0) port = Variables.port;
            Console.WriteLine("{0}> launching server on port: {1}", DateTime.Now, port);
            listener = new Listener();
            listener.Accepted += new Listener.SocketAcceptedHandler(Listener_Accepted);
            listener.listen(port);
        }
        //ADD CLIENT TO LIST
        static void Listener_Accepted(Socket s)
        {
            Player player = new Player(s);
            if (freeIds.Count > 0)
            {
                player.ID = freeIds[0];
                freeIds.RemoveAt(0);
            }
            else
                player.ID = (byte)players.Count;
            Console.WriteLine("{0}> client connected: {1}", DateTime.Now, player.ID);
            player.OnSend += new Player.OnSendEventHandler(Client_OnSend);
            player.DataReceived += new Player.DataReceivedEventHandler(Client_DataReceived);
            player.Disconnected += new Player.DisconnectedEventHandler(client_Disconnected);
            player.receive();
            Packet_Return(player, Packets.Type, "game -id " + player.ID);
            Packet_Joined(player, false);
            players.Add(player);
        }
        //CALLED WHEN MESSAGE IS SENT
        static void Client_OnSend(Player sender, int sent)
        {
            //Console.WriteLine(string.Format("{0}> Data Sent: {1}", DateTime.Now, sent));
        }
        //RECIEVE REQUESTS
        static void Client_DataReceived(Player sender, ReceiveBuffer e)
        {
            BinaryReader br = new BinaryReader(e.BufStream);
            Packets header = (Packets)br.ReadInt32();
            switch (header)
            {
                case Packets.BlockPlacement:
                    int x = br.ReadInt32();
                    int y = br.ReadInt32();
                    int z = br.ReadInt32();
                    Block blk = new Block(br.ReadByte(), br.ReadByte(), br.ReadByte(), br.ReadByte());
                    if (world.GetNeighbors(x, y, z) || blk.id == 0)
                    {
                        world.SetBlock(x, y, z, blk);
                        Packet_Relay(null, e.BufStream);
                    }
                    break;

                case Packets.Location:
                    if (sender.ID == br.ReadByte())
                        Packet_Relay(sender, e.BufStream);
                    break;
                case Packets.ChatMsg:
                    string s = br.ReadString();
                    Console.WriteLine("{0} > {1}", DateTime.Now, s);
                    Packet_Relay(null, e.BufStream);
                    break;
            }
        }


        //REMOVE CLIENT FROM LIST
        static void client_Disconnected(Player sender)
        {
            freeIds.Add(sender.ID);
            players.Remove(sender);
            Packet_Joined(sender, true);
        }


        static void Packet_Joined(Player player, bool invert)
        {
            BinaryWriter bw = new BinaryWriter(new MemoryStream());
            if (invert)
                bw.Write((int)Packets.PlayerLeft);
            else
                bw.Write((int)Packets.PlayerJoined);
            bw.Write(player.ID);
            bw.Write("example");
            bw.Close();
            Packet_Relay(player, (MemoryStream)bw.BaseStream);
            bw.BaseStream.Dispose();
        }
        //RETURN REQUEST
        static void Packet_Send(Packets pkt, string input)
        {
            BinaryWriter bw = new BinaryWriter(new MemoryStream());
            bw.Write((int)pkt);
            bw.Write(input);
            bw.Close();
            Packet_Relay(null, (MemoryStream)bw.BaseStream);
            bw.BaseStream.Dispose();
        }
        //RETURN REQUEST
        static void Packet_Return(Player player, Packets pkt, string input)
        {
            BinaryWriter bw = new BinaryWriter(new MemoryStream());
            bw.Write((int)pkt);
            bw.Write(input);
            bw.Close();
            byte[] data = ((MemoryStream)bw.BaseStream).ToArray();
            bw.BaseStream.Dispose();
            player.Send(data, 0, data.Length);
            data = null;
        }
        //RELAY PACKET
        static void Packet_Relay(Player sender, MemoryStream BufStream)
        {
            byte[] data = BufStream.ToArray();
            BufStream.Dispose();
            foreach (Player p in players)
            {
                if (p != sender)
                    p.Send(data, 0, data.Length);
            }
            data = null;
        }
        //PACKET OF TEXT
        static void SendText(string text, int id = 0)
        {
            if (text == "" || text == null)
                return;
            BinaryWriter bw = new BinaryWriter(new MemoryStream());
            bw.Write((int)Packets.ChatMsg);
            bw.Write(text);
            bw.Close();
            byte[] data = ((MemoryStream)bw.BaseStream).ToArray();
            bw.BaseStream.Dispose();
            if (id == 0)
            {
                foreach (Player p in players)
                {
                    p.Send(data, 0, data.Length);
                }
            }
            else if (id > 0 && id <= players.Count)
            {
                players[id - 1].Send(data, 0, data.Length);
            }
            else
            {
                Console.WriteLine("{0}> invalid player id, there are only {1} players", DateTime.Now, players.Count);
            }
            data = null;
        }

        //LOAD WORLD

    }
}
