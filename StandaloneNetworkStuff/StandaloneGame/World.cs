﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace StandaloneGame
{
    public class World
    {
        public static World self;
        private Chunk[,,] chunks;
        private static V3I WorldSize;

        public World(string path)
        {
            self = this;
            Stream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None);
            chunks = LoadFromFile(fs, this);

        }        
        public static Chunk[,,] LoadFromFile(Stream stream, World world)
        {
            using (BinaryReader br = new BinaryReader(stream))
            {

                string name = br.ReadString();
                float killPlane = br.ReadSingle();
                float waterPlane = br.ReadSingle();
                short length = br.ReadInt16();
                short height = br.ReadInt16();
                short width = br.ReadInt16();

                WorldSize = new V3I(width, height, length);
                int dataSize = length * width * height * 4;

                byte[] data = br.ReadBytes(dataSize);
                Chunk[,,] chunks = new Chunk[width / Chunk.CHUNK_SIZE + 1, height / Chunk.CHUNK_SIZE + 1, length / Chunk.CHUNK_SIZE + 1];
                for (int x = 0; x <= width / Chunk.CHUNK_SIZE; x++)
                    for (int y = 0; y <= height / Chunk.CHUNK_SIZE; y++)
                        for (int z = 0; z <= length / Chunk.CHUNK_SIZE; z++)
                            chunks[x, y, z] = CreateChunk(x * Chunk.CHUNK_SIZE, y * Chunk.CHUNK_SIZE, z * Chunk.CHUNK_SIZE, world);

                for (int z = 0; z < length; z++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        for (int x = 0; x < width; x++)
                        {
                            int idx = 4 * (x + (y * width) + (z * height * width));
                            if (data[idx] == 0)
                                continue;


                            Block b = new Block(data[idx + 0], data[idx + 1], data[idx + 2], data[idx + 3]);
                            chunks[x / Chunk.CHUNK_SIZE, y / Chunk.CHUNK_SIZE, z / Chunk.CHUNK_SIZE].SetBlock(ChunkCoordinate(x), ChunkCoordinate(y), ChunkCoordinate(z), b);

                        }
                    }
                }

                return chunks;
            }
        }

        public void SetBlock(int x, int y, int z, Block block)
        {
            if (block.id == 0)
                block = null;

            int chunkX = x / Chunk.CHUNK_SIZE;
            int chunkY = y / Chunk.CHUNK_SIZE;
            int chunkZ = z / Chunk.CHUNK_SIZE;

            int blockX = x - (chunkX * Chunk.CHUNK_SIZE);
            int blockY = y - (chunkY * Chunk.CHUNK_SIZE);
            int blockZ = z - (chunkZ * Chunk.CHUNK_SIZE);

            GetChunk(chunkX, chunkY, chunkZ).SetBlock(blockX, blockY, blockZ, block);
        }
          

        public Chunk GetChunk(int x, int y, int z)
        {
            if (x < 0 || y < 0 || z < 0)
                return null;

            if (x >= chunks.GetLength(0) || y >= chunks.GetLength(1) || z >= chunks.GetLength(2))
                return null;

            return chunks[x, y, z];
        }

        public Chunk GetChunkForBlock(int x, int y, int z)
        {
            return GetChunk(x / Chunk.CHUNK_SIZE, y / Chunk.CHUNK_SIZE, z / Chunk.CHUNK_SIZE);
        }

        public static Chunk CreateChunk(int x, int y, int z, World world)
        {
            Chunk newChunk = new Chunk();

            newChunk.world = world;
            newChunk.posX = x;
            newChunk.posY = y;
            newChunk.posZ = z;

            return newChunk;
        }

        public Block GetBlock(int x, int y, int z)
        {
            int chunkX = x / Chunk.CHUNK_SIZE;
            int chunkY = y / Chunk.CHUNK_SIZE;
            int chunkZ = z / Chunk.CHUNK_SIZE;
            
            int blockX = x - (chunkX * Chunk.CHUNK_SIZE);
            int blockY = y - (chunkY * Chunk.CHUNK_SIZE);
            int blockZ = z - (chunkZ * Chunk.CHUNK_SIZE);

            Chunk chunk = GetChunk(chunkX, chunkY, chunkZ);

            if (chunk != null)
                return chunk.GetBlock(blockX, blockY, blockZ);

            return null;
        }
        public bool GetNeighbors(int x, int y, int z)
        {
            if (GetBlock(x + 1, y, z) != null)
                return true;
            if (GetBlock(x - 1, y, z) != null)
                return true;
            if (GetBlock(x, y + 1, z) != null)
                return true;
            if (GetBlock(x, y - 1, z) != null)
                return true;
            if (GetBlock(x, y, z + 1) != null)
                return true;
            if (GetBlock(x, y, z - 1) != null)
                return true;
            return false;
        }


        //if true return Dmg if false return Data
        public int GetDmgData(Block block, bool toggle)
        {
            double raw = (double)block.dmg / 10;
            double conv = raw - System.Math.Floor(raw);
            if (toggle)
                return (int)raw;
            else
                return (int)(System.Math.Round(10 * conv));
        }


        //MATH STUFF FOR CHUNKS
        public static int ChunkCoordinate(int i)
        {
            return i - ((i / Chunk.CHUNK_SIZE) * Chunk.CHUNK_SIZE);
        }
        public static int ChunkPosition(int i)
        {
            return (i / Chunk.CHUNK_SIZE) * Chunk.CHUNK_SIZE;
        }
    }
}