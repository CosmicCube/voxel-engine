﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace StandaloneServer
{
    enum Commands : int
    {
        Login = 0,
        Register = 1,
        RequestServers = 2,
        RequestProfile = 3,
        InvitePlayer = 4,

        Error = 8,
        ChatMsg = 9,
        Type = 10,

        LoginReturn = 11,
        ServerList = 12,
        ProfileInfo = 13,
        InviteAccept = 14,
        InviteDecline = 15,

        ServerDetails = 30,
    }
    struct ReceiveBuffer
    {
        public const int BUFFER_SIZE = 1024;
        public byte[] Buffer;
        public int ToReceive;
        public MemoryStream BufStream;

        public ReceiveBuffer(int toRec)
        {
            Buffer = new byte[BUFFER_SIZE];
            ToReceive = toRec;
            BufStream = new MemoryStream(toRec);
        }
        public void Dispose()
        {
            Buffer = null;
            ToReceive = 0;
            if (BufStream != null)
                BufStream.Dispose();
        }

    }
    internal class Client
    {
        public int ID;
        public bool loggedIn;
        Socket skt;
        ReceiveBuffer buffer;
        byte[] lenBuffer;
        public IPEndPoint EndPoint { get { if (skt != null && skt.Connected) return (IPEndPoint)skt.RemoteEndPoint; return new IPEndPoint(IPAddress.None, 0); } }

        //SET PLAYER ID AND SOCKET ETC
        public Client(Socket s, int id)
        {
            ID = id;
            skt = s;
            lenBuffer = new byte[4];
        }
        //CLOSE CONNECTION
        public void Close()
        {
            if (skt != null)
            {
                skt.Disconnect(false);
                skt.Close();
            }
            skt.Dispose();
            skt = null;
            lenBuffer = null;
            Disconnected = null;
            DataReceived = null;
        }

        //SEND DATA
        public void Send(byte[] data, int index, int length)
        {
            skt.BeginSend(BitConverter.GetBytes(length), 0, 4, SocketFlags.None, sendCallback, null);
            skt.BeginSend(data, index, length, SocketFlags.None, sendCallback, null);
        }
        //COMPLETE DATA SENDING
        void sendCallback(IAsyncResult ar)
        {
            try
            {
                int sent = skt.EndSend(ar);
                if (OnSend != null)
                    OnSend(this, sent);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        //AWAIT DATA RECIEVED
        public void receive()
        {
            skt.BeginReceive(lenBuffer, 0, lenBuffer.Length, SocketFlags.None, receiveCallback, null);
        }
        //MARKER PACKET WAS RECIEVED
        void receiveCallback(IAsyncResult ar)
        {
            try
            {
                int rec = skt.EndReceive(ar);
                if (rec != 4)
                {
                    if (rec == 0 && Disconnected != null)
                    {
                        Disconnected(this);
                        return;
                    }
                    else
                        throw new Exception();
                }
            }
            catch (SocketException ex)
            {
                switch (ex.SocketErrorCode)
                {
                    case SocketError.ConnectionAborted:
                    case SocketError.ConnectionReset:
                        if (Disconnected != null) { Disconnected(this); return; }
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            buffer = new ReceiveBuffer(BitConverter.ToInt32(lenBuffer, 0));
            skt.BeginReceive(buffer.Buffer, 0, buffer.Buffer.Length, SocketFlags.None, receivePacketCallback, null);
        }
        //DATA PACKET WAS RECIEVED
        void receivePacketCallback(IAsyncResult ar)
        {
            int rec = skt.EndReceive(ar);
            if (rec <= 0)
                return;
            buffer.BufStream.Write(buffer.Buffer, 0, rec);
            buffer.ToReceive -= rec;

            if (buffer.ToReceive > 0)
            {
                Array.Clear(buffer.Buffer, 0, buffer.Buffer.Length);
                skt.BeginReceive(buffer.Buffer, 0, buffer.Buffer.Length, SocketFlags.None, receivePacketCallback, null);
                return;
            }
            if (DataReceived != null)
            {
                buffer.BufStream.Position = 0;
                DataReceived(this, buffer);
            }
            buffer.Dispose();
            receive();
        }

        public delegate void DataReceivedEventHandler(Client sender, ReceiveBuffer e);
        public delegate void OnSendEventHandler(Client sender, int sent);
        public delegate void DisconnectedEventHandler(Client sender);
        public event OnSendEventHandler OnSend;
        public event DataReceivedEventHandler DataReceived;
        public event DisconnectedEventHandler Disconnected;
    }
}