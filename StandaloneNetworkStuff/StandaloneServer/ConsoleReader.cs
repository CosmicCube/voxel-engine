﻿using System.Text.RegularExpressions;

namespace StandaloneServer
{
    internal class ConsoleReader
    {
        public void Translate(string log)
        {
            string[][] regx = {
                new string[] { "start", @"(-ip) (\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})", @"(-port) (\d{1,5})" },
                new string[] { "kill", "", "" },
                new string[] { "list", "", @"(-c)([0-1])"},
                new string[] { "message", @"(-text) (.+)", "" } };
            for (int i = 0; i < regx.Length; i++)
            {
                bool matched = new Regex(regx[i][0], RegexOptions.IgnoreCase).Match(log).Success;
                if (matched)
                {
                    Match arg1 = new Regex(regx[i][1]).Match(log);
                    Match arg2 = new Regex(regx[i][2]).Match(log);
                    if (arg1.Success && regx[i][1] != "")
                    {
                        if (arg2.Success && regx[i][2] != "")
                            Program.Execute(i, arg1.Groups[2].ToString(), int.Parse(arg2.Groups[2].ToString()));
                        else
                            Program.Execute(i, arg1.Groups[2].ToString());
                    }
                    else if (arg2.Success && regx[i][2] != "")
                        Program.Execute(i, null, int.Parse(arg2.Groups[2].ToString()));
                    else
                        Program.Execute(i);
                }
            }
        }
    }
}