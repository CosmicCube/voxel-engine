﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace StandaloneServer
{
    class GameServer
    {
        public string ip;
        public int port;
        public Socket socket;

        public GameServer(string ip, int port)
        {
            this.ip = ip;
            this.port = port;
        }

        public void Connect()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IAsyncResult iar = socket.BeginConnect(IPAddress.Parse(ip), port, socketConnected, null);  
        }

        private void socketConnected(IAsyncResult ar)
        {
            try
            {
                socket.EndConnect(ar);
                if (socket.Connected)
                    Console.WriteLine("Connection made to --> " + ip + ":" + port);
            }
            catch
            {
                socket.Close();
                Console.WriteLine("Connection failed -->" + ip + ":" + port);
            }
        }
    }
}
