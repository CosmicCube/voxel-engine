﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;

namespace StandaloneServer
{
    public class Json
    {
        //Json Phrase Servers
        public static void phrase_list(string input)
        {
            MatchCollection regions = new Regex("\"name\": \"(\\w+)[\",\n\\s]+servers\":").Matches(input);
            MatchCollection servers = new Regex("(\\{[\\s\\w:\",.]+\\})").Matches(input);
            Program.serverList.regions = new List<Region>();
            foreach (Match rgn in regions)
            {
                Region region = new Region();
                region.name = rgn.Groups[1].Value;
                region.servers = new List<Server>();
                Program.serverList.regions.Add(region);

                foreach (Match srvr in servers)
                {
                    Server server = new Server();
                    string[] fields = new string[] { "name", "desc", "region", "ip", "url", "port", "host", "pass" };
                    foreach (string s in fields)
                    {
                        Match match = new Regex("(." + s + ".:) \"{0,1}([\\w\\d\\s.]+)\"{0,1},{0,1}").Match(srvr.Groups[1].ToString());
                        string result = match.Groups[2].ToString();
                        switch (s)
                        {
                            case "name": server.name = result; break;
                            case "desc": server.desc = result; break;
                            case "region": server.region = result; break;
                            case "ip": server.ip = result; break;
                            case "url": server.url = result; break;
                            case "port": try { server.port = int.Parse(result); } catch { server.port = 9825; } break;
                            case "host": server.host = result; break;
                            case "pass": server.pass = result; break;
                        }
                    }
                    if (server.region == region.name)
                        region.servers.Add(server);
                }
            }
        }
        public static string phrase_list(ServerList sl)
        {
            if (sl == null || sl.regions == null)
                return null;
            string stream = "{\n";
            stream += "\"version\": " + sl.version + ",\n";
            stream += "\"updated\": \"" + sl.updated + "\",\n";
            stream += "\"regions\": [ \n";
            foreach (Region r in sl.regions)
            {
                stream += "{\n\"name\": \"" + r.name + "\",\n";
                stream += "\"servers\": [ \n";
                if (r.servers != null)
                {
                    foreach (Server s in r.servers)
                    {
                        stream += "{\"name\": \"" + s.name + "\",\n";
                        stream += "\"desc\": \"" + s.desc + "\",\n";
                        stream += "\"region\": \"" + s.region + "\",\n";
                        stream += "\"ip\": \"" + s.ip + "\",\n";
                        stream += "\"url\": \"" + s.url + "\",\n";
                        stream += "\"port\": " + s.port + ",\n";
                        stream += "\"host\": \"" + s.host + "\",\n";
                        stream += "\"pass\": \"" + s.pass + "\"},\n";
                    }
                }
                stream = stream.Remove(stream.Length - 2);
                stream += "\n]\n},\n";
            }
            stream = stream.Remove(stream.Length - 2);
            stream += "\n]\n}";
            return stream;
        }
        public static void phrase_server(IPAddress ip, string input)
        {
            Server server = new Server();
            string[] fields = new string[] { "name", "desc", "region", "ip", "url", "port", "host", "pass" };
            foreach (string s in fields)
            {
                Match match = new Regex("(." + s + ".:) \"{0,1}([\\w\\s\\d.]+)\"{0,1},{0,1}").Match(input);
                string result = match.Groups[2].ToString();
                switch (s)
                {
                    case "name": server.name = result; break;
                    case "desc": server.desc = result; break;
                    case "region": server.region = result; break;
                    case "ip": server.ip = result; break;
                    case "url": server.url = result; break;
                    case "port": server.port = int.Parse(result); break;
                    case "host": server.host = result; break;
                    case "pass": server.pass = result; break;
                }
            }
            if (ip != null && Program.userLogin(null, null, server.host, server.pass))
            {
                //remove duplicate entries
                Match match = new Regex("\"name\": \"" + server.name + "\"").Match(Program.ServerList);
                if (match.Success)
                {
                    Console.WriteLine("duplicate server> " + match.Value);
                    return;
                }
                //correct IP/Url
                IPAddress address = new IPAddress(0);
                try { address = Dns.GetHostAddresses(server.url)[0]; }
                catch
                {
                    try { address = IPAddress.Parse(server.ip); }
                    catch { }
                }
                if (ip != address) { server.ip = ip.ToString(); server.url = ""; }
                
                //determine which region to add the server too
                Console.WriteLine("host verified> adding server: " + server.name);
                server.pass = "";
                int r = 0;
                if (Program.serverList.regions == null)
                    return;
                foreach (Region R in Program.serverList.regions)
                {
                    if (R.name == server.region)
                        break;
                    r++;
                }

                //check if the server ip + port exists but not the name (edited server)
                foreach (Server s in Program.serverList.regions[r].servers)
                {
                    if (s.ip == server.ip && s.port == server.port)
                    {
                        Program.serverList.regions[r].servers.Remove(s);
                        break;
                    }
                }
                Program.serverList.regions[r].servers.Add(server);
            }
            Program.ServerList = phrase_list(Program.serverList);
        }
    }
}