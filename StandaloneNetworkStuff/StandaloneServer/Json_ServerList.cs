﻿using System.Collections.Generic;

namespace StandaloneServer
{
    public class Server
    {
        public string name { get; set; }
        public string desc { get; set; }
        public string region { get; set; }
        public string ip { get; set; }
        public string url { get; set; }
        public int port { get; set; }
        public string host { get; set; }
        public string pass { get; set; }
    }

    public class Region
    {
        public string name { get; set; }
        public List<Server> servers { get; set; }
    }

    public class ServerList
    {
        public int version { get; set; }
        public string updated { get; set; }
        public List<Region> regions { get; set; }
    }
}