﻿using System;
using System.Net;
using System.Net.Sockets;

namespace StandaloneServer
{
    internal class Listener
    {
        Socket skt;
        public bool Running { get; private set; }
        public delegate void SocketAcceptedHandler(Socket e);
        public event SocketAcceptedHandler Accepted;





        Socket socket()
        {
            
            return new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public void listen(int port, string message = "none")
        {
            if (Running)
                return;
            try
            {
                skt = socket();
                skt.Bind(new IPEndPoint(IPAddress.Any, port));
                skt.Listen(0);

                skt.BeginAccept(listenCallback, message);
                Running = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void listenCallback(IAsyncResult ar)
        {
            try
            {
                Socket s = skt.EndAccept(ar);

                // Need error checking, socket may not actually be connected.
                Console.WriteLine("A " + ar.AsyncState + " connected");

                if (Accepted != null)
                {
                    Accepted(s);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (Running)
                {
                    skt.BeginAccept(listenCallback, ar.AsyncState);
                }
            }
            
        }
        public void Stop()
        {
            if (!Running)
                return;
            skt.Close();
            Running = false;
        }
    }
}