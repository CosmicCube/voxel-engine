﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace StandaloneServer
{
    class Program
    {
        // Changed from 9825 so running both the master and "We Are Brits" game server doesn't conflict.
        public const int SERVER_PORT = 49825;

        public const int GAMESERVER_LISTEN_PORT = 49826;
        public const int PLAYER_LISTEN_PORT = 49827;
        static List<GameServer> officialGameServers;
        // static List<GameServer> customGameServers;
        
        static Listener playerListener;
        static Listener serverListener;

        static Dictionary<string, playerData> dictionary;
        static List<Client> clients;
        static Listener listener;
        static int currentId;
        public static ServerList serverList = new ServerList();
        public static string ServerList;
        static string dir = Directory.GetCurrentDirectory();

        //LAUNCH PROGRAM
        static void Main(string[] args)
        {
            ServerList = File.ReadAllText(dir + "/data/ServerList.json");
            Json.phrase_list(ServerList);
           
            officialGameServers = LoadGameServersInfoFromJson();
            
            // Attempt to contact each game server in list.
            // A listener will 
            //foreach (GameServer gs in officialGameServers)
            {
              //  gs.Connect();
            }

            //playerListener = new Listener();
            //serverListener = new Listener();

            // Listen for new connections for players and game servers
           // playerListener.listen(PLAYER_LISTEN_PORT);
           // serverListener.listen(GAMESERVER_LISTEN_PORT, "game server");
           
             clients = new List<Client>();
            StartServer(9825);
            string log;                      
             ConsoleReader CS = new ConsoleReader();

               
            do
           {
               log = Console.ReadLine();
                CS.Translate(log);
            }
            while (log != null); 
            Console.WriteLine("---");
        }
        //APPLY CONSOLE OUTPUT
        public static void Execute(int Case, string arg1 = "", int arg2 = 0)
        {
            switch (Case)
            {
                case 0: StartServer(arg2); break;
                case 1: Environment.Exit(0); break;
                case 2: ListClients(arg2); break;
                case 3: foreach (Client c in clients) { Commands_Return(c, Commands.ChatMsg, arg1); } break;
            }
        }
        //LIST CURRENT CLIENTS
        static void ListClients(int s)
        {
            if (clients.Count == 0)
            {
                Console.WriteLine("{0}> there are no clients connected:", DateTime.Now);
            }
            if (s == 1)
            {
                Console.WriteLine("client list:");
                Console.WriteLine("clients {0}", clients.Count);
            }
            else
            {
                int i = 0;
                foreach (Client c in clients)
                {
                    i++;
                    Console.WriteLine("client list {0}: {1}", i, c.ID);
                }
            }
        }        
        //IT ALL BEGINS HERE
        static void StartServer(int port)
        {
            if (port == 0) port = 9825;
            Console.WriteLine("{0}> launching server on port: {1}", DateTime.Now, port);
            listener = new Listener();
            listener.Accepted += new Listener.SocketAcceptedHandler(Listener_Accepted);
            listener.listen(port);
        }
        //ADD CLIENT TO LIST
        static void Listener_Accepted(Socket s)
        {
            Client client = new Client(s, clients.Count);
            Console.WriteLine("{0}> client connected: {1}", DateTime.Now, client.ID);
            client.DataReceived += new Client.DataReceivedEventHandler(Client_DataReceived);
            client.Disconnected += new Client.DisconnectedEventHandler(client_Disconnected);
            client.receive();
            Commands_Return(client, Commands.Type, "master");
            clients.Add(client);
        }
        //RECIEVE REQUESTS
        static void Client_DataReceived(Client sender, ReceiveBuffer e)
        {
            BinaryReader r = new BinaryReader(e.BufStream);
            string s;
            Commands subheader = (Commands)r.ReadInt32();
            switch (subheader)
            {
                case Commands.ChatMsg:
                    s = r.ReadString();
                    Console.WriteLine("{0}> {1}", sender.ID, s);
                    break;
                case Commands.Login:
                    s = r.ReadString();
                    if(userLogin(sender, s));
                        Commands_Return(sender, Commands.LoginReturn, "Accepted");
                    break;
                case Commands.Register:
                    s = r.ReadString();
                    userRegister(sender, s);
                    break;
                case Commands.RequestServers:
                    Console.WriteLine("sending sever list to client: " + sender.ID);
                    Commands_Return(sender, Commands.ServerList, Json.phrase_list(serverList));
                    break;
                case Commands.RequestProfile:
                    s = r.ReadString();
                    string user = userLookUp(s);
                    if (user != null)
                        Commands_Return(sender, Commands.ProfileInfo, user);
                    else
                        Commands_Return(sender, Commands.ProfileInfo, "NotFound");
                    break;
                case Commands.ServerDetails:
                    s = r.ReadString();
                    Json.phrase_server(sender.EndPoint.Address, s);
                    break;
            }
        }
        //RETURN REQUEST
        public static void Commands_Return(Client client, Commands cmd, string input)
        {
            BinaryWriter bw = new BinaryWriter(new MemoryStream());
            bw.Write((int)cmd);
            if(input != null)
                bw.Write(input);
            bw.Close();
            byte[] data = ((MemoryStream)bw.BaseStream).ToArray();
            bw.BaseStream.Dispose();
            client.Send(data, 0, data.Length);
            data = null;
        }
        //REMOVE CLIENT FROM LIST
        static void client_Disconnected(Client sender)
        {
            Console.WriteLine("{0}> client left: {1}", DateTime.Now, sender.ID);
            clients.Remove(sender);
        }


        //REQUEST TO LOGIN TO SERVER
        public static bool userLogin(Client sender, string s, string user = null, string pass = null)
        {            
            if (user == null && pass == null && s != null)
            {
                Match arg = new Regex("(.+) :: (.+)").Match(s);
                if (!arg.Success)
                    return false;
                user = arg.Groups[1].ToString();
                pass = arg.Groups[2].ToString();
            }
            playerData dat = LookUp(user);
            if (dat != null && dat.password == pass)
            {
                if (sender != null)
                {
                    sender.loggedIn = true;
                    sender.ID = dat.id;
                }
                return true;
            }
            return false;
        }
        //REQUEST TO CREATE AN ACCOUNT
        static void userRegister(Client sender, string packit)
        {
            try
            {
                Match match = new Regex(@"(.+) :: (.+)={0,2} :: (\w+@.+)={0,2}").Match(packit);
                int id = currentId;
                string user = match.Groups[1].ToString();
                string pass = match.Groups[2].ToString();
                string email = match.Groups[3].ToString();


                string fileIndex = Convert.ToBase64String(Encoding.UTF8.GetBytes(user[0].ToString()));
                fileIndex = Regex.Replace(fileIndex, "={1,2}", "");
                string path = dir + "/Users/user " + fileIndex + "/players.json";
                Console.WriteLine("attempting to reg> " + path);

                if (!Directory.Exists(dir + "/Users/user " + fileIndex))
                    Directory.CreateDirectory(dir + "/Users/user " + fileIndex);
                if (!File.Exists(path)) { FileStream fs = File.Create(path); fs.Close(); fs.Dispose(); }

                TextWriter tw = new StreamWriter(path, true);
                tw.WriteLine("**" + id + " :: " + user + " :: " + pass + " :: " + email + "~~");
                tw.Close();

                sender.loggedIn = true;
                sender.ID = id;
                Commands_Return(sender, Commands.LoginReturn, "AccountCreated");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        //REQUEST INFO ON PLAYER NAME
        static string userLookUp(string user)
        {
            string fileIndex = Convert.ToBase64String(Encoding.UTF8.GetBytes(user[0].ToString()));
            string userIndex = Convert.ToBase64String(Encoding.UTF8.GetBytes(user));
            if (File.Exists(dir + "/Users/user " + fileIndex + "/" + user + ".json"))
            {
                return File.ReadAllText(dir + "/Users/user " + fileIndex + "/" + user + ".json");
            }
            return null;
        }


        //INTERNAL PLAYER LOOKUPS
        static playerData LookUp(string user)
        {
            string fileIndex = Convert.ToBase64String(Encoding.UTF8.GetBytes(user[0].ToString()));
            fileIndex = Regex.Replace(fileIndex, "={1,2}", "");
            string path = dir + "/Users/user " + fileIndex + "/players.json";
            if (File.Exists(path))
            {
                string dat = File.ReadAllText(path);
                
                Match match = new Regex("\\*\\*(\\d+) :: (" + user + ") :: (.+) :: ([\\w@.]+)={0,2}~~").Match(dat);
                if (match.Success)
                    return new playerData(int.Parse(match.Groups[1].ToString()), match.Groups[2].ToString(), match.Groups[3].ToString(), match.Groups[4].ToString());
            }
            return null;
        }               
        static Dictionary<string, playerData> loadDictionary(string fileIndex)
        {
            Dictionary<string, playerData> dict = new Dictionary<string, playerData>();
            if (File.Exists("/Users/user " + fileIndex + "/players.dat"))
            {
                string dat = File.ReadAllText("/Users/user " + fileIndex + "/players.dat");
                MatchCollection matches = new Regex("\\*\\*(.+) :: (.+) :: (\\w+@.+)={0,2}~~").Matches(dat);
                foreach (Match m in matches)
                    dict.Add(m.Groups[2].ToString(), new playerData(int.Parse(m.Groups[1].ToString()), m.Groups[2].ToString(), m.Groups[3].ToString()));
            }
            return dict;
        }

        static List<GameServer> LoadGameServersInfoFromJson()
        {
            List<GameServer> gameServers = new List<GameServer>();

            JObject serverListJson = (JObject)JsonConvert.DeserializeObject(File.ReadAllText(@"Data/ServerList.json"));
            foreach (JToken region in serverListJson["regions"])
            {
                // Skip regions with empty server list
                if (!region["servers"].ToObject<JArray>()[0].HasValues)
                    continue;

                foreach (JToken server in region["servers"])
                {
                    string ip = server["ip"].ToString();
                    int port = server["port"].ToObject<int>();

                    Console.WriteLine("Game Server Info Loaded: " + ip + ":" + port);

                    gameServers.Add(new GameServer(ip, port));
                }
            };

            return gameServers;
        }
 


    }
}
