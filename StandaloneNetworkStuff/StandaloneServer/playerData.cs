﻿namespace StandaloneServer
{
    internal class playerData
    {
        public int id;
        public string password;
        public string username;
        public string email;

        public playerData(int id, string username, string password, string email = "")
        {
            this.id = id;
            this.password = password;
            this.username = username;
            this.email = email;
        }
    }
}